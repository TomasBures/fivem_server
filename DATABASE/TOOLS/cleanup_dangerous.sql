delete from addon_account_data where `owner` is not null;
update addon_account_data set money = 0 where `owner` is null;

delete from addon_inventory_items;

delete from baninfo;
delete from banlist;
delete from banlisthistory;
delete from billing;
delete from cardealer_vehicles;
delete from characters;

delete from datastore_data where `owner` is not null;
update datastore_data set `data` = '{}' where `owner` is null;

delete from jail;
delete from owned_properties;
delete from owned_vehicles;
delete from paid_billing;
delete from qalle_brottsregister;
delete from qalle_medicalregister;
delete from rented_vehicles;
delete from society_moneywash;
delete from user_accounts;
delete from user_contacts;
delete from user_inventory;
delete from user_licenses;
delete from user_parkings;
delete from users;
delete from vehicle_inventory;
delete from vehicle_inventory_weapons;
delete from vehicle_sold;

