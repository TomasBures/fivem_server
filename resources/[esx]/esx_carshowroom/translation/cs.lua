Locales['cs'] = {
  -- Everything
  ['press_e'] = 'stiskni ~INPUT_CONTEXT~ pro navstevu showroomu',
  ['costs'] = ' Ceny: ',
  ['currency'] = ' €',
  ['back'] = 'Zpet',
  ['contact_dealer'] = 'Kontaktuj dealera, stoji: ',
  ['press_e_to_enter'] = 'stiskni ~INPUT_CONTEXT~ pro vstup do suterenu',
  ['press_e_to_exit'] = 'stiskni ~INPUT_CONTEXT~ pro navrat do prodejny',
}