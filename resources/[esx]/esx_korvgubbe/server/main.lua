ESX = nil

TriggerEvent('esx:getSharedObject', function(obj)
	ESX = obj
end)

-- MENU (START)
RegisterServerEvent('esx_korvgubbe:openMenu')
AddEventHandler('esx_korvgubbe:openMenu', function()
	local _source = source

	TriggerClientEvent('esx_korvgubbe:openMenuToPlayer', source)
end)
-- MENU (END)

-- SAUSAGE (START)
RegisterServerEvent('esx_korvgubbe:buySausage')
AddEventHandler('esx_korvgubbe:buySausage', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	
	if(xPlayer.getMoney() >= 5) then
		xPlayer.removeMoney(5)
		
		xPlayer.addInventoryItem('sausage-eat', 1)
	else
		notification("Nemas dost ~r~penez")
	end
end)

ESX.RegisterUsableItem('sausage-eat', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('sausage-eat', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 800000)
	TriggerClientEvent('esx_basicneeds:onEatHotdog', source)
	TriggerClientEvent('esx:showNotification', source, 'Prave si znedl HOTDOG')

end)
-- SAUSAGE (END)

-- WATER (START)
RegisterServerEvent('esx_korvgubbe:buyWater')
AddEventHandler('esx_korvgubbe:buyWater', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	
	if(xPlayer.getMoney() >= 2) then
		xPlayer.removeMoney(2)
		
		xPlayer.addInventoryItem('water', 1)
	else
		notification("Nemas dost ~r~penez")
	end
end)
-- WATER (END)


function notification(text)
	TriggerClientEvent('esx:showNotification', source, text)
end