Locales['en'] = {
  ['invoices'] = 'faktury',
  ['invoices_item'] = '$%s',
  ['received_invoice'] = 'prave si ~r~obdrzal~s~ fakturu na uhradu',
  ['paid_invoice'] = '~g~zaplatil si~s~ fakturu v hodnote ~r~$%s~s~',
  ['received_payment'] = '~g~dostal si~s~ platbu v hodnote ~r~$%s~s~',
  ['player_not_online'] = 'hrac nie je prihlaseny v hre',
  ['no_money'] = 'nemas dostatok penazi na uhradenie faktury. Makaj na tom aby si ich mal do 24hod.!',
  ['target_no_money'] = 'Tento hrac ~r~nema~s~ dostatok penazi na zaplatenie faktury!',
}
