Config = {}

Config.Animations = {

	{
		name  = 'festives',
		label = 'Párty',
		items = {
			--{label = "Fajčenie cigarety", type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING"}},
			{label = "Hranie na gitare/bubne", type = "scenario", data = {anim = "WORLD_HUMAN_MUSICIAN"}},
			{label = "DJ", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@dj", anim = "dj"}},
			{label = "Pitie malinovky", type = "scenario", data = {anim = "WORLD_HUMAN_DRINKING"}},
			{label = "Pitie piva", type = "scenario", data = {anim = "WORLD_HUMAN_PARTYING"}},
			{label = "Hranie na imaginárnu gitaru", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@air_guitar", anim = "air_guitar"}},
			{label = "Výťazné oslávenie", type = "anim", data = {lib = "anim@mp_player_intcelebrationfemale@air_shagging", anim = "air_shagging"}},
			{label = "Rock'n'roll", type = "anim", data = {lib = "mp_player_int_upperrock", anim = "mp_player_int_rock"}},
			-- {label = "Kouření jointu", type = "scenario", data = {anim = "WORLD_HUMAN_SMOKING_POT"}},
			{label = "Ožratý postoj", type = "anim", data = {lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a"}},
			-- {label = "Zvracení v autě", type = "anim", data = {lib = "oddjobs@taxi@tie", anim = "vomit_outside"}},
		}
	},

	{
		name  = 'greetings',
		label = 'Pozdravy',
		items = {
			{label = "Pozdrav rukou", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_hello"}},
			{label = "Normálne podanie ruky", type = "anim", data = {lib = "mp_common", anim = "givetake1_a"}},
			{label = "Štýlové podanie ruky", type = "anim", data = {lib = "mp_ped_interaction", anim = "handshake_guy_a"}},
			{label = "Mafiánsky podrav", type = "anim", data = {lib = "mp_ped_interaction", anim = "hugs_guy_a"}},
			{label = "Salutovanie", type = "anim", data = {lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute"}},
		}
	},

	{
		name  = 'work',
		label = 'Práca',
		items = {
			{label = "Kľaknutie s rukami nad hlavou", type = "anim", data = {lib = "random@arrests@busted", anim = "idle_c"}},
			{label = "Polícia: prehliatka", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_bj_to_prop_female"}},
			{label = "Polícia: vyšetrovanie", type = "anim", data = {lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f"}},
			{label = "Polícia: rozprávať do vyšielačky", type = "anim", data = {lib = "random@arrests", anim = "generic_radio_chatter"}},
			{label = "Polícia: riadenie dopravy", type = "scenario", data = {anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT"}},
			{label = "Polícia: pozeranie do diaľky", type = "scenario", data = {anim = "WORLD_HUMAN_BINOCULARS"}},
			{label = "Záhradník: Zakopať niečo", type = "scenario", data = {anim = "world_human_gardener_plant"}},
			{label = "Mechanik: oprava motora", type = "anim", data = {lib = "mini@repair", anim = "fixing_a_ped"}},
			{label = "Lekár: prehliadka", type = "scenario", data = {anim = "CODE_HUMAN_MEDIC_KNEEL"}},
			-- {label = "Taxi: rozprávanie dozadu na zákazníka", type = "anim", data = {lib = "oddjobs@taxi@driver", anim = "leanover_idle"}},
			{label = "Taxi: dávanie faktúry", type = "anim", data = {lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger"}},
			-- {label = "Prodavač: podávání zboží", type = "anim", data = {lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper"}},
			{label = "Barman: naliatie panáka", type = "anim", data = {lib = "mini@drinking", anim = "shots_barman_b"}},
			{label = "Novinár: fotenie", type = "scenario", data = {anim = "WORLD_HUMAN_PAPARAZZI"}},
			{label = "Zapisovanie poznámok", type = "scenario", data = {anim = "WORLD_HUMAN_CLIPBOARD"}},
			{label = "Zatĺcť kladivom niečo", type = "scenario", data = {anim = "WORLD_HUMAN_HAMMERING"}},
			{label = "Bezdomovec: Držanie cedule", type = "scenario", data = {anim = "WORLD_HUMAN_BUM_FREEWAY"}},
			-- {label = "Tulák: imitace sochy", type = "scenario", data = {anim = "WORLD_HUMAN_HUMAN_STATUE"}},
		}
	},

	{
		name  = 'humors',
		label = 'Nálady',
		items = {
			{label = "Potlesk", type = "scenario", data = {anim = "WORLD_HUMAN_CHEERING"}},
			{label = "Super", type = "anim", data = {lib = "mp_action", anim = "thanks_male_06"}},
			{label = "Ukázať prstom", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_point"}},
			{label = "Poď sem", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft"}}, 
			{label = "Čo je?", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on"}},
			{label = "Ja?", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_me"}},
			{label = "To som vedel sakra!", type = "anim", data = {lib = "anim@am_hold_up@male", anim = "shoplift_high"}},
			{label = "Postávanie", type = "scenario", data = {lib = "amb@world_human_jog_standing@male@idle_b", anim = "idle_d"}},
			{label = "Je to v piči!", type = "scenario", data = {lib = "amb@world_human_bum_standing@depressed@idle_a", anim = "idle_a"}},
			{label = "Facepalm - To je kokot!", type = "anim", data = {lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm"}},
			{label = "Kluď", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_easy_now"}},
			-- {label = "Čo som to urobil?", type = "anim", data = {lib = "oddjobs@assassinate@multi@", anim = "react_big_variations_a"}},
			{label = "Nebi ma prosím", type = "anim", data = {lib = "amb@code_human_cower_stand@male@react_cowering", anim = "base_right"}},
			{label = "Príprava na bitku", type = "anim", data = {lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e"}},
			-- {label = "To snáď nie je možné!", type = "anim", data = {lib = "gestures@m@standing@casual", anim = "gesture_damn"}},
			{label = "Dať pusu", type = "anim", data = {lib = "mp_ped_interaction", anim = "kisses_guy_a"}},
			{label = "Pojebte sa! - FuckOff", type = "anim", data = {lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter"}},
			{label = "Honka - Onanis", type = "anim", data = {lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01"}},
			{label = "Odjebať sa", type = "anim", data = {lib = "mp_suicide", anim = "pistol"}},
		}
	},

	{
		name  = 'sports',
		label = 'Športy',
		items = {
			{label = "Ukazovať svaly", type = "anim", data = {lib = "amb@world_human_muscle_flex@arms_at_side@base", anim = "base"}},
			{label = "Posilovať", type = "anim", data = {lib = "amb@world_human_muscle_free_weights@male@barbell@base", anim = "base"}},
			{label = "Robiť kliky", type = "anim", data = {lib = "amb@world_human_push_ups@male@base", anim = "base"}},
			{label = "Brušáky", type = "anim", data = {lib = "amb@world_human_sit_ups@male@base", anim = "base"}},
			{label = "Jóga", type = "anim", data = {lib = "amb@world_human_yoga@male@base", anim = "base_a"}},
		}
	},

	{
		name  = 'misc',
		label = 'Ostatné',
		items = {
			{label = "Pitie kávy", type = "anim", data = {lib = "amb@world_human_aa_coffee@idle_a", anim = "idle_a"}},
			{label = "Sedenie", type = "anim", data = {lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle"}},
			{label = "Oprieť sa o stenu", type = "scenario", data = {anim = "world_human_leaning"}},
			{label = "Ležať na chrpte", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE_BACK"}},
			{label = "Ležať na bruchu", type = "scenario", data = {anim = "WORLD_HUMAN_SUNBATHE"}},
			{label = "Leštenie", type = "scenario", data = {anim = "world_human_maid_clean"}},
			{label = "Grilovanie", type = "scenario", data = {anim = "PROP_HUMAN_BBQ"}},
			{label = "Rybarenie", type = "scenario", data = {anim = "world_human_stand_fishing"}},
			{label = "Fotenie selfie", type = "scenario", data = {anim = "world_human_tourist_mobile"}},
			{label = "Načúvať cez dvere", type = "anim", data = {lib = "mini@safe_cracking", anim = "idle_base"}}, 
		}
	},

	{
		name  = 'attitudem',
		label = 'Postoje',
		items = {
			{label = "Normálny muž", type = "attitude", data = {lib = "move_m@confident", anim = "move_m@confident"}},
			{label = "Normálna žena", type = "attitude", data = {lib = "move_f@heels@c", anim = "move_f@heels@c"}},
			{label = "Depresívny muž", type = "attitude", data = {lib = "move_m@depressed@a", anim = "move_m@depressed@a"}},
			{label = "Depresívna žena", type = "attitude", data = {lib = "move_f@depressed@a", anim = "move_f@depressed@a"}},
			{label = "Business", type = "attitude", data = {lib = "move_m@business@a", anim = "move_m@business@a"}},
			{label = "Odhodlaný", type = "attitude", data = {lib = "move_m@brave@a", anim = "move_m@brave@a"}},
			{label = "Běžný", type = "attitude", data = {lib = "move_m@casual@a", anim = "move_m@casual@a"}},
			{label = "Prejedený", type = "attitude", data = {lib = "move_m@fat@a", anim = "move_m@fat@a"}},
			{label = "Hipster", type = "attitude", data = {lib = "move_m@hipster@a", anim = "move_m@hipster@a"}},
			{label = "Zranený", type = "attitude", data = {lib = "move_m@injured", anim = "move_m@injured"}},
			{label = "Uponáhľaný", type = "attitude", data = {lib = "move_m@hurry@a", anim = "move_m@hurry@a"}},
			{label = "Bezdomovec", type = "attitude", data = {lib = "move_m@hobo@a", anim = "move_m@hobo@a"}},
			{label = "Smutný", type = "attitude", data = {lib = "move_m@sad@a", anim = "move_m@sad@a"}},
			{label = "Svalnatosť", type = "attitude", data = {lib = "move_m@muscle@a", anim = "move_m@muscle@a"}},
			{label = "Šokovaný", type = "attitude", data = {lib = "move_m@shocked@a", anim = "move_m@shocked@a"}},
			{label = "Podezrivý", type = "attitude", data = {lib = "move_m@shadyped@a", anim = "move_m@shadyped@a"}},
			{label = "Únava", type = "attitude", data = {lib = "move_m@buzzed", anim = "move_m@buzzed"}},
			-- {label = "Uspěchanost", type = "attitude", data = {lib = "move_m@hurry_butch@a", anim = "move_m@hurry_butch@a"}},
			{label = "Pyšný", type = "attitude", data = {lib = "move_m@money", anim = "move_m@money"}},
			{label = "Sprint", type = "attitude", data = {lib = "move_m@quick", anim = "move_m@quick"}},
			{label = "Gay", type = "attitude", data = {lib = "move_f@maneater", anim = "move_f@maneater"}},
			{label = "Namyslený", type = "attitude", data = {lib = "move_f@sassy", anim = "move_f@sassy"}},	
			{label = "Arogantný", type = "attitude", data = {lib = "move_f@arrogant@a", anim = "move_f@arrogant@a"}},
		}
	},
	{
		name  = 'porn',
		label = 'Sex',
		items = {
			{label = "Fajka v aute - muž", type = "anim", data = {lib = "oddjobs@towing", anim = "m_blow_job_loop"}},
			{label = "Fajka v aute - žena", type = "anim", data = {lib = "oddjobs@towing", anim = "f_blow_job_loop"}},
			{label = "Sex v aute - muž", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_player"}},
			{label = "Sex v aute - žena", type = "anim", data = {lib = "mini@prostitutes@sexlow_veh", anim = "low_car_sex_loop_female"}},
			{label = "Škrábanie v rozkroku", type = "anim", data = {lib = "mp_player_int_uppergrab_crotch", anim = "mp_player_int_grab_crotch"}},
			{label = "Postoj prostitutky", type = "anim", data = {lib = "mini@strip_club@idles@stripper", anim = "stripper_idle_02"}},
			{label = "Póza prostitutky", type = "scenario", data = {anim = "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"}},
			{label = "Ukázanie ceckou", type = "anim", data = {lib = "mini@strip_club@backroom@", anim = "stripper_b_backroom_idle_b"}},
			{label = "Striptýz 1", type = "anim", data = {lib = "mini@strip_club@lap_dance@ld_girl_a_song_a_p1", anim = "ld_girl_a_song_a_p1_f"}},
			{label = "Striptýz 2", type = "anim", data = {lib = "mini@strip_club@private_dance@part2", anim = "priv_dance_p2"}},
			{label = "Striptýz 3", type = "anim", data = {lib = "mini@strip_club@private_dance@part3", anim = "priv_dance_p3"}},
		}
	}
}