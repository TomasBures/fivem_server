Locales['cs'] = {
  ['mechanic']                  = 'mechanik',
  ['drive_to_indicated']        = '~y~Jed~s~ do vyznacene lokace.',
  ['mission_canceled']          = 'Mise ~r~zrusena~s~',
  ['vehicle_list']              = 'seznam vozidel',
  ['work_wear']                 = 'pracovni obleceni',
  ['civ_wear']                  = 'civilni obleceni',
  ['deposit_stock']             = '[Sklad] - vlozit',
  ['withdraw_stock']            = '[Sklad] - vzit',
  ['boss_actions']              = 'moznosti bosse',
  ['service_vehicle']           = 'sluzebni vozidlo',
  ['flat_bed']                  = 'flatbed odtahovka - pripevnit/uvolnit',
  ['tow_truck']                 = 'odtahovka',
  ['service_full']              = 'sluzba je plna: ',
  ['open_actions']              = 'Stiskni ~INPUT_CONTEXT~ pro pristup do menu.',
  ['harvest']                   = 'sbirat',
  ['harvest_menu']              = 'stiskni ~INPUT_CONTEXT~ pro pristup do menu sbirani.',
  ['not_experienced_enough']    = 'nejsi ~r~dostatecne zkuseny~s~ pro vykonani teto akce.',
  ['gas_can']                   = 'Plechovka propan butanu',
  ['repair_tools']              = 'nastroje na kompletni opravu',
  ['body_work_tools']           = 'nastroje na opravu karoserie',
  ['blowtorch']                 = 'letlampa',
  ['repair_kit']                = 'sada na kompletni opravu',
  ['body_kit']                  = 'sada na opravu karoserie',
  ['craft']                     = 'vyroba',
  ['craft_menu']                = 'stiskni ~INPUT_CONTEXT~ pro pristup do vyrobniho menu.',
  ['billing']                   = 'fakturace',
  ['hijack']                    = 'odemknout',
  ['repair']                    = 'opravit',
  ['clean']                     = 'umyt a nalestit',
  ['imp_veh']                   = 'zabavit vozidlo',
  ['place_objects']             = 'spawnout objekt',
  ['invoice_amount']            = 'castka na fakturu',
  ['amount_invalid']            = 'neplatna castka',
  ['no_players_nearby']         = 'v blizkosti neni zadny hrac',
  ['no_vehicle_nearby']         = 'v blizkosti neni zadne vozidlo',
  ['inside_vehicle']            = 'tuto akci nemuzes vykonat uvnitr auta!', 
  ['vehicle_unlocked']          = 'vozidlo bylo ~g~odemceno',
  ['vehicle_repaired']          = 'vozidlo bylo ~g~opraveno',
  ['vehicle_cleaned']           = 'vozidlo bylo ~g~umyto a nalesteno',
  ['vehicle_impounded']         = 'vozidlo bylo ~r~zabaveno',
  ['must_seat_driver']          = 'musis byt na sedadle ridice!',
  ['must_near']                 = 'musis byt ~r~blizko vozidla~s~ abys ho zabavil.',
  ['vehicle_success_attached']  = 'vozidlo bylo ~b~pripevneno~s~',
  ['please_drop_off']           = 'prosim dovez vozidlo do garaze',
  ['cant_attach_own_tt']        = '~r~nemuzes~s~ pripevnit vlastni odtahovku',
  ['no_veh_att']                = 'neni tu zadne ~r~vozidlo~s~ k pripevneni',
  ['not_right_veh']             = 'tohle neni to spravne vozidlo',
  ['veh_det_succ']              = 'vozidlo uspesne ~b~uvolneno~s~!',
  ['imp_flatbed']               = '~r~Akce neni mozna!~s~ Potrebujes ~b~Flatbed odtahovku~s~ pro pripevneni vozidla',
  ['objects']                   = 'objekty',
  ['roadcone']                  = 'kuzel',
  ['toolbox']                   = 'box s naradim',
  ['mechanic_stock']            = 'sklad mechaniku',
  ['quantity']                  = 'mnozstvi',
  ['invalid_quantity']          = 'neplatne mnozstvi',
  ['inventory']                 = 'inventar',
  ['veh_unlocked']              = '~g~Vozidlo odemceno',
  ['hijack_failed']             = '~r~Odemykani se nezdarilo',
  ['body_repaired']             = '~g~Karoserie opravena',
  ['veh_repaired']              = '~g~Vozidlo opraveno',
  ['veh_stored']                = 'stiskni ~INPUT_CONTEXT~ pro zaparkovani vozidla.',
  ['press_remove_obj']          = 'stiskni ~INPUT_CONTEXT~ pro odstraneni objektu',
  ['please_tow']                = 'prosim ~y~odtahni~s~ toto vozidlo',
  ['wait_five']                 = 'musis ~r~pockat~s~ 5 minut',
  ['must_in_flatbed']           = 'musis byt ve flatbed odtahovce pro spusteni mise',
  ['mechanic_customer']         = 'mechanikuv zakaznik',
  ['you_do_not_room']           = '~r~Uz nemas misto',
  ['recovery_gas_can']          = 'Ziskavas... ~b~Plechovku s propan butanem~s~',
  ['recovery_repair_tools']     = 'Ziskavas... ~b~Nastroje na kompletni opravu~s~',
  ['recovery_body_tools']       = 'Ziskavas... ~b~Nastroje na opravu karoserie~s~',
  ['not_enough_gas_can']        = '~r~Nemas dostatek~s~ plechovek s plynem.',
  ['assembling_blowtorch']      = 'Skladani ~b~Letlampy~s~...',
  ['not_enough_repair_tools']   = '~r~Nemas dostatek~s~ nastroju na kompletni opravu.',
  ['assembling_repair_kit']     = 'Skladani ~b~Sady na kompletni opravu~s~...',
  ['not_enough_body_tools']     = '~r~Nemas dostatek~s~ nastroju na opravu karoserie.',
  ['assembling_body_kit']       = 'Skladani ~b~Sady na opravu karoserie~s~...',
  ['your_comp_earned']          = 'Vase spolecnost ~g~vydelala~s~ ~g~€',
  ['you_used_blowtorch']        = 'pouzil jsi ~b~letlampu',
  ['you_used_repair_kit']       = 'pouzil jsi ~b~sadu na kompletni opravu',
  ['you_used_body_kit']         = 'you used a ~b~sadu na opravu karoserie',
  ['have_withdrawn']            = 'vzal jsi ~y~x%s~s~ ~b~%s~s~',
  ['have_deposited']            = 'vlozil jsi ~y~x%s~s~ ~b~%s~s~',
  ['player_cannot_hold']        = '~r~nemas~s~ dostatek ~y~volneho mista~s~ v inventari!',
  ['garagespawn_menu']          = 'Stiskni ~INPUT_CONTEXT~ pro otevreni menu garaze.',
  ['engine_work_tools']         = 'Nastroje na opravu motoru',
  ['recovery_eng_tools']        = 'Ziskavas... ~b~Nastroje na opravu motoru~s~',
  ['eng_kit']                   = 'sada na opravu motoru',
  ['assembling_eng_kit']        = 'Skladani ~b~Sady na opravu motoru~s~...',
  ['not_enough_eng_tools']      = '~r~Nemas dostatek~s~ nastroju na opravu motoru.',
  ['max_crafted']               = 'Uz nemuzes dal vyrabet, vic toho proste neuneses',
  ['you_used_eng_kit']          = 'pouzil jsi ~b~sadu na opravu motoru',
  ['recovery_fuelcanister']     = 'Ziskavas... ~b~Kanistr s benzinem~s~',
  ['you_used_fuelcanister']     = 'pouzil jsi ~b~kanistr s benzinem',
  ['fuel_canister']             = 'kanistr s benzinem',
  ['veh_fuelled']               = 'vozidlo bylo natankovano',
}
