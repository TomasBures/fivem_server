USE `essentialmode`;

INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES
	/*coopjednota */
	('bread', 'Chleba', 30, 0, 1),
	('chocolate', 'Cokolada', 30, 0, 1),
	('sandwich', 'Sendvic', 30, 0, 1),
	('water', 'Voda', 15, 0, 1),
	('mineral', 'Voda', 15, 0, 1),
	/* mcdonalds */
	('hamburger', 'Hamburger', 10, 0, 1),
	('cheesburger', 'Cheesburger', 10, 0, 1),
	('bigmac', 'BigMac', 10, 0, 1),
	('mcchicken', 'McChicken', 10, 0, 1),
	('cocacola', 'Coca-cola', 10, 0, 1),
	('coffe', 'McCafe', 10, 0, 1),
	('fanta', 'Fanta', 10, 0, 1),
	('milkshake', 'MilkShake', 10, 0, 1)
	/* Ardy */
;
