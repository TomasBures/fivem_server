ESX = nil
local TrunksAcessed = { { plate = 'foo', idsource = -1 } }

TriggerEvent('esx:getSharedObject', function(obj)
  ESX = obj
end)

AddEventHandler('onMySQLReady', function()

	MySQL.Sync.execute("delete from vehicle_inventory_weapons where plate not in (select ov.plate from owned_vehicles ov)", {})
	MySQL.Sync.execute("delete from vehicle_inventory where plate not in (select ov.plate from owned_vehicles ov);", {})
	
end)

RegisterServerEvent('esx_vehicle_inventory:getInventory')
AddEventHandler('esx_vehicle_inventory:getInventory', function(plate)
  local inventory_ = {}
  local _source = source
  MySQL.Async.fetchAll(
    'SELECT i.label, vi.item, vi.itemcount FROM vehicle_inventory vi, items i WHERE vi.plate = @plate and vi.item = i.name',
    {
      ['@plate'] = plate
    },
    function(inventory)
      if inventory ~= nil and #inventory > 0 then
        for i=1, #inventory, 1 do
          table.insert(inventory_, {
            label     = inventory[i].label,
            name      = inventory[i].item,
            count     = inventory[i].itemcount
          })
        end
      end
    local xPlayer  = ESX.GetPlayerFromId(_source)
	
	local isFound = false
	
	for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate then 
		    TrunksAcessed[i].idsource = _source
			isFound = true
			break
		end
	end
	
	if isFound == false then
	    table.insert(TrunksAcessed, {plate = plate, idsource = _source})
	end
	
    TriggerClientEvent('esx_vehicle_inventory:setInventoryLoaded', xPlayer.source, inventory_)
    end)
end)

RegisterServerEvent('esx_vehicle_inventory:getWeaponsInventory')
AddEventHandler('esx_vehicle_inventory:getWeaponsInventory', function(plate)
  local inventoryWep = {}
  local _source = source
  MySQL.Async.fetchAll(
    'SELECT id, weapon, ammo FROM vehicle_inventory_weapons WHERE plate = @plate',
    {
      ['@plate'] = plate
    },
    function(inventory)
      if inventory ~= nil and #inventory > 0 then
        for i=1, #inventory, 1 do
          table.insert(inventoryWep, {
		    id        = inventory[i].id,
            weapon    = inventory[i].weapon,
            ammo      = inventory[i].ammo,
          })
		  
        end
      end
    local xPlayer  = ESX.GetPlayerFromId(_source)
	
	local isFound = false
	
	for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate then 
		    TrunksAcessed[i].idsource = _source
			isFound = true
			break
		end
	end
	
	if isFound == false then
	    table.insert(TrunksAcessed, {plate = plate, idsource = _source})
	end
	
    TriggerClientEvent('esx_vehicle_inventory:setWeaponsInventoryLoaded', xPlayer.source, inventoryWep)
    end)
end)


RegisterServerEvent('esx_vehicle_inventory:removeInventoryItem')
AddEventHandler('esx_vehicle_inventory:removeInventoryItem', function(plate, item, count)
  local _source = source
  
  local isValid = false
  
  for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate and TrunksAcessed[i].idsource == _source then 
			isValid = true
			break
		end
  end
  
    if isValid == true then
	    MySQL.Async.fetchAll(
        'UPDATE `vehicle_inventory` SET `itemcount`= `itemcount` - @qty WHERE `plate` = @plate AND `item`= @item',
        {
          ['@plate'] = plate,
          ['@qty'] = count,
          ['@item'] = item
        },
        function(result)
          local xPlayer  = ESX.GetPlayerFromId(_source)
          if xPlayer ~= nil then
            xPlayer.addInventoryItem(item, count)
          end
        end)
	
	else
	    TriggerClientEvent('esx_vehicle_inventory:multipleAcess', _source)
	end
end)

RegisterServerEvent('esx_vehicle_inventory:removeWeaponsInventoryItem')
AddEventHandler('esx_vehicle_inventory:removeWeaponsInventoryItem', function(plate, id, item, count)
  local _source = source
  
  local isValid = false
  
  for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate and TrunksAcessed[i].idsource == _source then 
			isValid = true
			break
		end
  end
  
    if isValid == true then
	    MySQL.Async.execute(
        'delete from vehicle_inventory_weapons WHERE `plate` = @plate AND `id`= @id',
        {
          ['@plate'] = plate,
          ['@id'] = id
        },
        function(result)
          local xPlayer  = ESX.GetPlayerFromId(_source)
          if xPlayer ~= nil then
			xPlayer.addWeapon(item, count)
          end
        end)
	
	else
	    TriggerClientEvent('esx_vehicle_inventory:multipleAcess', _source)
	end
end)

RegisterServerEvent('esx_vehicle_inventory:addInventoryItem')
AddEventHandler('esx_vehicle_inventory:addInventoryItem', function(plate, item, count)
  local _source = source
  
  local isValid = false
  
  for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate and TrunksAcessed[i].idsource == _source then 
			isValid = true
			break
		end
  end
  
	if isValid == true then
        MySQL.Async.fetchAll(
            'INSERT INTO vehicle_inventory (item,itemcount,plate) VALUES (@item,@qty,@plate) ON DUPLICATE KEY UPDATE itemcount=itemcount+ @qty',
            {
              ['@plate'] = plate,
              ['@qty'] = count,
              ['@item'] = item
            },
            function(result)
            local xPlayer  = ESX.GetPlayerFromId(_source)
            xPlayer.removeInventoryItem(item, count)
        end)
	
	else
	    TriggerClientEvent('esx_vehicle_inventory:multipleAcess', _source)
	end
end)

RegisterServerEvent('esx_vehicle_inventory:addWeaponsInventoryItem')
AddEventHandler('esx_vehicle_inventory:addWeaponsInventoryItem', function(plate, item, count)
  local _source = source
  
  local isValid = false
  
  for i = 1, #TrunksAcessed do
		if TrunksAcessed[i] ~= nil and TrunksAcessed[i].plate ~= nil and TrunksAcessed[i].plate == plate and TrunksAcessed[i].idsource == _source then 
			isValid = true
			break
		end
  end
  
	if isValid == true then
        MySQL.Async.fetchAll(
            'INSERT INTO vehicle_inventory_weapons (weapon,ammo,plate) VALUES (@item,@qty,@plate)',
            {
              ['@plate'] = plate,
              ['@qty'] = count,
              ['@item'] = item
            },
            function(result)
            local xPlayer  = ESX.GetPlayerFromId(_source)
			xPlayer.removeWeapon(item)
        end)
	
	else
	    TriggerClientEvent('esx_vehicle_inventory:multipleAcess', _source)
	end
end)
