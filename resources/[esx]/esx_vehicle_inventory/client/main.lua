local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX                           = nil
local GUI                     = {}
local PlayerData              = {}
local lastVehicle             = nil
local lastOpen                = false
GUI.Time                      = 0
local vehiclePlate            = {}
local trunkContainsCount      = 0

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

function VehicleInFront()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 4.0, 0.0)
    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
    local a, b, c, d, result = GetRaycastResult(rayHandle)
    return result
end

-- Key controls
Citizen.CreateThread(function()
    while true do

        Wait(10)

        if IsControlPressed(0, Keys["["]) and (GetGameTimer() - GUI.Time) > 150 then
            local vehFront = VehicleInFront()
            local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
            local closecar = GetClosestVehicle(x, y, z, 4.0, 0, 71)
            if vehFront > 0 and closecar ~= nil and GetPedInVehicleSeat(closecar, -1) ~= GetPlayerPed(-1) then
                lastVehicle = vehFront

                local model = GetDisplayNameFromVehicleModel(GetEntityModel(closecar))
                local unlocked = GetVehicleDoorLockStatus(closecar)

                ESX.UI.Menu.CloseAll()

                if unlocked == 1 then
                    SetVehicleDoorOpen(vehFront, 5, false, false)
					lastOpen = true
                    
					ESX.UI.Menu.CloseAll()
                    TriggerServerEvent("esx_vehicle_inventory:getInventory", GetVehicleNumberPlateText(vehFront))
                else
                    ESX.ShowNotification('Tento kufr je uzamcen')
                end
            else
                ESX.ShowNotification('~r~Zadne vozidlo v blizkosti~w~')
            end
            
            GUI.Time  = GetGameTimer()
        elseif lastOpen and IsControlPressed(0, Keys["BACKSPACE"]) and (GetGameTimer() - GUI.Time) > 150 then
            lastOpen = false
            ESX.UI.Menu.CloseAll()
            if lastVehicle > 0 then
                SetVehicleDoorShut(lastVehicle, 5, false)
                lastVehicle = 0
            end
            GUI.Time  = GetGameTimer()
        end
    end
end)

RegisterNetEvent('esx_vehicle_inventory:multipleAcess')
AddEventHandler('esx_vehicle_inventory:multipleAcess', function()
	Wait(1000)
	ESX.ShowNotification('~r~Jiny hrac otevrel kufr, prosim zkus to pozdeji');
	ESX.UI.Menu.CloseAll()
	lastVehicle = nil
	lastOpen = false
end)

RegisterNetEvent('esx_vehicle_inventory:setInventoryLoaded')
AddEventHandler('esx_vehicle_inventory:setInventoryLoaded', function(inventory)
    local elements = {}
    local vehFrontBack = VehicleInFront()

	local vehicleClass = GetVehicleClass(vehFrontBack)
	local itemlimit = Config.InventoryItemsLimit
	
	if Config.VehicleLimit[vehicleClass] ~= nil and Config.VehicleLimit[vehicleClass] > 0 then
		itemlimit = Config.VehicleLimit[vehicleClass]
	end
	
	table.insert(elements, {
        label     = '-- [Zbrane] --',
        count     = 0,
        value     = 'loadout',
    })
	
    table.insert(elements, {
        label     = '-- [Vlozit predmet] --',
        count     = 0,
        value     = 'deposit',
    })

	trunkContainsCount = 0
	
    if inventory ~= nil and #inventory > 0 then
        for i=1, #inventory, 1 do
            if inventory[i].count > 0 then
                table.insert(elements, {
                    label     = inventory[i].label .. ' x' .. inventory[i].count,
                    count     = inventory[i].count,
                    value     = inventory[i].name,
                })
				
				trunkContainsCount = trunkContainsCount + inventory[i].count
            end

        end
    end

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'inventory_deposit',
        {
            title    = 'Obsah kufru [' .. tostring(trunkContainsCount) .. '/' .. itemlimit .. ']' ,
            align    = 'bottom-right',
            elements = elements,
        },
        function(data, menu)
		    if data.current.value == 'loadout' then
				ESX.UI.Menu.CloseAll()
				vehFront = VehicleInFront()
				if vehFront > 0 then
					TriggerServerEvent("esx_vehicle_inventory:getWeaponsInventory", GetVehicleNumberPlateText(vehFront))
				else
					ESX.ShowNotification('~r~Prilis ses vzdalil od kufru')
					SetVehicleDoorShut(vehFrontBack, 5, false)
				end
            elseif data.current.value == 'deposit' then
                local elem = {}
                PlayerData = ESX.GetPlayerData()
                for i=1, #PlayerData.inventory, 1 do
                    if PlayerData.inventory[i].count > 0 then
                        table.insert(elem, {
                            label     = PlayerData.inventory[i].label .. ' x' .. PlayerData.inventory[i].count,
                            count     = PlayerData.inventory[i].count,
                            value     = PlayerData.inventory[i].name,
                            name      = PlayerData.inventory[i].label,
                            limit     = PlayerData.inventory[i].limit,
                        })
                    end
                end
                ESX.UI.Menu.Open(
                    'default', GetCurrentResourceName(), 'inventory_player',
                    {
                        title    = 'Obsah inventare',
                        align    = 'bottom-right',
                        elements = elem,
                    },function(data3, menu3)
                        ESX.UI.Menu.Open(
                            'dialog', GetCurrentResourceName(), 'inventory_item_count_give',
                            {
                                title = 'mnozstvi'
                            },
                            function(data4, menu4)
                                local quantity = tonumber(data4.value)
                                vehFront = VehicleInFront()

                                --fin test

                                if quantity > 0 and quantity <= tonumber(data3.current.count) and vehFront > 0  then
                                    local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
                                    local closecar = GetClosestVehicle(x, y, z, 4.0, 0, 71)

									if (trunkContainsCount + quantity) <= itemlimit then
                                        TriggerServerEvent('esx_vehicle_inventory:addInventoryItem', GetVehicleNumberPlateText(vehFront), data3.current.value, quantity)
                                        ESX.ShowNotification('~g~Predmet uspesne vlozen do kufru. Stav kufru: ' .. tostring(trunkContainsCount + quantity) .. '/' .. itemlimit)
									else
									    ESX.ShowNotification('~r~Predmet se nevejde do kufu. Stav kufru by byl: ' .. tostring(trunkContainsCount + quantity) .. '/' .. itemlimit)
									end
								elseif vehFront <= 0 then
						            ESX.ShowNotification('~r~Prilis ses vzdalil od kufru')
                                else
                                    ESX.ShowNotification('~r~Neplatne mnozstvi')
                                end

                                ESX.UI.Menu.CloseAll()
								
								local vehFront = VehicleInFront()
                                if vehFront > 0 then
                                    ESX.SetTimeout(300, function()
                                        TriggerServerEvent("esx_vehicle_inventory:getInventory", GetVehicleNumberPlateText(vehFront))
                                    end)
                                else
                                    SetVehicleDoorShut(vehFrontBack, 5, false)
                                end
                            end,

                            function(data4, menu4)
                                SetVehicleDoorShut(vehFrontBack, 5, false)
                                ESX.UI.Menu.CloseAll()
                            end)
                    end)
            else
                ESX.UI.Menu.Open(
                    'dialog', GetCurrentResourceName(), 'inventory_item_count_give',
                    {
                        title = 'mnozstvi'
                    },
                    function(data2, menu2)

                        local quantity = tonumber(data2.value)
                        PlayerData = ESX.GetPlayerData()
                        vehFront = VehicleInFront()

                        --test
                        local overLimit = false


                        for i=1, #PlayerData.inventory, 1 do

                            if PlayerData.inventory[i].name == data.current.value then
                                if tonumber(PlayerData.inventory[i].limit) < tonumber(PlayerData.inventory[i].count) + quantity and PlayerData.inventory[i].limit ~= -1 then
                                    overLimit = true
                                else
                                    overLimit = false
                                end
                            end
                        end

                        --fin test

                        if quantity > 0 and quantity <= tonumber(data.current.count) and vehFront > 0 then
                            if not overLimit then
                                TriggerServerEvent('esx_vehicle_inventory:removeInventoryItem', GetVehicleNumberPlateText(vehFront), data.current.value, quantity)
                            else
                                ESX.ShowNotification('~r~Tolik toho neuneses')
                            end
						elseif vehFront <= 0 then
						    ESX.ShowNotification('~r~Prilis ses vzdalil od kufru')
                        else
                            ESX.ShowNotification('~r~Neplatne mnozstvi')
                        end

                        ESX.UI.Menu.CloseAll()

                        local vehFront = VehicleInFront()
                        if vehFront > 0 then
                            ESX.SetTimeout(300, function()
                                TriggerServerEvent("esx_vehicle_inventory:getInventory", GetVehicleNumberPlateText(vehFront))
                            end)
                        else
                            SetVehicleDoorShut(vehFrontBack, 5, false)
                        end
                    end,
                    function(data2, menu2)
                        SetVehicleDoorShut(vehFrontBack, 5, false)
                        ESX.UI.Menu.CloseAll()
                    end
                )
            end
        end)
end)

RegisterNetEvent('esx_vehicle_inventory:setWeaponsInventoryLoaded')
AddEventHandler('esx_vehicle_inventory:setWeaponsInventoryLoaded', function(inventory)
    local elements = {}
    local vehFrontBack = VehicleInFront()

    table.insert(elements, {
        label     = '-- [Vlozit zbran] --',
        count     = 0,
        value     = 'deposit',
    })
	
    if inventory ~= nil and #inventory > 0 then
        for i=1, #inventory, 1 do
            if inventory[i].ammo > 0 then
                table.insert(elements, {
                    label     = ESX.GetWeaponLabel(inventory[i].weapon) .. ' [Naboje: ' .. inventory[i].ammo .. ']',
                    count     = inventory[i].ammo,
                    value     = inventory[i].weapon,
					id        = inventory[i].id,
                })
			elseif inventory[i].ammo <= 0 then
                table.insert(elements, {
                    label     = ESX.GetWeaponLabel(inventory[i].weapon) .. ' [Bez naboju]',
                    count     = inventory[i].ammo,
                    value     = inventory[i].weapon,
					id        = inventory[i].id,
                })
            end
        end
    end

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'inventory_weapon_deposit',
        {
            title    = 'Zbrane v kufru',
            align    = 'bottom-right',
            elements = elements,
        },
        function(data, menu)
            if data.current.value == 'deposit' then
                local elem = {}
                PlayerData = ESX.GetPlayerData()
				
				local weaponList = ESX.GetWeaponList()
				local playerPed  = GetPlayerPed(-1)

				for i=1, #weaponList, 1 do

					local weaponHash = GetHashKey(weaponList[i].name)

						if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
							local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
							if ammo > 0 then
								table.insert(elem, {label = weaponList[i].label .. ' [Naboje: ' .. ammo .. ']', value = weaponList[i].name, ammocnt = ammo})
							else
								table.insert(elem, {label = weaponList[i].label .. ' [Bez naboju]', value = weaponList[i].name, ammocnt = ammo})
							end
					end
				end
				
                ESX.UI.Menu.Open(
                    'default', GetCurrentResourceName(), 'inventory_weapons_player',
                    {
                        title    = 'Tvoje zbrane',
                        align    = 'bottom-right',
                        elements = elem,
                    },function(data3, menu3)
                            vehFront = VehicleInFront()

                            --fin test
                            if vehFront > 0 then
                                TriggerServerEvent('esx_vehicle_inventory:addWeaponsInventoryItem', GetVehicleNumberPlateText(vehFront), data3.current.value, data3.current.ammocnt)
                                ESX.ShowNotification('~g~Zbran uspesne vlozena do kufru')
							elseif vehFront <= 0 then
						        ESX.ShowNotification('~r~Prilis ses vzdalil od kufru')
                            end

                            ESX.UI.Menu.CloseAll()
								
							local vehFront = VehicleInFront()
                            if vehFront > 0 then
                                ESX.SetTimeout(300, function()
                                    TriggerServerEvent("esx_vehicle_inventory:getWeaponsInventory", GetVehicleNumberPlateText(vehFront))
                                end)
                            else
                                SetVehicleDoorShut(vehFrontBack, 5, false)
                            end
                    end)
            else
                PlayerData = ESX.GetPlayerData()
                vehFront = VehicleInFront()

                --fin test

                if vehFront > 0 then
                    TriggerServerEvent('esx_vehicle_inventory:removeWeaponsInventoryItem', GetVehicleNumberPlateText(vehFront), data.current.id, data.current.value, data.current.count)
				elseif vehFront <= 0 then
				    ESX.ShowNotification('~r~Prilis ses vzdalil od kufru')
                end

                ESX.UI.Menu.CloseAll()

                local vehFront = VehicleInFront()
                if vehFront > 0 then
                    ESX.SetTimeout(300, function()
                        TriggerServerEvent("esx_vehicle_inventory:getWeaponsInventory", GetVehicleNumberPlateText(vehFront))
                    end)
                else
                    SetVehicleDoorShut(vehFrontBack, 5, false)
                end
            end
        end)
end)
