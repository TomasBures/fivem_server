CREATE TABLE `vehicle_inventory` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`plate` VARCHAR(12) NOT NULL,
	`item` VARCHAR(50) NOT NULL,
	`itemcount` int(11) NOT NULL,

	PRIMARY KEY (`id`)
);

ALTER TABLE `vehicle_inventory` ADD UNIQUE(`item`, `plate`);

CREATE TABLE `vehicle_inventory_weapons` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`plate` VARCHAR(12) NOT NULL,
	`weapon` VARCHAR(50) NOT NULL,
	`ammo` int(11) NOT NULL,

	PRIMARY KEY (`id`)
);
