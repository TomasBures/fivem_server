--Truck
Config	=	{}

Config.InventoryItemsLimit = 50

Config.VehicleLimit = {
[0] = 30, --Compact
[1] = 40, --Sedan
[2] = 70, --SUV
[3] = 25, --Coupes
[4] = 30, --Muscle
[5] = 10, --Sports Classics
[6] = 10, --Sports
[7] = 10, --Super
[8] = 5, --Motorcycles
[9] = 70, --Off-road
[10] = 250, --Industrial
[11] = 70, --Utility
[12] = 100, --Vans
[13] = 0, --Cycles
[14] = 50, --Boats
[15] = 20, --Helicopters
[16] = 200, --Planes
[17] = 40, --Service
[18] = 40, --Emergency
[19] = 100, --Military
[20] = 250, --Commercial
[21] = 200, --Trains
}