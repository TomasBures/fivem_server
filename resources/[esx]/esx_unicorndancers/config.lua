Config = {}

Config.Ped = {

  {label='Stripterka 1',type=5, hash='s_f_y_stripper_01'},
  {label='Stripterka 2',type=5, hash='s_f_y_stripper_02'},
  {label='Stripterka Tlusta',type=5, hash='a_f_m_fatcult_01'},
  {label='Stripter 1',type=5, hash='a_m_y_musclbeac_01'},
  {label='Stripter Sado Maso',type=5, hash='a_m_m_acult_01'},
}

Config.Salle = {
  scene1 = {
    label = 'Leva zadni scena',
    pos = {x=101.884, y=-1289.77, z=29.258, a=290.598},
  },
  scene2 = {
    label = 'Prava zadni scena',
    pos = {x=104.6779, y=-1295.2697, z=29.258, a=297.289},
  },
  scene3 = {
    label = 'Prostredni zadni scena',
    pos = {x=104.0453, y=-1292.199, z=29.258, a=298.912},
  },
  scene4 = {
    label = 'Prostredni scena',
    pos = {x=107.359, y=-1290.2869, z=28.8587, a=297.33},
  },
  scene5 = {
    label = 'Leva predni scena',
    pos = {x=112.0371 ,y=-1286.2375, z=28.4586, a=30.04},
  },
  scene6 = {
    label = 'Prava predni scena',
    pos = {x=113.205, y=-1288.293, z=28.4586, a=211.88},
  }
}

Config.Dict = {
  show1={
    label = 'Tanec na kline 1',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p1',
    anim ='ld_girl_a_song_a_p1_f',
  },
  show2={
    label = 'Tanec na kline 2',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p2',
    anim ='ld_girl_a_song_a_p2_f',
  },
  show3={
    label = 'Tanec na kline 3',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p3',
    anim ='ld_girl_a_song_a_p3_f',
  },
  show4={
    label = 'Tanec u tyce',
    name = 'mini@strip_club@pole_dance@pole_dance1',
    anim ='pd_dance_0',
  },
}
Config.Zones = {

      Pos = { x = 122.35705566406, y = -1281.4152832031, z = 29.480518341064},
}
