# core_rpchat
FXServer ESX Core RP Chat


#### Description
Heavy edited RP chat.

#### Requirements
- [esx_identity](https://github.com/ESX-Org/esx_identity)

#### Installation

1) Add `start core_rpchat` to your server.cfg