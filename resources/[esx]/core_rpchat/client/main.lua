

--[[ 
██████╗ ██████╗ ██████╗ ███████╗██████╗ ██████╗ 
██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔══██╗██╔══██╗
██║     ██║   ██║██████╔╝█████╗  ██████╔╝██████╔╝
██║     ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██╔═══╝ 
╚██████╗╚██████╔╝██║  ██║███████╗██║  ██║██║     
 ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝     
                                                 
██████╗ ███████╗██████╗ ███╗   ██╗██╗██╗  ██╗    
██╔══██╗██╔════╝██╔══██╗████╗  ██║██║██║ ██╔╝    
██████╔╝█████╗  ██████╔╝██╔██╗ ██║██║█████╔╝     
██╔═══╝ ██╔══╝  ██╔══██╗██║╚██╗██║██║██╔═██╗     
██║     ███████╗██║  ██║██║ ╚████║██║██║  ██╗    
╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝
]]   

ESX                           = nil
local docUsed = false
local displaying = {}
local currenttxt = {}
local displayingProS = {}
local currenttxtProS = {}
local show3d = true

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
    Citizen.Wait(1000)
  while ESX.GetPlayerData().job == nil do
    Citizen.Wait(1000)
  end

  PlayerData = ESX.GetPlayerData()
      Citizen.Wait(1000)
end)


RegisterNetEvent('sendProximityMessage')
AddEventHandler('sendProximityMessage', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    --TriggerEvent('chatMessage', "" .. name .. "", {111, 111, 111}, " " .. message)
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; color: rgba(255,255,255,0.6);background-color: rgba(0, 0, 0, 0.2); border-radius: 10px;"><i class="fas fa-times-circle"></i> {0} - {1} </div>',
        args = { name, message }
    })             
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    --TriggerEvent('chatMessage', "" .. name .. "", {111, 111, 111}, " " .. message)
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; color: rgba(255,255,255,0.6);background-color: rgba(0, 0, 0, 0.2); border-radius: 10px;"><i class="fas fa-times-circle"></i> {0} - {1} </div>',
        args = { name, message }
    })             
  end
end)

RegisterNetEvent('sendProximityMessageMe')
AddEventHandler('sendProximityMessageMe', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if(message == "") then
    if(pid == myId) then
      TriggerEvent("chatMessage", "^1System", {255, 0, 0}, "Prázdná zpráva!")
    end
    return
  end  
  if pid == myId then
    displayingProS[myId.."player"] = false
    TriggerServerEvent('3dme:shareDisplayProS', message)    
    --TriggerEvent('chatMessage', "", {230, 180, 255}, "* " .. name .." " .. message)
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 114, 232, 0.5); border-radius: 10px;"><i class="fas fa-walking"></i> {0} {1}</div>',
        args = { name, message }
    })    
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    displayingProS[pid.."player"] = false    
    --TriggerEvent('chatMessage', "", {230, 180, 255}, "* " .. name .." " .. message)
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 114, 232, 0.5); border-radius: 10px;"><i class="fas fa-walking"></i> {0} {1}</div>',
        args = { name, message }
    })        
  end
end)

RegisterNetEvent('sendProximityMessageDo')
AddEventHandler('sendProximityMessageDo', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if(message == "") then
    if(pid == myId) then
      TriggerEvent("chatMessage", "^1System", {255, 0, 0}, "Prázdná zpráva!")
    end
    return
  end
  if pid == myId then
    displaying[myId.."player"] = false
    TriggerServerEvent('3dme:shareDisplay', message)
    --TriggerEvent('chatMessage', "", {255, 255, 150}, "*" .. message .. " [" .. name .. "]")
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 232, 114, 0.5); border-radius: 10px;"><i class="fas fa-dot-circle"></i> {1} [{0}]</div>',
        args = { name, message }
    })           
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 13.999 then
    displaying[pid.."player"] = false
    --TriggerEvent('chatMessage', "", {255, 255, 150}, "*" .. message .. " [" .. name .. "]")
    TriggerEvent('chat:addMessage', {
        template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 232, 114, 0.5); border-radius: 10px;"><i class="fas fa-dot-circle"></i> {1} [{0}]</div>',
        args = { name, message }
    })           
  end
end)



AddEventHandler('esx-qalle-chat:me', function(id, name, message)
    local myId = PlayerId()
    local pid = GetPlayerFromServerId(id)

    if pid == myId then
        TriggerEvent('chat:addMessage', {
            template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(86, 125, 188, 0.6); border-radius: 3px;"><i class="fas fa-user-circle"></i> {0}:<br> {1}</div>',
            args = { name, message }
        })
    elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 15.4 then
        TriggerEvent('chat:addMessage', {
            template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(86, 125, 188, 0.6); border-radius: 3px;"><i class="fas fa-user-circle"></i> {0}:<br> {1}</div>',
            args = { name, message }
        })
    end
end)

RegisterNetEvent('sendProximityMessageDoc')
AddEventHandler('sendProximityMessageDoc', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if(docUsed) then
    if pid == myId then
     ESX.ShowNotification('~r~Pocitadlo jiz bezi!')        
    end
    return
  end
  if (tonumber(message) == nil) then
    if pid == myId then
     ESX.ShowNotification('~r~Musi byt cislo!')  
    end
    return
  end
  if(tonumber(message) > 50) then
    if pid == myId then
     ESX.ShowNotification('~r~Maximalni mnozstvi je 50')          
    end
    return
  end
  if(tonumber(message) < 2) then
    if pid == myId then
      ESX.ShowNotification('~r~Minimalni mnozstvi je 2')      
    end
    return
  end
  if pid == myId then
  docUsed = true
  end
  Citizen.CreateThread(function()
    for i=1, tonumber(message) do
      if pid == myId then
	    displaying[myId.."player"] = false
	    TriggerServerEvent('3dme:shareDisplay', i .. "/" ..message)          	      
        TriggerEvent('chat:addMessage', {
            template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 232, 114, 0.5); border-radius: 10px;"><i class="fas fa-user-circle"></i> {0}/{1} [{2}]</div>',
            args = { i, message, name }
        })        
      elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 13.999 then
	    displaying[pid.."player"] = false
        TriggerEvent('chat:addMessage', {
            template = '<div style="padding: 0.5vw; margin: 0.05vw; background-color: rgba(232, 232, 114, 0.5); border-radius: 10px;"><i class="fas fa-user-circle"></i> {0}/{1} [{2}]</div>',
            args = { i, message, name }
        })          
      end
      if(i == tonumber(message)) then
      docUsed = false
      end
      Citizen.Wait(1000)
    end
  end)
end)


local color = { r = 255, g = 255, b = 150, alpha = 255}
local colorS = { r = 230, g = 180, b = 255, alpha = 255}
local background = {
    enable = true,
    color = { r = 0, g = 0, b = 0, alpha = 100 },
}
local time = 8000
local timeProS = 9000
local backgroundProS = {
    enable = true,
    color = { r = 0, g = 0, b = 0, alpha = 100 },
}
RegisterFontFile('out')
fontId = RegisterFontId('CoreRPFont')
local font = fontId
local chatMessage = false
local dropShadow = true

-- Don't touch
local nbrDisplaying = 1

RegisterNetEvent('3dme:triggerDisplay')
AddEventHandler('3dme:triggerDisplay', function(text, source)
    if not show3d then
      return
    end
    local offset = 1 + (nbrDisplaying*0.14)
    currenttxt[GetPlayerFromServerId(source).."player"] = text
    Display(GetPlayerFromServerId(source), text, offset)
end)


RegisterNetEvent('3dme:triggerDisplayProS')
AddEventHandler('3dme:triggerDisplayProS', function(text, source)
    local offset = 1 + (nbrDisplaying*0.14)
    currenttxtProS[GetPlayerFromServerId(source).."player"] = text
    DisplayProS(GetPlayerFromServerId(source), text, offset)
end)


RegisterNetEvent('sendProximityMessage3D')
AddEventHandler('sendProximityMessage3D', function(id)
  local myId = PlayerId()

  if show3d then
    show3d = false
    TriggerEvent("chatMessage", "^1System", {255, 0, 0}, "Zobrazování 3D textů vypnuto.")
  else
    show3d = true
     TriggerEvent("chatMessage", "^1System", {255, 0, 0}, "Zobrazování 3D textů zapnuto.")
  end
end)

function Display(mePlayer, text, offset)
    displaying[mePlayer.."player"] = true
    Citizen.CreateThread(function()
        Wait(time)
        if(currenttxt[mePlayer.."player"] == text) then
          displaying[mePlayer.."player"] = false
        end
    end)
    Citizen.CreateThread(function()
        nbrDisplaying = nbrDisplaying + 1
        while displaying[mePlayer.."player"] do
            Wait(0)
            local coordsMe = GetEntityCoords(GetPlayerPed(mePlayer), false)
            local coords = GetEntityCoords(PlayerPedId(), false)
            local dist = Vdist2(coordsMe, coords)
            if dist < 200 then
                DrawText3D(coordsMe['x'], coordsMe['y'], coordsMe['z']+offset, text)
            end
        end
        nbrDisplaying = nbrDisplaying - 1
    end)
end

function DrawText3D(x,y,z, text)
    z = z - 1.2

    local onScreen,_x,_y = World3dToScreen2d(x,y,z)
    local px,py,pz = table.unpack(GetGameplayCamCoord())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)

    --local scale = ((1/dist)*2)*(1/(GetGameplayCamFov()))*60
    local scale = 0.68
    if onScreen then

        -- Formalize the text
        SetTextColour(color.r, color.g, color.b, color.alpha)
        SetTextScale(0.0*scale, 0.55*scale)
        SetTextFont(font)
        SetTextProportional(1)
        SetTextCentre(true)
        if dropShadow then
            SetTextDropshadow(10, 100, 100, 100, 255)
        end

        -- Calculate width and height
        local height = GetTextScaleHeight(0.55*scale, font)
        local width = utf8.len(text)*0.0032

        -- Diplay the text
        SetTextEntry("STRING")
        AddTextComponentString(text)
        EndTextCommandDisplayText(_x, _y)

        if background.enable then
            DrawRect(_x, _y+scale/45, width + 0.030, height + 0.010, background.color.r, background.color.g, background.color.b , background.color.alpha)
        end
    end
end

function DisplayProS(mePlayer, text, offset)
    displayingProS[mePlayer.."player"] = true
    Citizen.CreateThread(function()
        Wait(timeProS)
        if(currenttxtProS[mePlayer.."player"] == text) then
          displayingProS[mePlayer.."player"] = false
        end
    end)
    Citizen.CreateThread(function()
        nbrDisplaying = nbrDisplaying + 1
        while displayingProS[mePlayer.."player"] do
            Wait(0)
            local coordsMe = GetEntityCoords(GetPlayerPed(mePlayer), false)
            local coords = GetEntityCoords(PlayerPedId(), false)
            local dist = Vdist2(coordsMe, coords)
            if dist < 200 then
                DrawText3DProS(coordsMe['x'], coordsMe['y'], coordsMe['z']+offset, text)
            end
        end
        nbrDisplaying = nbrDisplaying - 1
    end)
end

function DrawText3DProS(x,y,z, text)
    z = z - 0.2
    local onScreen,_x,_y = World3dToScreen2d(x,y,z)
    local px,py,pz = table.unpack(GetGameplayCamCoord())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)

    local scale = 0.68

    if onScreen then

        -- Formalize the text
        SetTextColour(colorS.r, colorS.g, colorS.b, colorS.alpha)
        SetTextScale(0.0*scale, 0.55*scale)
        SetTextFont(font)
        SetTextProportional(1)
        SetTextCentre(true)
        if dropShadow then
            SetTextDropshadow(10, 100, 100, 100, 255)
        end

        -- Calculate width and height
        local height = GetTextScaleHeight(0.55*scale, font)
        local width = utf8.len(text)*0.0032

        -- Diplay the text
        SetTextEntry("STRING")
        AddTextComponentString(text)
        EndTextCommandDisplayText(_x, _y)

        if backgroundProS.enable then
            DrawRect(_x, _y+scale/45, width + 0.030, height + 0.010, backgroundProS.color.r, backgroundProS.color.g, backgroundProS.color.b , backgroundProS.color.alpha)
        end
    end
end