

--[[ 
██████╗ ██████╗ ██████╗ ███████╗██████╗ ██████╗ 
██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔══██╗██╔══██╗
██║     ██║   ██║██████╔╝█████╗  ██████╔╝██████╔╝
██║     ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██╔═══╝ 
╚██████╗╚██████╔╝██║  ██║███████╗██║  ██║██║     
 ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝     
                                                 
██████╗ ███████╗██████╗ ███╗   ██╗██╗██╗  ██╗    
██╔══██╗██╔════╝██╔══██╗████╗  ██║██║██║ ██╔╝    
██████╔╝█████╗  ██████╔╝██╔██╗ ██║██║█████╔╝     
██╔═══╝ ██╔══╝  ██╔══██╗██║╚██╗██║██║██╔═██╗     
██║     ███████╗██║  ██║██║ ╚████║██║██║  ██╗    
╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝
]]  

ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)



--SystemName = 'SERVER MONITORING'
--SystemAvatar = 'https://www.freepngimg.com/thumb/twitter/2-2-twitter-png-file-thumb.png'
--BlackmarketAvatar = 'https://cdn0.iconfinder.com/data/icons/flat-round-system/512/tor-512.png'
--WeazelNewsAvatar = 'https://pbs.twimg.com/profile_images/958448695749632000/t9KaSZAX.jpg'

function getIdentity(source)
	local identifier = GetPlayerIdentifiers(source)[1]
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {['@identifier'] = identifier})
	if result[1] ~= nil then
		local identity = result[1]

		return {
			identifier = identity['identifier'],
			firstname = identity['firstname'],
			lastname = identity['lastname'],
			dateofbirth = identity['dateofbirth'],
			sex = identity['sex'],
			height = identity['height'],
      name = identity['name'],
      job = identity['job']
			
		}
	else
		return nil
	end
end
 

AddEventHandler('chatMessage', function(source, name, message)
  if string.sub(message, 1, string.len("/")) ~= "/" then
    local name = getIdentity(source)
    TriggerClientEvent("sendProximityMessage", -1, source, "[L-OOC] " .. name.firstname .. " " .. name.lastname, message)
    setLog(message, source)        
  end
  CancelEvent()
end)
  
  -- TriggerEvent('es:addCommand', 'me', function(source, args, user)
  --    local name = getIdentity(source)
  --    TriggerClientEvent("sendProximityMessageMe", -1, source, name.firstname, table.concat(args, " "))
  -- end) 

TriggerEvent('es:addCommand', 'me', function(source, args, user)
  --table.remove(args, 1)
    local name = getIdentity(source)
    TriggerClientEvent("sendProximityMessageMe", -1, source, name.firstname .. " " .. name.lastname, table.concat(args, " "))
end)

TriggerEvent('es:addCommand', 'do', function(source, args, user)
  --table.remove(args, 1)
  local name = getIdentity(source)
  TriggerClientEvent("sendProximityMessageDo", -1, source, name.firstname .. " " .. name.lastname, table.concat(args, " "))
end)


  --- TriggerEvent('es:addCommand', 'me', function(source, args, user)
  ---    local name = getIdentity(source)
  ---    TriggerClientEvent("sendProximityMessageMe", -1, source, name.firstname, table.concat(args, " "))
  -- end) 
--[[  TriggerEvent('es:addCommand', 'me', function(source, args, user)
    local name = getIdentity(source)
    table.remove(args, 2)
    TriggerClientEvent('esx-qalle-chat:me', -1, source, name.firstname, name.lastname, table.concat(args, " "))
end)]]


TriggerEvent('es:addCommand', 'doc', function(source, args, user)
  --table.remove(args, 1)
  local name = getIdentity(source)
  TriggerClientEvent("sendProximityMessageDoc", -1, source, name.firstname .. " " .. name.lastname, table.concat(args, " "))
end)

RegisterCommand('pd', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(3)
    local name = getIdentity(source)
    local xPlayer = ESX.GetPlayerFromId(source)      
    fal = name.firstname .. "" .. name.lastname
   -- local _source = source
   -- local xPlayer = ESX.GetPlayerFromId(_source)
   --for k, v in ipairs(GetPlayerIdentifiers(source)) do
   --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
   --        senderidentifierDiscord = v
   --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
   --        
   --    end
   --end	      
    if xPlayer.job.name and xPlayer.job.name == 'police' then
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(0, 0, 255, 0.6); border-radius: 3px;"><i class="fas fa-briefcase"></i> <b>LSPD</b><br> {0}</div>',
        args = { msg }
    })
    setLog(msg, source)        
    else 
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	          	
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'PD CHAT ALERT', '**NĚKDO ZKOUŠÍ ZNEUŽÍT PD CHAT!!**\n **' .. senderdcname .. '**\n ' .. msg .. '\n', BlackmarketAvatar, false)		    	
	    TriggerClientEvent('chat:addMessage', source, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(255, 0, 0, 0.7); border-radius: 3px;"><i class="fas fa-exclamation-triangle"></i> <b>TO MYSLÍŠ VÁŽNĚ?</b> <i class="fas fa-exclamation-triangle"></i><br>^7 PŘIPADÁŠ SI SNAD JAKO POLICISTA?</div>',
        args = { msg }
    })
    end    
end, false)

RegisterCommand('ems', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(4)
    local name = getIdentity(source)  
    fal = name.firstname .. "" .. name.lastname
   -- local _source = source
	local xPlayer = ESX.GetPlayerFromId(source)
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	      
    if xPlayer.job.name and (xPlayer.job.name == 'ambulance' or xPlayer.job.name == 'fire') then
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background: linear-gradient( to bottom, rgba( 255, 0, 0, 0.69 ), rgba( 255, 255, 255, 0.66 ) ); border-radius: 3px;"><i class="fas fa-briefcase"></i> <b>EMS</b><br> {0}</div>',
        args = { msg }
    })
    setLog(msg, source)        
    else 
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	          	
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'EMS CHAT ALERT', '**NĚKDO ZKOUŠÍ ZNEUŽÍT PD CHAT!!**\n **' .. senderdcname .. '**\n ' .. msg .. '\n', BlackmarketAvatar, false)		    	
	    TriggerClientEvent('chat:addMessage', source, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(255, 0, 0, 0.7); border-radius: 3px;"><i class="fas fa-exclamation-triangle"></i> <b>TO MYSLÍŠ VÁŽNĚ?</b> <i class="fas fa-exclamation-triangle"></i><br>^7 PŘIPADÁŠ SI SNAD JAKO DOKTOR?</div>',
        args = { msg }
    })
    end    
end, false)

 RegisterCommand('twt', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(4)
    local name = getIdentity(source)
    kdo = GetPlayerName(source)
    fal = name.firstname .. "" .. name.lastname
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	    
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'Twitter Main #1', '**' .. senderdcname .. '**\n **@' .. fal ..'**: ' .. msg .. '\n', SystemAvatar, false)
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookTwitter, fal, msg .. '\n', SystemAvatar, false)    
 
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(28, 160, 242, 0.6); border-radius: 25px;"><i class="fab fa-twitter"></i> @{0}: {1}</div>',
       args = { fal, msg }
    })
    setLog(msg, source)        
end, false)

 RegisterCommand('anon', function(source, args, rawCommand)
	local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(5)
    local name = getIdentity(source)
    fal = name.firstname .. " " .. name.lastname
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	       
	if name.job ~= nil and name.job ~= 'police' then     
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'Blackmarket Main #1', '**' .. senderdcname .. '**\n **' .. fal ..'**:\n ' .. msg .. '\n', BlackmarketAvatar, false)
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookTOR, 'TOR', msg .. '\n', BlackmarketAvatar, false)     
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(41, 41, 41, 0.6); border-radius: 3px;"><i class="fas fa-skull-crossbones"></i> Anonymous: ^7 {1}</div>',
        args = { fal, msg }
    })
    setLog(msg, source)        
	else
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	     				
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'Blackmarket Main #1', '**POLICISTA ZNEUŽIL BLACKMARKET!!**\n **' .. senderdcname .. '**\n **' .. fal ..'**:\n ' .. msg .. '\n', BlackmarketAvatar, false)		
	    TriggerClientEvent('chat:addMessage', source, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(255, 0, 0, 0.7); border-radius: 3px;"><i class="fas fa-exclamation-triangle"></i> <b>TO MYSLÍŠ VÁŽNĚ?</b> <i class="fas fa-exclamation-triangle"></i><br>^7 POLICIE NEMÁ PŘÍSTUP K BLACKMARKETU!!</div>',
        args = { fal, msg }
    })
	end
end, false)


 RegisterCommand('news', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(5)
    local name = getIdentity(source)
    fal = name.firstname .. " " .. name.lastname
    --if name.job ~= nil and name.job == 'lifeinvader' then   
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	        
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'News Main #1', '**' .. senderdcname .. '**\n **' .. fal ..'**:\n ' .. msg .. '\n', WeazelNewsAvatar, false)
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookNews, 'Weazel News', msg .. '\n', WeazelNewsAvatar, false)     
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(255, 0, 0, 0.7); border-radius: 3px;"><i class="fas fa-broadcast-tower"></i> <b>WeazelNews</b><br>^7 {1}</div>',
        args = { fal, msg }
    })
	--else
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	     		
	--TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'News Main #1', '**NĚKDO ZNEUŽIL /NEWS!!**\n **' .. senderdcname .. '**\n **' .. fal ..'**:\n ' .. msg .. '\n', WeazelNewsAvatar, false)				
	--    TriggerClientEvent('chat:addMessage', source, {
    --    template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(255, 0, 0, 0.7); border-radius: 3px;"><i class="fas fa-exclamation-triangle"></i> <b>TO MYSLÍŠ VÁŽNĚ?</b> <i class="fas fa-exclamation-triangle"></i><br>^7 PŘIPADÁŠ SI SNAD JAKO NOVINÁŘ?</div>',
    --    args = { fal, msg }
    --})
	--end
    setLog(msg, source)        
end, false)

 RegisterCommand('advert', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(8)
    local name = getIdentity(source)
    fal = name.firstname .. " " .. name.lastname
    --for k, v in ipairs(GetPlayerIdentifiers(source)) do
    --    if string.sub(v, 1, string.len("discord:")) == "discord:" then
    --        senderidentifierDiscord = v
    --        senderdcname = '<@' .. string.sub(senderidentifierDiscord, 9) .. '>'
    --        
    --    end
    --end	        
	----TriggerEvent('DiscordBot:ToDiscord', DiscordWebhookSystemInfos, 'Advert Main #1', '**' .. senderdcname .. '**\n **' .. fal ..'**:\n ' .. msg .. '\n', WeazelNewsAvatar, false)
	--TriggerEvent('DiscordBot:ToDiscord', 'hook', 'Lifeinvader ADVERT', msg .. '\n', WeazelNewsAvatar, false)       
    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.5vw; margin: 0.5vw; background-color: rgba(214, 168, 0, 1); border-radius: 3px;"><i class="fas fa-ad"></i><b> LifeInvader Ads</b><br> {1}<br></div>',
        args = { fal, msg }
    })
    setLog(msg, source)        
end, false)

TriggerEvent('es:addCommand', '3d', function(source)
  local name = getIdentity(source)
  TriggerClientEvent("sendProximityMessage3D", source, source)
end,  {help = 'Vypne/zapne box u /do'})

RegisterCommand('ooc', function(source, args, rawCommand)
    local playerName = GetPlayerName(source)
    local msg = rawCommand:sub(5)
    local name = getIdentity(source)


    TriggerClientEvent('chat:addMessage', -1, {
        template = '<div style="padding: 0.2vw; margin: 0.2vw; background-color: rgba(67, 47, 71, 0.4); border-radius: 3px;"><i class="fas fa-globe"></i> [OOC] {0}<br>^7 {1}</div>',
        args = { playerName, msg }
    })
    setLog(msg, source)     
end, false)

RegisterServerEvent('3dme:shareDisplay')
AddEventHandler('3dme:shareDisplay', function(text)
  TriggerClientEvent('3dme:triggerDisplay', -1, text, source)
    setLog(text, source)  
end)

RegisterServerEvent('3dme:shareDisplayProS')
AddEventHandler('3dme:shareDisplayProS', function(text)
  TriggerClientEvent('3dme:triggerDisplayProS', -1, text, source)
    setLog(text, source)    
end)


function setLog(text, source)
  local time = os.date("%d/%m/%Y %X")
  local name = GetPlayerName(source)
  local identifier = GetPlayerIdentifiers(source)
  local data = time .. ' : ' .. name .. ' - ' .. identifier[1] .. ' : ' .. text

  local content = LoadResourceFile(GetCurrentResourceName(), "log.txt")
  local newContent = content .. '\r\n' .. data
  SaveResourceFile(GetCurrentResourceName(), "log.txt", newContent, -1)
end

function stringsplit(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t={} ; i=1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end
