Locales['en'] = {
	-- shop
	['boat_shop'] = 'Obchod s lodemi',
	['boat_shop_open'] = 'Stiskni ~INPUT_CONTEXT~ pro otevreni ~y~obchodu s lodemi~s~.',
	['boat_shop_confirm'] = 'koupit <span style="color: yellow;">%s</span> za <span style="color: orange;">€%s</span>?',
	['boat_shop_bought'] = '~y~koupil jsi~s~ ~b~%s~s~ za ~g~€%s~s~',
	['boat_shop_nomoney'] = 'Nemuzes si dovolit tuto lod!',
	['confirm_no'] = 'ne',
	['confirm_yes'] = 'ano',

	-- garage
	['garage'] = 'garaz lodi',
	['garage_open'] = 'Stiskni ~INPUT_CONTEXT~ pro otevreni ~y~garaze lodi~s~.',
	['garage_store'] = 'Stiskni ~INPUT_CONTEXT~ pro ~y~ulozeni~s~ lode do garaze.',
	['garage_taken'] = 'lod vytazena z garaze!',
	['garage_stored'] = 'lod byla bezpecne ulozena do garaze!',
	['garage_noboats'] = 'Nemas zadnou lod v garazi! Navstiv ~y~obchod s lodemi~s~ a nejakou si kup.',
	['garage_blocked'] = 'Lod neni mozne vytahnout z garaze. Jina lod blokuje spawn point!',
	['garage_notowner'] = 'Tahle lod ti nepatri!',

	-- license
	['license_menu'] = 'Koupit lodni licenci?',
	['license_buy_no'] = 'ne',
	['license_buy_yes'] = 'koupit lodni licenci <span style="color: green;">€%s</span>',
	['license_bought'] = 'Koupil jsi ~y~lodni licenci~s~ za ~g~€%s~s~',
	['license_nomoney'] = 'nemuzes si dovolit ~y~lodni licenci~s~!',

	-- blips
	['blip_garage'] = 'garaz lodi',
	['blip_shop'] = 'obchod s lodemi',
}