Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.GangStations = {

  Gang = {

    Blip = {
--      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
     { name = 'WEAPON_COMBATPISTOL',     price = 4000 },
     { name = 'WEAPON_APPISTOL',         price = 19000 },
	 { name = 'WEAPON_PISTOL50',         price = 6000 },
	 { name = 'WEAPON_MACHETE',          price = 2000 },
	 { name = 'WEAPON_SWITCHBLADE',      price = 500 },
	 { name = 'WEAPON_POOLCUE',          price = 800 },
	 { name = 'WEAPON_BATTLEAXE',        price = 3000 },
	 { name = 'WEAPON_KNUCKLE',          price = 200 },
	 { name = 'WEAPON_CROWBAR',          price = 1000 },
	 { name = 'WEAPON_DAGGER',           price = 1500 },
	 { name = 'WEAPON_MICROSMG',         price = 20000 },
	 { name = 'WEAPON_MACHINEPISTOL',    price = 17000 },
	 { name = 'WEAPON_COMPACTRIFLE',     price = 30000 },
	 { name = 'WEAPON_DBSHOTGUN',        price = 22000 },
	 
    },

	  AuthorizedVehicles = {
		  { name = 'schafter5',  label = 'Véhicule Civil' },
		  { name = 'Akuma',    label = 'Moto' },
		  { name = 'Granger',   label = '4X4' },
		  { name = 'mule3',      label = 'Camion de Transport' },
	  },

    Cloakrooms = {
      { x = -17.79, y = -1439.45, z = 30.1},
    },

    Armories = {
      { x = -255.67, y = -1543.39, z = 30.9},
    },

    Vehicles = {
      {
        Spawner    = { x = -20.1, y = -1444.8, z = 29.61 },
        SpawnPoint = { x = -24.19, y = -1436.09, z = 29.65 },
        Heading    = 181.51,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = -2000.1, y = -14404.8, z = 30.61 },
        SpawnPoint = { x = 112.94457244873, y = -3102.5942382813, z = 5.0050659179688 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -25.23, y = -1426.61, z = 29.65 },
      
    },

    BossActions = {
      { x = -11.07, y = -1428.78, z = 30.1 },
    },

  },

}
