INSERT INTO `addon_account` (name, label, shared) VALUES 
	('society_gang','Gang',1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
	('society_gang','Gang',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES 
	('society_gang', 'Gang', 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('gang', 'Gang', 1);


INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
('gang', 0, 'soldato', 'Clen', 1500, '{}', '{}'),
('gang', 1, 'capo', 'Ochranka', 1800, '{}', '{}'),
('gang', 2, 'dealer', 'Dealer', 2700, '{}', '{}'),
('gang', 3, 'consigliere', 'Viceboss', 2100, '{}', '{}'),
('gang', 4, 'boss', 'Boss', 2700, '{}', '{}');

CREATE TABLE `fine_types_gang` (
  
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  
  PRIMARY KEY (`id`)
);

INSERT INTO `fine_types_gang` (label, amount, category) VALUES 
	('Dluh organizaci',3000,0),
	('Dluh organizaci',5000,0),
	('Dluh organizaci',10000,1),
	('Dluh organizaci',20000,1),
	('Dluh organizaci',50000,2),
	('Dluh organizaci',150000,3),
	('Dluh organizaci',350000,3)
;