ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('LegacyFuel:PayFuel')
AddEventHandler('LegacyFuel:PayFuel', function(price)
	local xPlayer = ESX.GetPlayerFromId(source)
	local amount  = round(price, 0)

	xPlayer.removeMoney(amount)
end)

local Vehicles = {
	{ plate = '87OJP476', fuel = 30}
}

local VehiclesDeltas = {
	{ plate = 'vardefiner', delta = 0}
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(60000)
		
--		print('esx_legacyfuel: Running DB fuelconsumption check')
		
		local sql = ''
	
		for i = 1, #VehiclesDeltas do
			if VehiclesDeltas[i] ~= nil and VehiclesDeltas[i].plate ~= nil and VehiclesDeltas[i].plate ~= 'vardefiner' and VehiclesDeltas[i].delta ~= nil and VehiclesDeltas[i].delta > 0.0 then 
				sql = sql ..  'update owned_vehicles set mileage = mileage + ' .. string.format('%0.2f', VehiclesDeltas[i].delta) .. ' where plate = \'' .. VehiclesDeltas[i].plate .. '\'; '
				VehiclesDeltas[i].delta = 0.0
			end
		end	
	
		if sql ~= nil and sql ~= '' then
			MySQL.Async.execute(sql, {} , function(rowsChanged)	end)
--			print(sql)
--			print('esx_legacyfuel: Fuel deltas found... saved to DB')
		end
	
	end
end)

RegisterServerEvent('LegacyFuel:UpdateServerFuelTable')
AddEventHandler('LegacyFuel:UpdateServerFuelTable', function(plate, fuel, deltasrc)
	local found = false
	local foundDelta = false
	
	for i = 1, #Vehicles do
		if Vehicles[i].plate == plate then 
			found = true
			
			if fuel ~= Vehicles[i].fuel then
				table.remove(Vehicles, i)
				table.insert(Vehicles, {plate = plate, fuel = fuel})
			end
			break 
		end
	end
	
	if not found then
		table.insert(Vehicles, {plate = plate, fuel = fuel})
	end
	
	if deltasrc == nil then
--		print('esx_legacyfuel: fuel update with nil deltasrc PLATE: ' .. plate .. ' NEWFUEL: ' .. tostring(fuel))
	else
		for i = 1, #VehiclesDeltas do		
			if VehiclesDeltas[i].plate == plate then 
				foundDelta = true
			
				VehiclesDeltas[i].delta = VehiclesDeltas[i].delta + deltasrc
				break 
			end
		end
		
		if not foundDelta then
			table.insert(VehiclesDeltas, {plate = plate, delta = deltasrc})
		end
	end
end)

RegisterServerEvent('LegacyFuel:CheckServerFuelTable')
AddEventHandler('LegacyFuel:CheckServerFuelTable', function(plate)
	for i = 1, #Vehicles do
		if Vehicles[i].plate == plate then
			local vehInfo = {plate = Vehicles[i].plate, fuel = Vehicles[i].fuel}

			TriggerClientEvent('LegacyFuel:ReturnFuelFromServerTable', source, vehInfo)

			break
		end
	end
end)

RegisterServerEvent('LegacyFuel:CheckCashOnHand')
AddEventHandler('LegacyFuel:CheckCashOnHand', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local cb 	  = xPlayer.getMoney()

	TriggerClientEvent('LegacyFuel:RecieveCashOnHand', source, cb)
end)

function round(num, numDecimalPlaces)
	return tonumber(string.format("%." .. (numDecimalPlaces or 0) .. "f", num))
end
