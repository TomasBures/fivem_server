var tableauQuestion = [
	{
		question: "Elektrickovym kolajovym pasom sa rozumie",
		propositionA: "Cast cesty urcena predovsetkym na premavku elektricky.",
		propositionB: "Cast cesty, na ktorej je dovolena len premavka elektricky a nemotoroveho vozidla.",
		propositionC: "Vozovka urcena na premavku elektricky, vlaku a ineho drahoveho vozidla.",
		propositionD: "Rychla cesta pre Sup-Sup missiu!",
		reponse: "A"
	},
	
	{
		question: "Znizenou viditelnostou sa rozumie",
		propositionA: "Taka viditelnost, ked vodic motoroveho vozidla z dovodu znecistenia alebo poskodenia skiel nevidi dostatocne zretelne ostatnych ucastnikov cestnej premavky.",
		propositionB: "Viditelnost v case od 18.00 do 7,00 hodiny a od 1. jula do 31. augusta len od 20.00 do 6.00 hodiny.",
		propositionC: "Taka viditelnost, pri ktorej sa ucastnici cestnej premavky dostatocne zretelne navzajom nevidia ani ked nevidia predmety na ceste, najma od sumraku do svitania, za hmly, snezenia, dazda a v tuneli.",
		propositionD: "Nemas oci alebo ich mas zatvorene...",
		reponse: "C"
	},

	{
		question: "Predchadza sa",
		propositionA: "Vpravo; vlavo sa predchadza len motocykel.",
		propositionB: "Vlavo za kazdych okolnosti.",
		propositionC: "Vlavo; vpravo sa predchadza vozidlo, ktore meni smer jazdy vlavo a ak uz nie je pochybnost o dalsom smere jeho jazdy.",
		propositionD: "To je jedno aj tak vzdy do neho naburam...",
		reponse: "C"
	},

	{
		question: "Vodic motoroveho vozidla s najvacsou pripustnou celkovou hmotnostou neprevysujucou 3500 kg smie jazdit rychlostou najviac",
		propositionA: "110km/h; na dialnici a na rychlostnej ceste nema rychlost jazdy obmedzenu.",
		propositionB: "90 km/h; na dialnici a na rychlostnej ceste smie jazdit rychlostou najviac 130 km/h.",
		propositionC: "80 km/h.",
		propositionD: "Kolko to da a hlavne nebrzdit auta za sebou ako 60 dedo!",
		reponse: "B"
	},

	{
		question: "Vozidlo vchadzajuce na cestu z miesta mimo cesty, z polnej cesty alebo z lesnej cesty musi byt vopred ocistene, aby neznecistovalo",
		propositionA: "Miestnu komunikaciu a to len vtedy, ak vchadza na cestu v obci.",
		propositionB: "Cestu.",
		propositionC: "Cestu; to neplati ak vchadza na cestu mimo obce.",
		propositionD: "Kamaratove auto alebo auto hraca pri havarii. Hlavne nech je pekne ked sa odlozi do garaze!",
		reponse: "B"
	},

	{
		question: "Vodic nesmie zastavit a stat",
		propositionA: "Na miestach, kde zastavenie alebo statie moze ohrozit bezpecnost a plynulost cestnej premavky alebo obmedzit jazdu vozidiel.",
		propositionB: "V obci na ceste 1. triedy.",
		propositionC: "Kolmo na okraj cesty v obci.",
		propositionD: "Pred policajtom alebo na policajtovi ked ma nasere kvoli pokute!",
		reponse: "A"
	},

	{
		question: "Spravca cesty moze odstranit vozidlo stojace na ceste vratane chodnika, na naklady jeho prevadzkovatela, ak",
		propositionA: "Je na mieste, aj kde netvori prekazku cestnej premavky.",
		propositionB: "Je na mieste, kde tvori prekazku cestnej premavky.",
		propositionC: "Je ponechane na mieste, kde je dopravnou znackou povolene statie.",
		propositionD: "Nech to skusi ten neznajboh a uvidi sa na kapote mojho auta!",
		reponse: "B"
	},

	{
		question: "Ak pri jazde vozidla v tuneli vznikne porucha vozidla, pre ktoru sa toto vozidlo stane nepojazdnym, alebo ak vznikne dopravna nehoda vratane poziaru, je vodic po zastaveni vozidla povinny bezodkladne",
		propositionA: "Opustit tunel a dopravnu nehodu oznamit najblizsiemu policajtovi.",
		propositionB: "Vykonat vhodne opatrenia, aby nebola ohrozena bezpecnost cestnej premavky v tuneli; ak to okolnosti vyzaduju, je opravneny zastavovat ine vozidla.",
		propositionC: "Zariadit odtiahnutie vozidla z tunela.",
		propositionD: "Ukradnut si auto a pokracovat v jazde, vsak to ja ne!",
		reponse: "B"
	},

	{
		question: "V skolskej zone",
		propositionA: "Nemozno stat.",
		propositionB: "Mozno stat, ak tym nie je obmedzeny pohyb chodcov.",
		propositionC: "Mozno stat bez obmedzenia.",
		propositionD: "Dostrielam vsetky ucitelske auta a posprejujem ich s vyhrazkami - Este jedna 5 a tak skoncis!",
		reponse: "B"
	},

	{
		question: "Vodic motoroveho vozidla nesmie predchadzat ine motorove vozidlo na dialnici",
		propositionA: "Ak by pri predchadzani neobmedzil ine motorove vozidlo iduce po dialnici.",
		propositionB: "Ak nema rozhlad na vzdialenost 200 metrov.",
		propositionC: "Ak by pri predchadzani svojou vyrazne nizsou rychlostou obmedzil ine motorove vozidlo iduce po dialnici.",
		propositionD: "Pokial ide pomalsie ako ja nezajem, idem - riesim!",
		reponse: "C"
	},
]