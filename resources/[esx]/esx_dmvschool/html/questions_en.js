﻿var tableauQuestion = [
	{
		question: "Stat znamena:",
		propositionA: "Uviest vozidlo do kludu na dobu dlhšiu ako pri zastaveni .",
		propositionB: "Prerusit jazdu z dovodu obednej prestavky.",
		propositionC: "Zastavenie vozidla z dovodu zrazky s inym autom",
		propositionD: "Vystupit z auta a postavit sa na kapotu.",
		reponse: "A"
	},
	
	{
		question: "Obytna zona je:",
		propositionA: "Miesto kde je odparkovany obytny karavan.",
		propositionB: "Obec kde zije aspon 10 ludi.",
		propositionC: "Zastavana oblast, ktorej zaciatok je oznaceny dopravnou znackou „Obytna zona“ a koniec je oznaceny dopravnou znackou „Koniec obytnej zony“.",
		propositionD: "Tam kde byva Dezo",
		reponse: "C"
	},

	{
		question: "Pri cerpani pohonnych hmot ma prednost:",
		propositionA: "Vodic vozidla s pravom prednostnej jazdy, pritom nepouziva zvlastne vystrazne znamenia.",
		propositionB: "Vodic vozidla ktore je vacsie.",
		propositionC: "Ten vodic ktory ma pri sebe najviac penazi.",
		propositionD: "Ten nejrychlejsi.",
		reponse: "A"
	},

	{
		question: "Pri preprave nakladu:",
		propositionA: "Musi byt naklad zaistiteny lepiacov paskou.",
		propositionB: "Musis dodrzovat pitny rezim.",
		propositionC: "Nesmie byt prekrocena maximalna pripustna hmotnost vozidla.",
		propositionD: "Nalozis vsetko co sa vojde.",
		reponse: "C"
	},

	{
		question: "Vodicom protiiducich vozidiel sa v pripade potreby vyhybam...",
		propositionA: "Ja sa nevyhybam, nech sa uhnu ostatny.",
		propositionB: "Zastanem v strede nech ma dobre trafí.",
		propositionC: "Vpravo, vcas a v dostatocnej miere.",
		propositionD: "Vacsi ma automaticky prednost.",
		reponse: "C"
	},

	{
		question: "Motorove vozidlo musi mit za jizdy:",
		propositionA: "Rozsvietene obrysove a stretavacie svetla alebo svetla pre denne svietenie pokial je nimi vozidlo vybavene.",
		propositionB: "Otvorenu kapotu aby sa chladil motor.",
		propositionC: "Vzdy zapnute radio.",
		propositionD: "Sklopene spatne zrkadla.",
		reponse: "A"
	},

	{
		question: "Ked blika na zeleznicnom priecesti cervene svetlo:",
		propositionA: "Mysím rýchlo prejsť.",
		propositionB: "Nesmie vozidlo vstupit na zeleznicne priecestie..",
		propositionC: "Zastanem na priecesti a pockam.",
		propositionD: "Strelim do svetla aby prestalo svietit;.",
		reponse: "B"
	},

	{
		question: "Pomali idúce vozidlo sa predbieha:",
		propositionA: "Zatrubim nech sa mi uhne.",
		propositionB: "Vlavo; vpravo sa predbieha vozidlo, ktore meni smer jazdy dolava a nie je pochybnost o dalsej zmene smeru jeho jazdy.",
		propositionC: "Hocikde kde je miesto",
		propositionD: "Vezmem ho sprava po krajnici",
		reponse: "B"
	},

	{
		question: "Ked je na semafore zelena:",
		propositionA: "Vozidlo smie vojst do krizovatky",
		propositionB: "Zapalím si jointa.",
		propositionC: "Zastanem a pockam az zhasne.",
		propositionD: "Odstavím auto a odídem",
		reponse: "A"
	},

	{
		question: "Prvu pomoc je povinny poskytnut:",
		propositionA: "Ja sa isto nikoho dotykat nebudem.",
		propositionB: "Co je prva pomoc.",
		propositionC: "Kazdy, kto tak moze urobit bez nebezpecia pre seba alebo niekoho ineho.",
		propositionD: "Len špecializovaný doktor",
		reponse: "C"
	},
]