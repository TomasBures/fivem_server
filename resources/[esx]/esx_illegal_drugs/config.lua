Config              = {}
Config.MarkerType   = -1 -- Marker visible or not. -1 = hiden  Set to 1 for a visible marker. To have a list of avaible marker go to https://docs.fivem.net/game-references/markers/
Config.DrawDistance = 100.0 --Distance where the marker be visible from
Config.ZoneSize     = {x = 5.0, y = 5.0, z = 3.0} -- Size of the marker
Config.MarkerColor  = {r = 0, g = 255, b = 0} --Color of the marker

Config.RequiredCopsCoke  = 2 --Ammount of cop that need to be online to be able to harvest/process/sell coke
Config.RequiredCopsMeth  = 0 --Ammount of cop that need to be online to be able to harvest/process/sell meth
Config.RequiredCopsWeed  = 2 --Ammount of cop that need to be online to be able to harvest/process/sell weed
Config.RequiredCopsOpium = 2 --Ammount of cop that need to be online to be able to harvest/process/sell opium

Config.TimeToFarmWeed     = 0.5  * 1000 -- Ammount of time to harvest weed
Config.TimeToProcessWeed  = 6  * 1000 -- Ammount of time to process weed
Config.TimeToSellWeed     = 6  * 1000 -- Ammount of time to sell weed

Config.TimeToFarmOpium    = 0.5  * 1000 -- Ammount of time to harvest coke
Config.TimeToProcessOpium = 6  * 1000 -- Ammount of time to process coke
Config.TimeToSellOpium    = 6  * 1000 -- Ammount of time to sell coke

Config.TimeToFarmCoke     = 0.5  * 1000 -- Ammount of time to harvest coke
Config.TimeToProcessCoke  = 6  * 1000 -- Ammount of time to process coke
Config.TimeToSellCoke     = 6  * 1000 -- Ammount of time to sell coke

Config.TimeToFarmMeth     = 0.5  * 1000 -- Ammount of time to harvest meth
Config.TimeToProcessMeth  = 6 * 1000 -- Ammount of time to process meth
Config.TimeToSellMeth     = 6  * 1000 -- Ammount of time to sell meth

Config.Locale = 'en'

Config.Zones = {
	CokeField =			{x=1093.139,  y=-3195.673,  z=-39.131},
	CokeProcessing =	{x=1101.837,  y=-3193.732,  z=-38.993},
	CokeDealer =		{x=385.59,   y=-1882.91,   z=25.03},
	MethField =			{x=3578.721,  y=3651.301,  z=32.89}, --whiskey
	MethProcessing =	{x=2906.25,  y=4347.14,  z=49.3}, --whiskey
	MethDealer =		{x=-2959.64,  y=397.67,   z=14.03}, --whiskey
	WeedField =			{x=1057.448,  y=-3197.646,  z=-39.138},
	WeedProcessing =	{x=1037.527,  y=-3205.368,  z=-38.17},
	WeedDealer =		{x = 169.05,   y= -1633.5,  z= 28.29},
	OpiumField =		{x=2221.14,  y=5614.57,   z=53.87},
	OpiumProcessing =	{x=1009.73,   y=-3195.18,    z=-38.99},
	OpiumDealer =		{x=335.69, y=-2022.05,   z=21.35}
}

Config.DisableBlip = true -- Set to true to disable blips. False to enable them.
Config.Map = {

  {name="Coke Farm Entrance",    color=4, scale=0.8, id=501, x=47.842,     y=3701.961,   z=40.722},
  {name="Coke Farm",             color=4, scale=0.8, id=501, x=1093.139,   y=-3195.673,  z=-39.131},
  {name="Coke Processing",       color=4, scale=0.8, id=501, x=1101.837,   y=-3193.732,  z=-38.993},
  {name="Coke Sales",            color=3, scale=0.8, id=501, x=959.117,    y=-121.055,   z=74.963},
  {name="Meth Farm Entrance",    color=6, scale=0.8, id=499, x=1386.659,   y=3622.805,   z=35.012},
  {name="Meth Farm",             color=6, scale=0.8, id=499, x=1005.721,   y=-3200.301,  z=-38.519},
  {name="Meth Processing",       color=6, scale=0.8, id=499, x=1014.878,   y=-3194.871,  z=-38.993},
  {name="Meth Sales",            color=3, scale=0.8, id=499, x=7.981,      y=6469.067,   z=31.528},
  {name="Opium Farm Entrance",   color=6, scale=0.8, id=403, x=2433.804,   y=4969.196,   z=42.348},
  {name="Opium Farm",            color=6, scale=0.8, id=403, x=2433.804,   y=4969.196,   z=42.348},
  {name="Opium Processing",      color=6, scale=0.8, id=403, x=2434.43,    y=4964.18,    z=42.348},
  {name="Opium Sales",           color=3, scale=0.8, id=403, x=-3155.608,  y=1125.368,   z=20.858},
  {name="Weed Farm Entrance",    color=2, scale=0.8, id=140, x=2221.858,   y=5614.81,    z=54.902},
  {name="Weed Farm",             color=2, scale=0.8, id=140, x=1057.448,   y=-3197.646,  z=-39.138},
  {name="Weed Processing",       color=2, scale=0.8, id=140, x=1037.527,   y=-3205.368,  z=-38.17},
  {name="Weed Sales",            color=3, scale=0.8, id=140, x=-1062.26,   y=-1663.06,   z=3.56}

}
