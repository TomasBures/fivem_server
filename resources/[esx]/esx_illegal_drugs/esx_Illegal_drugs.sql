USE `essentialmode`;

INSERT INTO `items` (name, label, `limit`) VALUES
	('weed', 'Trava (1G)', 420),
	('weed_pooch', 'Balicek travy (28G)', 15),
	('coke', 'Kokain (1G)', 420),
	('coke_pooch', 'Balicek kokainu (28G)', 15),
	('meth', 'Metamfetamin (1G)', 420),
	('meth_pooch', 'Balicek metamfetaminu (28G)', 15),
	('opium', 'Opium (1G)', 420),
	('opium_pooch', 'Balicek opia (28G)', 15)
;
