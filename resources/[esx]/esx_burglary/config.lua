Config                = {}

Config.GiveBlack      = true -- If it should give black money
Config.AlertPolice    = true -- If the police should be alerted if someone fails a burglary

Config.DiscordWebhook = true
Config.WebhookUrl     = "https://discordapp.com/api/webhooks/615439012521443328/V614fb9Zm38u5gpW7sUYrJpSbBzWzoSZ3tp2A30SYu_9HognCbsOLsoJJq9fyQ8xf9VX"

Config.BlipTime       = 2 * 60 * 1000 -- time that region of burglary will show on map (2 min default)