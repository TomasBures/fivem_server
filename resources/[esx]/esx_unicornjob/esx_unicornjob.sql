SET @job_name = 'unicorn';
SET @society_name = 'society_unicorn';
SET @job_Name_Caps = 'Unicorn';



INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1),
  ('society_unicorn_fridge', 'Unicorn Fridge', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
    (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  (@job_name, @job_Name_Caps, 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  (@job_name, 0, 'barman', 'Barman', 300, '{}', '{}'),
  (@job_name, 1, 'dancer', 'Dancer', 300, '{}', '{}'),
  (@job_name, 2, 'viceboss', 'Vice Boss', 500, '{}', '{}'),
  (@job_name, 3, 'boss', 'Boss', 600, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES  
    ('jager', 'Jägermeister', 20),
    ('martini', 'Martini', 20),
    ('soda', 'Soda', 20),
    ('energy', 'Energy Drink', 20),
    ('drpepper', 'Dr. Pepper', 20),
    ('limonade', 'Limonada', 20),
    ('bolcacahuetes', 'Miska arasidu', 20),
    ('bolpistache', 'Miska pistacii', 20),
    ('bolchips', 'Miska chipsu', 20),
    ('saucisson', 'Klobasa', 20),
    ('grapperaisin', 'Hrozny', 20),
    ('jagerbomb', 'Jägerbomba', 20),
    ('whiskycoca', 'Whisky s kolou', 20),
    ('vodkaenergy', 'Vodka s energitakem', 20),
    ('vodkafruit', 'Vodka s ovocem', 20),
    ('rhumfruit', 'Rum s ovocem', 20),
    ('rhumcoca', 'Rum s colou', 20),
    ('mojito', 'Mojito', 20),
    ('ice', 'Led', 40),
    ('rhum', 'Rum', 20),
    ('menthe', 'Mata', 40)
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES 
	('vodka', 'Vodka', 20),
	('whisky', 'Whisky', 20),
    ('tequila', 'Tequila', 20),
    ('icetea', 'Ice Tea', 20),
	('coffev', 'Kava Deluxe', 20)
;
