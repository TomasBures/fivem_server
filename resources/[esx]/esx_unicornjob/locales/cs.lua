Locales['cs'] = {
    -- Cloakroom
    ['cloakroom']                = 'Satna',
    ['citizen_wear']             = 'Civilni obleceni',
    ['barman_outfit']            = 'Outfit barmana',
    ['dancer_outfit_1']          = 'Tanecni outfit 1',
    ['dancer_outfit_2']          = 'Tanecni outfit 2',
    ['dancer_outfit_3']          = 'Tanecni outfit 3',
    ['dancer_outfit_4']          = 'Tanecni outfit 4',
    ['dancer_outfit_5']          = 'Tanecni outfit 5',
    ['dancer_outfit_6']          = 'Tanecni outfit 6',
    ['dancer_outfit_7']          = 'Tanecni outfit 7',
    ['no_outfit']                = 'Pro tebe nemame zadnou uniformu...',
    ['open_cloackroom']          = 'Stiskni ~INPUT_CONTEXT~ pro prevleceni',
  
    -- Vault  
    ['get_weapon']               = 'Vzit zbran',
    ['put_weapon']               = 'Vlozit zbran',
    ['get_weapon_menu']          = 'Sejf - Vzit zbran',
    ['put_weapon_menu']          = 'Sejf - Vlozit zbran',
    ['get_object']               = 'Vzit objekt',
    ['put_object']               = 'Vlozit objekt',
    ['vault']                    = 'Sejf',
    ['open_vault']               = 'Stiskni ~INPUT_CONTEXT~ pro pristup do sejfu',
  
    -- Fridge  
    ['get_object']               = 'Vzit objekt',
    ['put_object']               = 'Vlozit objekt',
    ['fridge']                   = 'Lednice',
    ['open_fridge']              = 'Stiskni ~INPUT_CONTEXT~ pro pristup do lednice',
    ['unicorn_fridge_stock']     = 'Lednice Vanilla Unicornu',
    ['fridge_inventory']         = 'Obsah lednice',
  
    -- Shops  
    ['shop']                     = 'Vanilla Unicorn Obchod',
    ['shop_menu']                = 'Stiskni ~INPUT_CONTEXT~ pro pristup do obchodu.',
    ['bought']                   = 'Koupil jsi ~b~ 1x',
    ['not_enough_money']         = 'Nemas dostatek penez.',
    ['max_item']                 = 'Uz toho u sebe mas dost.',
  
    -- Vehicles  
    ['vehicle_menu']             = 'Vozidla',
    ['vehicle_out']              = 'Vozidlo jiz je venku z garaze',
    ['vehicle_spawner']          = 'Stiskni ~INPUT_CONTEXT~ pro vytazeni vozidla',
    ['store_vehicle']            = 'Stiskni ~INPUT_CONTEXT~ pro zaparkovani vozidla',
    ['service_max']              = 'Plna sluzba: ',
    ['spawn_point_busy']         = 'Blizko vyjezdu stoji vozidlo',
  
    -- Boss Menu  
    ['take_company_money']       = 'Vzit firemni penize',
    ['deposit_money']            = 'Vlozit penize',
    ['amount_of_withdrawal']     = 'Castka vyberu',
    ['invalid_amount']           = 'Neplatne mnozstvi',
    ['amount_of_deposit']        = 'Vlozit castku',
    ['open_bossmenu']            = 'Stiskni ~INPUT_CONTEXT~ pro otevreni menu',
    ['invalid_quantity']         = 'Neplatne mnozstvi',
    ['you_removed']              = 'Vzal jsi x',
    ['you_added']                = 'Vlozil jsi x',
    ['quantity']                 = 'Mnozstvi',
    ['inventory']                = 'Inventar',
    ['unicorn_stock']            = 'Sklad Vanilla Unicornu',
  
    -- Billing Menu  
    ['billing']                  = 'Ucet',
    ['no_players_nearby']        = 'Zadni hraci pobliz',
    ['billing_amount']           = 'Castka k zaplaceni',
    ['amount_invalid']           = 'Neplatna castka',
  
    -- Crafting Menu  
    ['crafting']                 = 'Michani drinku',
    ['martini']                  = 'Martini',
    ['icetea']                   = 'Ice Tea',
    ['drpepper']                 = 'Dr. Pepper',
    ['saucisson']                = 'Klobasa',
    ['grapperaisin']             = 'Hrozny',
    ['energy']                   = 'Energy Drink',
    ['jager']                    = 'Jägermeister',
    ['limonade']                 = 'Limonada',
    ['vodka']                    = 'Vodka',
    ['ice']                      = 'Led',
    ['soda']                     = 'Soda',
    ['whisky']                   = 'Whisky',
    ['rhum']                     = 'Rum',
    ['tequila']                  = 'Tequila',
    ['menthe']                   = 'Mata',
    ['jusfruit']                 = 'Fruit juice',
    ['jagerbomb']                = 'Jägerbomba',
    ['bolcacahuetes']            = 'Miska arasidu',
    ['bolnoixcajou']             = 'Cashew',
    ['bolpistache']              = 'Miska pistacii',
    ['bolchips']                 = 'Miska chipsu',
    ['jagerbomb']                = 'Jägerbomba',
    ['golem']                    = 'Golem',
    ['whiskycoca']               = 'Whisky s kolou',
    ['vodkaenergy']              = 'Vodka s energitakem',
    ['vodkafruit']               = 'Vodka s ovocem',
    ['rhumfruit']                = 'Rum s ovocem',
    ['teqpaf']                   = 'Teq\'paf',
    ['rhumcoca']                 = 'Rum s kolou',
    ['mojito']                   = 'Mojito',
    ['mixapero']                 = 'Aperitif Mix',
	['coffev']                   = 'Kava Deluxe',
    ['metreshooter']             = 'Shooter meter',
    ['jagercerbere']             = 'Jäger Cerberus',
    ['assembling_cocktail']      = 'Probiha mixovani drinku!',
    ['craft_miss']               = 'Pokazil jsi michani drinku ...',
    ['not_enough']               = 'Nedostatek ~r~ ',
    ['craft']                    = 'Dokoncil jsi mixovani ~g~',
	['lighter']                  = 'Zapalovac',
	['cigarett']                 = 'Cigarety',
  
    -- Misc  
    ['map_blip']                 = 'Vanilla Unicorn',
    ['unicorn']                  = 'Vanilla Unicorn',
  
    -- Phone  
    ['unicorn_phone']            = 'Unicorn',
    ['unicorn_customer']         = 'Zakaznik',
  
    -- Teleporters
    ['e_to_enter_1']             = 'Stiskni ~INPUT_PICKUP~ aby jsi sel za bar',
    ['e_to_exit_1']              = 'Stiskni ~INPUT_PICKUP~ aby jsi sel pred bar',
    ['e_to_enter_2']             = 'Stiskni ~INPUT_PICKUP~ aby jsi sel na strechu.',
    ['e_to_exit_2']              = 'Stiskni ~INPUT_PICKUP~ aby jsi sel do kancelare.',
    
  }
  