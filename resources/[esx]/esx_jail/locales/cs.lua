Locales ['cs'] = {
	['blip_name']          = 'vezeni',
	['judge']              = 'soudce',
	['escape_attempt']     = 'nemuzes utect z vezeni!',
	['remaining_msg']      = 'zbyva ~b~%s~s~ sekund nez budes propusten z vezeni',
	['jailed_msg']         = '%s je nyni ve vezeni na %s minut',
	['unjailed']           = '%s byl propusten z vezeni!'
}
