Locales['en'] = {
  -- Global menus
  ['cloakroom']                 = 'Satna',
  ['cloak_change']              = 'Stlac ~INPUT_PICKUP~ pre zmenu oblecenia.',
  ['citizen_wear']              = 'Civilne oblecenie',
  ['job_wear']                  = 'Pracovne oblecenie',
  ['bank_deposit_returned']     = 'Zaloha ~g~$%s~s~ ti bola vratena.',
  ['bank_deposit_taken']        = 'Zaplatil si zalohu ~r~$%s~s~ Dakujeme.',
  ['foot_work']                 = 'Pri tomto ukone musis stat na nohach.',
  ['next_point']                = 'Ide ti to skvele pokracuj k dalsiemu miestu.',
  ['not_your_vehicle']          = 'Toto nie je tvoje vozidlo alebo nesedis ako sofer.',
  ['in_vehicle']                = 'Musis sediet vo vozidle.',
  ['wrong_point']               = 'Nie si na spravnom mieste.',
  ['max_limit']                 = 'Uz viacej neunesies ~y~%s~s~',
  ['not_enough']                = 'Nemas dost ~y~%s~s~ na pokracovanie.',
  ['spawn_veh']                 = 'Vybrat auto z garaze',
  ['spawn_veh_button']          = 'Stlac ~INPUT_PICKUP~ pre vyzdvihnutie vozidla.',
  ['spawn_truck_button']        = 'Stlac ~INPUT_PICKUP~ pre vyzdvihnutie trucku.',
  ['spawn_blocked']             = 'Nejake vozidlo blokuje branu!',
  ['service_vh']                = 'Servisne vozidlo',
  ['return_vh']                 = 'Vratenie vozidla',
  ['return_vh_button']          = 'Stlac ~INPUT_PICKUP~ pre vratenie vozidla.',
  ['delivery_point']            = 'Miesto vykladky',
  ['delivery']                  = 'Dorucovanie',
  ['public_enter']              = 'Stlac ~INPUT_PICKUP~ pre vstup do budovy.',
  ['public_leave']              = 'Stlac ~INPUT_PICKUP~ pre opustenie budovy.',

  -- Lumber Jack job
  ['lj_locker_room']            = 'Satna drevorubacov',
  ['lj_mapblip']                = 'Pila',
  ['lj_wood']                   = 'Drevo',
  ['lj_pickup']                 = 'Stlac ~INPUT_PICKUP~ pre zdvihnutie dreva.',
  ['lj_cutwood']                = 'rezanie dreva',
  ['lj_cutwood_button']         = 'Stlac ~INPUT_PICKUP~ pre narezanie dreva.',
  ['lj_board']                  = 'Dosky',
  ['lj_planks']                 = 'Laty',
  ['lj_cutwood']                = 'rezanie dreva',
  ['lj_pick_boards']            = 'Stlac ~INPUT_PICKUP~ pre zdvyhnutie dosiek.',
  ['lj_deliver_button']         = 'Stlac ~INPUT_PICKUP~ pre dorucenie dosiek.',

  -- Fisherman
  ['fm_fish_locker']            = 'Satna rybarov',
  ['fm_fish']                   = 'Ryba',
  ['fm_fish_area']              = 'Rybarska zona',
  ['fm_fish_button']            = 'Stlac ~INPUT_PICKUP~ pre zaciatok lovu.',
  ['fm_spawnboat_title']        = 'Lodne doky',
  ['fm_spawnboat']              = 'Stlac ~INPUT_PICKUP~ pre nalodenie.',
  ['fm_boat_title']             = 'Lod',
  ['fm_boat_return_title']      = 'Vratenie lode',
  ['fm_boat_return_button']     = 'Stlac ~INPUT_PICKUP~ pre vratenie lode.',
  ['fm_deliver_fish']           = 'Stlac ~INPUT_PICKUP~ pre dorucenie ulovku.',

  -- Fuel
  ['f_oil_refiner']             = 'oil Refiner Locker Room',
  ['f_drill_oil']               = 'drill for oil',
  ['f_fuel']                    = 'oil',
  ['f_drillbutton']             = 'press ~INPUT_PICKUP~ to drill.',
  ['f_fuel_refine']             = 'refine oil',
  ['f_refine_fuel_button']      = 'press ~INPUT_PICKUP~ to refine.',
  ['f_fuel_mixture']            = 'mix refined oil',
  ['f_gas']                     = 'gas',
  ['f_fuel_mixture_button']     = 'press ~INPUT_PICKUP~ to mix oil.',
  ['f_deliver_gas']             = 'deliver Gas',
  ['f_deliver_gas_button']      = 'press ~INPUT_PICKUP~ to deliver gasoline.',

  -- Miner
  ['m_miner_locker']            = 'Satna pre banikov',
  ['m_rock']                    = 'Skala',
  ['m_pickrocks']               = 'Stlac ~INPUT_PICKUP~ pre tazbu kamena.',
  ['m_washrock']                = 'Cisty kamen',
  ['m_rock_button']             = 'Stlac ~INPUT_PICKUP~ pre umytie kamena.',
  ['m_rock_smelting']           = 'Tavenie kamena',
  ['m_copper']                  = 'bronz',
  ['m_sell_copper']             = 'Vykup bronzu',
  ['m_deliver_copper']          = 'Stlac ~INPUT_PICKUP~ pre predaj bronzu.',
  ['m_iron']                    = 'Zelezo',
  ['m_sell_iron']               = 'Vykup zeleza',
  ['m_deliver_iron']            = 'Stlac ~INPUT_PICKUP~ pre predaj zeleza.',
  ['m_gold']                    = 'Zlato',
  ['m_sell_gold']               = 'Vykup zlata',
  ['m_deliver_gold']            = 'Stlac ~INPUT_PICKUP~ pre predaj zlata.',
  ['m_diamond']                 = 'Diamanty',
  ['m_sell_diamond']            = 'Vykup Diamantov',
  ['m_deliver_diamond']         = 'Stlac ~INPUT_PICKUP~ pre predaj diamantov.',
  ['m_melt_button']             = 'Stlac~INPUT_PICKUP~ pre roztavenie kamena.',

  -- Reporter
  ['reporter_name']             = 'san Andreas Times',
  ['reporter_garage']           = 'Press ~INPUT_PICKUP~ to go down to the garage.',

  -- Slaughterer
  ['s_slaughter_locker']        = 'Satna mesiara',
  ['s_hen']                     = 'Kuracia farma',
  ['s_alive_chicken']           = 'Zive kura',
  ['s_catch_hen']               = 'Stlac ~INPUT_PICKUP~ pre chytanie kurciat.',
  ['s_slaughtered_chicken']     = 'Zabalene kurcata',
  ['s_chop_animal']             = 'Stlac ~INPUT_PICKUP~ pre hromadnu vrazdu kurciat.',
  ['s_slaughtered']             = 'Jatka',
  ['s_package']                 = 'Balenie',
  ['s_packagechicken']          = 'Balene kurence',
  ['s_unpackaged']              = 'Kurca na zabalenie',
  ['s_unpackaged_button']       = 'Stlac ~INPUT_PICKUP~ pre zabalenie kurciat.',
  ['s_deliver']                 = 'Stlac~INPUT_PICKUP~ pre predaj balenych kurciat .',

  -- Dress Designer
  ['dd_dress_locker']           = 'Satna Krajcira',
  ['dd_wool']                   = 'Vlna',
  ['dd_pickup']                 = 'Stlac ~INPUT_PICKUP~ pre zber vlny.',
  ['dd_fabric']                 = 'Tkanina',
  ['dd_makefabric']             = 'Stlac ~INPUT_PICKUP~ pre vyrobu tkaniny.',
  ['dd_clothing']               = 'Oblecenie',
  ['dd_makeclothing']           = 'Stlac ~INPUT_PICKUP~ pre vyrobu oblecenia.',
  ['dd_deliver_clothes']        = 'Stlac ~INPUT_PICKUP~ pre dorucenie oblecenia.',
  }
