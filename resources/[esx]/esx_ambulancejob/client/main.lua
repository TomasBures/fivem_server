Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local FirstSpawn, PlayerLoaded = true, false

IsDead = false
ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(100)
	end

	PlayerLoaded = true
	ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	ESX.PlayerData = xPlayer
	PlayerLoaded = true
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
end)

AddEventHandler('playerSpawned', function()
	IsDead = false

	if FirstSpawn then
		exports.spawnmanager:setAutoSpawn(false) -- disable respawn
		FirstSpawn = false

		ESX.TriggerServerCallback('esx_ambulancejob:getDeathStatus', function(isDead)
			if isDead and Config.AntiCombatLog then
				while not PlayerLoaded do
					Citizen.Wait(1000)
				end

				ESX.ShowNotification(_U('combatlog_message'))
				RemoveItemsAfterRPDeath()
			end
		end)
	end
end)

-- Create blips
Citizen.CreateThread(function()
	for k,v in pairs(Config.Hospitals) do
		local blip = AddBlipForCoord(v.Blip.coords)

		SetBlipSprite(blip, v.Blip.sprite)
		SetBlipScale(blip, v.Blip.scale)
		SetBlipColour(blip, v.Blip.color)
		SetBlipAsShortRange(blip, true)

		BeginTextCommandSetBlipName('STRING')
		AddTextComponentSubstringPlayerName(_U('hospital'))
		EndTextCommandSetBlipName(blip)
	end
end)

-- Disable most inputs when dead
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		if IsDead then
			DisableAllControlActions(0)
			EnableControlAction(0, Keys['G'], true)
			EnableControlAction(0, Keys['T'], true)
			EnableControlAction(0, Keys['E'], true)
		else
			Citizen.Wait(500)
		end
	end
end)

function OnPlayerDeath()
	IsDead = true
	TriggerServerEvent('esx_ambulancejob:setDeathStatus', true)

	StartDeathTimer()
	StartDistressSignal()

	StartScreenEffect('DeathFailOut', 0, false)
end

function StartDistressSignal()
	Citizen.CreateThread(function()
		local timer = Config.BleedoutTimer

		while timer > 0 and IsDead do
			Citizen.Wait(2)
			timer = timer - 30

			SetTextFont(4)
			SetTextProportional(1)
			SetTextScale(0.45, 0.45)
			SetTextColour(185, 185, 185, 255)
			SetTextDropShadow(0, 0, 0, 0, 255)
			SetTextEdge(1, 0, 0, 0, 255)
			SetTextDropShadow()
			SetTextOutline()
			BeginTextCommandDisplayText('STRING')
			AddTextComponentSubstringPlayerName(_U('distress_send'))
			EndTextCommandDisplayText(0.175, 0.805)

			if IsControlPressed(0, Keys['G']) then
				SendDistressSignal()
				break
			end
		end
	end)
end

function SendDistressSignal()
	local playerPed = PlayerPedId()
	PedPosition		= GetEntityCoords(playerPed)
	
	local PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z }

	ESX.ShowNotification(_U('distress_sent'))

    TriggerServerEvent('esx_addons_gcphone:startCall', 'ambulance', _U('distress_message'), PlayerCoords, {

		PlayerCoords = { x = PedPosition.x, y = PedPosition.y, z = PedPosition.z },
	})
end

function DrawGenericTextThisFrame()
	SetTextFont(4)
	SetTextProportional(0)
	SetTextScale(0.0, 0.5)
	SetTextColour(255, 255, 255, 255)
	SetTextDropshadow(0, 0, 0, 0, 255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(true)
end

function secondsToClock(seconds)
	local seconds, hours, mins, secs = tonumber(seconds), 0, 0, 0

	if seconds <= 0 then
		return 0, 0
	else
		local hours = string.format("%02.f", math.floor(seconds / 3600))
		local mins = string.format("%02.f", math.floor(seconds / 60 - (hours * 60)))
		local secs = string.format("%02.f", math.floor(seconds - hours * 3600 - mins * 60))

		return mins, secs
	end
end

function StartDeathTimer()
	local canPayFine = false

	if Config.EarlyRespawnFine then
		ESX.TriggerServerCallback('esx_ambulancejob:checkBalance', function(canPay)
			canPayFine = canPay
		end)
	end

	local earlySpawnTimer = ESX.Math.Round(Config.EarlyRespawnTimer / 1000)
	local bleedoutTimer = ESX.Math.Round(Config.BleedoutTimer / 1000)

	Citizen.CreateThread(function()
		-- early respawn timer
		while earlySpawnTimer > 0 and IsDead do
			Citizen.Wait(1000)

			if earlySpawnTimer > 0 then
				earlySpawnTimer = earlySpawnTimer - 1
			end
		end

		-- bleedout timer
		while bleedoutTimer > 0 and IsDead do
			Citizen.Wait(1000)

			if bleedoutTimer > 0 then
				bleedoutTimer = bleedoutTimer - 1
			end
		end
	end)

	Citizen.CreateThread(function()
		local text, timeHeld

		-- early respawn timer
		while earlySpawnTimer > 0 and IsDead do
			Citizen.Wait(0)
			text = _U('respawn_available_in', secondsToClock(earlySpawnTimer))

			DrawGenericTextThisFrame()

			SetTextEntry("STRING")
			AddTextComponentString(text)
			DrawText(0.5, 0.8)
		end

		-- bleedout timer
		while bleedoutTimer > 0 and IsDead do
			Citizen.Wait(0)
			text = _U('respawn_bleedout_in', secondsToClock(bleedoutTimer))

			if not Config.EarlyRespawnFine then
				text = text .. _U('respawn_bleedout_prompt')

				if IsControlPressed(0, Keys['E']) and timeHeld > 60 then
					RemoveItemsAfterRPDeath()
					break
				end
			elseif Config.EarlyRespawnFine and canPayFine then
				text = text .. _U('respawn_bleedout_fine', ESX.Math.GroupDigits(Config.EarlyRespawnFineAmount))

				if IsControlPressed(0, Keys['E']) and timeHeld > 60 then
					TriggerServerEvent('esx_ambulancejob:payFine')
					RemoveItemsAfterRPDeath()
					break
				end
			end

			if IsControlPressed(0, Keys['E']) then
				timeHeld = timeHeld + 1
			else
				timeHeld = 0
			end

			DrawGenericTextThisFrame()

			SetTextEntry("STRING")
			AddTextComponentString(text)
			DrawText(0.5, 0.8)
		end
			
		if bleedoutTimer < 1 and IsDead then
			RemoveItemsAfterRPDeath()
		end
	end)
end

function RemoveItemsAfterRPDeath()
	TriggerServerEvent('esx_ambulancejob:setDeathStatus', false)

	Citizen.CreateThread(function()
		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(10)
		end

		ESX.TriggerServerCallback('esx_ambulancejob:removeItemsAfterRPDeath', function()
			ESX.SetPlayerData('lastPosition', Config.RespawnPoint.coords)
			ESX.SetPlayerData('loadout', {})

			TriggerServerEvent('esx:updateLastPosition', Config.RespawnPoint.coords)
			RespawnPed(PlayerPedId(), Config.RespawnPoint.coords, Config.RespawnPoint.heading)

			StopScreenEffect('DeathFailOut')
			DoScreenFadeIn(800)
		end)
	end)
end

function RespawnPed(ped, coords, heading)
	SetEntityCoordsNoOffset(ped, coords.x, coords.y, coords.z, false, false, false, true)
	NetworkResurrectLocalPlayer(coords.x, coords.y, coords.z, heading, true, false)
	SetPlayerInvincible(ped, false)
	TriggerEvent('playerSpawned', coords.x, coords.y, coords.z)
	ClearPedBloodDamage(ped)

	ESX.UI.Menu.CloseAll()
end

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
	local specialContact = {
		name		= 'Zachranka',
		number		= 'ambulance',
		base64Icon	= 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAFh2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxOS0wMi0xNlQxODo1NToxMiswMTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTktMDItMTZUMTk6MDU6MjMrMDE6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMTktMDItMTZUMTk6MDU6MjMrMDE6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjBkNmI0MDg0LWEyZWQtYTQ0NC1iZGVlLTU2NGNkYmIyNDU3MSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowZDZiNDA4NC1hMmVkLWE0NDQtYmRlZS01NjRjZGJiMjQ1NzEiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowZDZiNDA4NC1hMmVkLWE0NDQtYmRlZS01NjRjZGJiMjQ1NzEiPiA8cGhvdG9zaG9wOkRvY3VtZW50QW5jZXN0b3JzPiA8cmRmOkJhZz4gPHJkZjpsaT5hZG9iZTpkb2NpZDpwaG90b3Nob3A6YjczYTAwNTAtYWQxOS1iMTQ2LThkYmItNmFiZGIxMzk4NThjPC9yZGY6bGk+IDwvcmRmOkJhZz4gPC9waG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6MGQ2YjQwODQtYTJlZC1hNDQ0LWJkZWUtNTY0Y2RiYjI0NTcxIiBzdEV2dDp3aGVuPSIyMDE5LTAyLTE2VDE4OjU1OjEyKzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvXUpM8AAAvkSURBVFiFbZd7dFXVncc/e59z38+8kxsgQAQEIi/BKGDV0VYxVhHtjLqKVVYlrTpjp52XzhpddmzX1DejRRmntlXUPqgurYJhFMTyUlF5GAOEGCAhCXncm9zHufc89/yRiKxVf2v91ln7j3N+e//W+X6+vy3aDym+jIpyeGPLTg4e2sncc5v54vgYwZAPIaBQsJg2PfLb/Gj2+sEhA3/Ah+d5KEBqGrbpUpsqQ3m5j97d+ublkVgY5SlMp8TypS00Tj2XmpSOEDaed6YkOl8TSmlEYx69vd309qZJJCIMDmaYN//q+rc252Jb3thHpCxBIZMGivjjKayszZKljdzx/WkpT/mxbR8Fw2B+03waJk2jUDSA+F/V0oX4aiEFaJrEtm2CwQAXNs9na7odTUAiolEyyBqmA2gUsgVarl9M4zlJ/vvx3YBiNJPBVTOHL7v6Gnx+P0WjREN5NcpzEQI0XcO2S6C+KioRgAAP0PyQy5vk8wZGsURtTZIlS2ZQLFq4E+3WtYk3XYv/Wb+SdQ/fyMLzU0AeV3lomu6eM3UaMxqnM7m6DuW4eEoxMpzBLJmEAhFsy4/rjKe0rBymmcWnlzjcMUhdrb595XUX/9wwCuTzNrPn1BFPhrAthfIEug4wApQ4PZABoC4VB2w8pZBIFQsEcQoGhdExslmDZNI3c/6i1L5c1nkiHPbT1BSjsXE85UDvMAOn0vR2D2E56dZ33uu8dMvmnntT9dXvNTTEm3PZHHWTNUYtsD0lB07lqayrZd/+B2hqmsxwJkMmbQJ+fJqOZZtae+dhMmNpHNtkTlP12oGhsUPr128/v+NI3z2+ADWRcAChuWi6h56sihOPBxjNZPUDh3oefvHFzyll+/hw38Alra0X7p03t+bRnOG7d5u0naGM4+g6tLX9mPrqMAc+6aWhsZKRkSKgEQuHGRw87Xx6sJNbb7pySuPU8vWb27pbnn5qF6ZxgnROiYuXznq2vyd8fTAUxbYdZLIqhh7QiSbsp3+/6WC8lM0QjE+l/WAv/3DXH9jw3J5/mjRlaseaVbMuLWTGSrObqll0Xj3X3vA87+7ooqoiSSCsAIWtFLou9FXXNK9BhTse+vn7LY/9YgumIdDDM3l/aydbt3+6MlmWuSgWdYmELaSVzzMy2Jt67fUPW3e+fxJ/uAyBQzhRhi8Y45WXdnP3PTvOKTr+7S0tjStPDxYAWDC/Bl/QB4BtuoDCMAyuXnHBxSPD8lffXf1CePs77QSj1QTjAaTPAxR/+lMXA4P9Lwz0D1IsWWg3XN9KIZ/e9OhTexuH+oYJRmJ43jicpCbxBxOMDmbZtq2TY8dG/R/t6WDZxY38/Q8vQdMEqbo4jz+xjdG0jfTBrp0n+fX/7sWxFcFkEonCQ6E88AVCDPQMIPz+8mXL6k8EI9H9YseOz775yLqtW998tZ1ArBz5JRjUuDyVUkgpcFyBlc8CFpMaKug5/l+AIJvLU159P27JQw/5cIoFfOEoUvuKLZ4CTyl0KSgZNqB44MGLSsuXNqVkZjT7cMPUOsDCzKXxlJyor1BqvBOep5DCIxCLEIhV0HviNN+47BFG0hlCwRCtP2gGbDR0ApEEUgJKgVLj3VQKXUpM00M5oyjPIxqLB43s6A+1C5beuGPp4lTwiqvnLhpOK7qPduOY4A8HAYUQ4kwqpRBCofnjfHG0naaF57BwfgMtV82h80SG/fsO4/OHUSiEnACWACEkpdwoyjW56dblPPZYi11THvz3nlOnN8ryssThhin+O6oSvmX33Xvph4+uu5ma+iTGaA+2pRDyLGxKCQiEUEAZPT0FwAIkmUxx3FoECCFAKYSQmDmTUq6Pxc0zeOn3a7jvXy7b2Nt5YsbHH5z4RTAU7pWuazNn7mSGhqzdne1G8zlTxY+e3XBF7ntrrsIpFTCL9pnTqy+NU2iAwWftxwE/6365jS2vf4A/mhwvDAipUcrliCZC3P/Tm3ng/uZPm2bVfLv9UHb18RPDJxLJIK7rIDa9upOm2QkOHMphW2Xs+2g7iUSQW2697rVP9x9d2bp2E4Yh0HwT1iEUppGnfnIZe3b9M709g1zYvA6Ej3A8iOcKwMMsFVm8ZBLPrr9J6b78XUcO9z2jvAqk0Dje1U2yLEY4qiEjYcUTT36Co4JIaeLXk9TWpf6urW3/tLa2DhzPm1DDhAcp8ByDJ5+4gfq6JEePDZCoSoCUE8oZh5Ku6wwPF3nppZ3s2dt1QdO8ec35fIG+vj6qaqbjoiFcH7rrgq5rnD49xtxzy1dMn1X1s4OH+hZu3Pgx2cwYvkgFmj6xAaVwPQX4mDIphqccmuZOw3MdEN7EJj0QCj3go/vYKI8/+raYMbvxtt6T5m0LFlS9kogm/7OnJ9sRi2soXKRt2STivknV1XLjB/uObl739F8Wrn/qPbIZj2C8EnnW6RUKXQJY/GFTO1Lo5HM5cukMaH4UHkz8K7br4Y/oBGMpOjsGeeinf+aXz3x881Bm9PP6KfqDrmdFioaDtnLV6nsGBoy33nyzfcHzz+1l+HSOYLwcn187wwEQZz0FQgbZ+d4halNxrvzmHHbt6+HEsWF0v46aYJgSAikEEoUvEEL6InQdPsnmtmP4dHFJWbn2/WQylJaV1TXRgXTBt+2dvchgkmC0HPBQuBOfOivU+DCn+3XA4Dcvfkg0GuHJx64Fz8FxBFJKhBQImFCOGm+1hFCiHKVCvLyxjeG0XV1ZF2qQRl78bOV1s042zDwPr5QH4aK8syTH+ATlKTALeUzLQgoFJOjqGgZKtLcPAzaObVLMjmKbiq9GPTHRNw8pACfH7PMv4OoV03uPHT31gKyuqmdK7bR/XHXtbMA8Y0TwJXQkVq6AXRihYVaKysoItu0CHq2ty/msvZe77nwJpE5tKsqK6xbimSZWdgwhx8HkeuAoF8sa7+Da2xcRDSTvOG/2QqRCY2CQV29YNb9z8YXzsQoZpBAIKbFsRSnXT7QsxFPrV9O2eQ2TUglso4D0a/z4R8tp/zxNNpMFT1Jbm+TJx1tKv/vjd1ly0SzM7ClKOROlSdB0bGOIK1oWctXl0/YZxcTbFdV1yKHhHpBFCgX7B9+6cjIIH7atUcoOo8wCrXdfw+9eXs38poqHDh049a7u9wMuUg+SGckTiOhAFIBgMMyWt3bvHu7r+JsNG27c/eBDq0hU6tjZ09hZh1A8yc1/O5P+AXO1owr09/cijVKBYilHb39224qrZuy6YNlcnNLnXNWymBdeuY2bvjP9VaXU+f9235b/+PiDrr7Kqiig4xhZ3t1xHLvonfEDn+ZH01To+Vfe2L7zLweWXdcy885Nf7xt6PbWy0EN8I3LZnH5ZXNfSJRPP1yTqqcmNQk5uX4S1ZXVLJg3k9yYvPuWm2bwyBNr+clPlh+UnnXN7p1Hbjh8pPeTzs9PEYmGy3VNTKhDY8NzH5KakiA1tQKwkVKi+8NaR2c7W99p48CBsWeUKWeu+d65D//m5Ttpvf0ierrz98TCHnZhDGUX0M2SNaEwB8cN7J81s+4VVC7f9vbBtcl4JQ0NKX792w8o2g6J8rjnuSPjPJB+hocKHD3aQyjkBxSlkkEsHueab1+PrmvYdoGx0eDogYPd/3ruvPhuPLfaKMZGTxw/eUZhesPUhvENKFiwCNreOnzL221bmdPURDwaYe/eo3zRlSGR0HEde8LnFQhBIuFj69tH6OoYBTRKZgm/LySWnL+MklXERkcph8rKMvbs+ez1YCBIy5VNpDMjZ2SqfyU7MAxBWXmUiooEfr9Of3+W7ds/IxLUyNomAg/P9QAXTUq6j40wcGoMfApsl3R6lKqqWv+Ka5sJa+O3rYOHcuzfu4tAwE8kEsFxHORZM4auziKOUgLHcZFSIKVi/yddKMelvDKKWSoQCfljUveN88GvY1qQL1gEY35KGZvyyjJcNx/91Yb/IxQKIDSFkbMRVpFgIMDXxdfejoPBAMePn2YsZzBj9hSEFFiewiiV+u9auyi34luTQQlc10MhcV2JZVlMqo/SeaT75FuvtZEsi6M8geNaXLT0QgKRAK6j/qrW/wMSqK3Ex2B2awAAAABJRU5ErkJggg=='
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

AddEventHandler('esx:onPlayerDeath', function(reason)
	OnPlayerDeath()
end)

RegisterNetEvent('esx_ambulancejob:revive')
AddEventHandler('esx_ambulancejob:revive', function()
	local playerPed = PlayerPedId()
	local coords = GetEntityCoords(playerPed)
	TriggerServerEvent('esx_ambulancejob:setDeathStatus', false)

	Citizen.CreateThread(function()
		DoScreenFadeOut(800)

		while not IsScreenFadedOut() do
			Citizen.Wait(50)
		end

		ESX.SetPlayerData('lastPosition', {
			x = coords.x,
			y = coords.y,
			z = coords.z
		})

		TriggerServerEvent('esx:updateLastPosition', {
			x = coords.x,
			y = coords.y,
			z = coords.z
		})

		RespawnPed(playerPed, {
			x = coords.x,
			y = coords.y,
			z = coords.z,
			heading = 0.0
		})

		StopScreenEffect('DeathFailOut')
		DoScreenFadeIn(800)
	end)
end)

-- Load unloaded IPLs
if Config.LoadIpl then
	Citizen.CreateThread(function()
		LoadMpDlcMaps()
		EnableMpDlcMaps(true)
		RequestIpl('Coroner_Int_on') -- Morgue
	end)
end
