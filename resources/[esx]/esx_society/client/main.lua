ESX                   = nil

local base64MoneyIcon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAHbGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE1LTEwLTA5VDIwOjEzOjE2KzAyOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxOS0wMi0wNlQyMjozODoyOSswMTowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOS0wMi0wNlQyMjozODoyOSswMTowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxZWUwNDA5NC00YzVlLTk0NGQtOTM4NC00ZjgwOWVjY2Y0MjMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NmZkNGVlNjgtOWU3Mi1jYTQxLTg5NjMtMWRjMmI2M2EzYWExIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NmZkNGVlNjgtOWU3Mi1jYTQxLTg5NjMtMWRjMmI2M2EzYWExIiB0aWZmOk9yaWVudGF0aW9uPSIxIiB0aWZmOlhSZXNvbHV0aW9uPSI5NjAwMDAvMTAwMDAiIHRpZmY6WVJlc29sdXRpb249Ijk2MDAwMC8xMDAwMCIgdGlmZjpSZXNvbHV0aW9uVW5pdD0iMiIgZXhpZjpDb2xvclNwYWNlPSIxIiBleGlmOlBpeGVsWERpbWVuc2lvbj0iMTgwIiBleGlmOlBpeGVsWURpbWVuc2lvbj0iMTgwIj4gPHBob3Rvc2hvcDpUZXh0TGF5ZXJzPiA8cmRmOkJhZz4gPHJkZjpsaSBwaG90b3Nob3A6TGF5ZXJOYW1lPSLigqwiIHBob3Rvc2hvcDpMYXllclRleHQ9IuKCrCIvPiA8L3JkZjpCYWc+IDwvcGhvdG9zaG9wOlRleHRMYXllcnM+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6NmZkNGVlNjgtOWU3Mi1jYTQxLTg5NjMtMWRjMmI2M2EzYWExIiBzdEV2dDp3aGVuPSIyMDE1LTEwLTA5VDIwOjEzOjE2KzAyOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoxZWUwNDA5NC00YzVlLTk0NGQtOTM4NC00ZjgwOWVjY2Y0MjMiIHN0RXZ0OndoZW49IjIwMTktMDItMDZUMjI6Mzg6MjkrMDE6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+JCuJmQAARt1JREFUeJztnXV8FOfWx7/PzKzHEyTB3aEUqUJbClXqvXWjLtRv5Zba21u9tVujt663zq1RF1qo0JZSXIuTEOLJZn1mnveP2ZBks4GQbLT8PuyHzcgzz+789sw55zkipJTsQYORDmQCHiAL6A70AXoBqdF96YAWfe+sZ5wgUAzoQGn0fTmwCdgAbAWKAF90X2mzfJoOCLGH0PUiAxgC9ABygNHAYGAY4GqhOQSA5cAq4A8gD9gCrARKWmgO7Qp7CF0NDZgMHIhF4BFYJFZbc1JxYGCReykWwX8AvsaS9n95/NUJ3Q04GYvIVSS2teqMdh8Rqsn9NfAekNuqM2pFxCV0+skFlAXfa4XpNC/k7MvtQBfgBOBsLB24a2PHM6XEMMGUJoYpiRgmummgGxLdNDFME8OQ1PyKFQUURWBTVGyqgqIo2FQFTREoioKqgCoUhGj0x8zH0sFfA94HtoupM8ONHq0dQM6+fMd7rRXn0WKQsy8fCIwEzgGOBpTdOd+UEt2QBHWDQDhCZShMYUWQ7RUBCsr9FHpDlFSGKPNFqAxE8AcNghGDcMRENySGaTFaYBHapinYNQWXXcXtVEl120h128lMdtApxUWXFBddU12kehwkOx247Rp2TUFTGkT0rtHXWOBR4BM5+/JXgaVi6szVu/O52yM6NKHl7MsnAwcAV2EZebtEldQN6jreYIgib4ANRV5W51WwfnsluUUBCspClHojVPoNQiGdiC4xDEsiS1NaLymRSEBG/1WTGiEQCBAgFIEiBKoqUFUFu03B6VBIdmmkp9jJTnfSI8tN/+wUBuWk0iMjmYwkF0lOO3ZN7EqaK8Ax0VeZnH35v4EfxdSZXzfha23T6JCElrMvvwqYAkxtyPG6KQlEdCr8IbaW+Vi7rYxluWWs2ORlU36AovIwPr9OKKRjGCamYWBKHaSJxAASbYcIhFAQQkFRVDRNxelQSXJrdE530C8niRE90xnaPZ3+XVLpmuYh2WXHaVNQ6md3GnAngJx9+WzgKzF15uMJnniro8Po0HL25Q5gOnAWsNeujtdNSSAcoagywMptpcxfW8jiDeWsywtQVBqi0qcTCRvoRgRphpuJuLsLi+iapuGw20j2aHTJcDKoezJj+2Uxrn9nBnRNJ83j2BW5q7AIeB14UkydGWr26TcTaurQ7Z7QcvblXYHDgbuxjLx6YUqJP6STW17JstwSfvuzmIXryli71U9xSYRQMIJuhNsQgXeFaoK7XBpdM1wM6ZXC+AFZjO3bmcHZ6XRKdeG0abvSvbcCtwJfiKkz81tk6glEhyC0nH25BzgWuBfovbNjdVNS6PWzJr+UeWu2M29FCas2+CgpCxEKGRh6CCmrSNx+IYSCqmq4nDY6ZzgY2TeNScNz2GdAF/p1SSPVbUdVdsrsjcAtwEdi6kxfi0w6AWj3hJazLz8buAiYUN8xprS8ErmlXn5ZV8i3y7fz26oKtuQHCPgi6HoQU4YAs8Xm3bIQaJqNlGQHfXM8HDi0C5OG5zC6Tyc6Jbuwa+rOpPY84DkxdeZrLTffxqPdElrOvnwkcA87Mfaq1Iq1hWV8uyKPrxcXsnydj6KSEOFQGNMMIIm03KTbAIRQcTntdMl0Mm5QJofv1YMJg7Ppnpm0K3VkNjBDTJ25pOVmu/uoRehGjXD0U4maS4MQVS9uAK7ACgqqA1NKKkNhVm8v44uluXy2oIjVG3x4K0JRaRyk40rjhqFK305PdbJXvzSO37c3hwzLoWdmCi57vcQuAp4CHmwVNeSTK3br8MavRyUK2lSY9hhU/gJFIVAV2GskrP0FKtKQX546EXgSa1m6Dkwp8YcNVuYX89mSXL5aWMSKdVEiGz6kDNN0404kYIy2BIGqqmSmudh7YAZTx/Zkysie9MxKwlG/KrIUmC6OfWouPYpgpgYDxsAFayDohmf7Qd4dWNpK66HN+qHle5d4sAyUW+o7JhgxWF9Uxtcr8nj/53yWrPHh9QYxdH9UP04UCTsSmQEkhqFTUFzJV78G+X1NMV8u2srf9uvLQUO70TXNjabWWbAZAXwvP7riXuBeMfOeNmk0tklCy/v22g94GNgv3n7dlOSVe/l+9Tbe+zmPX5d5KSkNokcqE0zkjg6JYUQoLNb59OcQC9eWcOheWznlgH6M69eVzGRHPF/2LcAhUs64Xgz8/OdWmPRO0eYILT8750bgH1grW7VQZfAt3LKdt37ezNcLytiSFyQc8mJKP7tP5I6mSjQWEl2PsDW/grfnBFm4roQT9uvJyfv0p392ajzDcT/gU7nmiPvEA2v+1Tpzjo82o0PLZ/umAM8Bp8Q7LKQbbC2t5NOlW3jz+20sW+sj4PNhGP6/nNeiuaEoKslJDvYdksV5kwZx0NAcOqW40NS4MV3vABeJbnMq2oIO3SYILSMfDwVeBsbF7q6Sygs2F/DK3A18+WsZRYUBInoFUrbb1dp2AU3T6Nk1iZMP7MOZBw5gQE5afW6+34DzhJi44i9PaCnl34D/ECcarkpX/njRFl6fk8eyNX4C/goM00d7c8F57BF84faWO2C5+pLcdg4Y0ZkLJw/moGHdyUiKq1uXAJcKId5thWnuQKsSWko5AysGow58YZ0120t54+cNvPd9EdvyAoT1snYplZOdIRb+8yEG3DCjtafSaGiaxqBeqZw/eRDHjOtDr6xk7FpcFeRWIcQ9LT2/KrSaUSilfBi4Lna7KSVl/hDfrtnKS99s5ufFlXgr/BhGBbI9ps3pGsNyNtF/SB421SSiqyDanyGq6zorN5Rx/3tLWLSxiAsmDWFMv854HLZYFeRuKWWGEOL61pjnbmVuJAJSSpuUcib1kHlbuY/X5q/lnrfXM2e+l/KyEnSjtH2SGcBQmXHslwBcNWUeeD2tPKHGwzQNCot9vPvdRu54awGfLtxIqS9InOiJ66SUM6WULa5jtaiEllLagTeAk2L3mVKSW1bJf+ev48Uv8tm8OUA4UtLuVIzOKZWU+V3ohoIpBaMGbGbKqOVgwukH/MLDnx+EEpXQLruOqhhUBOor39EWIQmGwvy4dDuVgQihiMFRe/cmva5efRnQWUp5hhCixXIaW0yHllImAW8SJ7AopBus2V7GC/PWMuu7Ygq2B4joZdFl6/aFo4etZfYD/wbAKFRRXYZlv0YAB9H3AjIlfq+doZfexaby5NabcBOgqhr9eiRzweTBnLJ/P3pkJccLT50NnCGE8LbEnFqE0FLKTCwyT4ndF9INluQW8fTXf/LRvDLKS70YRnn7jU02VLqkVLLmuRmkpITAC9R8yCQDDli6tjt7X3czuqGC0r48NjUhhEJ2Jw/nTR7AtEOG0Ltzcjx/9bfA6UKIgmafT3NfQEqpAW8DJ8buC+g6CzZtZ+aX6/n8pwoqyyswzIr2S+Yq6BpoOm9e/RqnHTjfKvxlAhqEhI2HPz6cGW8cBVK0azJXQQiFTukuzpjUjwsnD2Vg1zRsdT0g7wOnCCGa1RhqCUK/AZweu90XivDjum08+cV6vv+tEl9lOYbppb35l+tFwEn/nO2sffUWqMQyvxUI+B24T3wWPP4OQeYqCCFIS3Fy0oG9uezwEQzvmRHPrfemEOKM5pxHs3o5pJQvUA+Z52/M5/HP1jPnFx+V3hIMs4LmJLMQkgsO/LXZxq8Dm86U0csskVGlIrvAlRni7MO/B7NlHEyaYnJA383Nfh0pJaXlQd7+bj1PfbaEFVtKCOt17ufpUU40G5rtW5VS/hs4P3Z7QNf5YX0ej8xex/e/+fD7SjBlJc0aJGQqdE3x8vzfX2m+a8RC03no3FkA/N8zJzLw+Bf46Ku9Abh08tyW8UWbCn06lfDNXY82/7UAkHh9Id6dt5HHP1nC8q0lROqS+vwoN5oFzeK2k1LeDFwduz2kGyzcXMDTX25k3m9+/L7iKJmbGYbKNVO+Bw16ZZaxaXsmaM2opxsq4waux50Zov/Z97KuIBO0CMc9cjF/m7eYd+56GpcrSKC5l8INlSsOnYcjLYzLHiEQtIPS/D8kb2WQWT9swGlXufKokQzITo01FK+WUuYLIe5P9LUTLqGllGcD98VujxgmS3OLmPnVer771YffX4opmzFG3FCsx3rEBkJy/D6LALj75Nng81Tvb45Hv2pgVySpJz/Cuu1ZYNNBNUA1eHf+aAZfeCfd0soTf10pLIPUUCHoACRTxywF4KajvoXCLNDV6lczoqIyzDtzN/DStyvZVODdUQ6tBu6LciWhSKhRKKUcAczBKva9A7opWZlfwmNfrOZ/c8rwlpWjm+U0l8586vhFXH7UXDZszSLkc5OaVcqp+y8AAQGfnf/+sD/o4E4K0rNrCVc8dypLtja6ZuPuQ4pmUTlOGL+YQ4atJTcvHen3kNk9nxuP/woU2JqXwePvH4mwh7DZdWyeCC99sx+bitIbdzGpgNj5/RNC0DnDzSVHDuGiKUPplpEUu0xeDBwihFjauEnEuWaiBpJSOrDCCGvl/lWtAD7+9Upe+bSY0pIKDKOsWV1zDtVkwQMPMnxwDWOoBKuCsotqIw1478t9+dvjCRcUrYKhWSUsf/G22s/dEqxFnRRqlWlftrwXY/7xd8LNbJwKodC9i4cbThjFGRMHkpHkjCX1UmCcECIhS8KJJPR7xCxpV8VmvD7/T576eBvbcwNE9MKW8TPrGhOGrOPbex5G0w0IU52g4obSYBIjrryN3MIMsLe/Fcm4MFRQDR6d9j7XnPS1RWYT63MbWKTWYN9rbuGXtb0sVagFoCgKw/pmcOOJo5g6pjepbkcsqWcJIU5OyLUSMYiU8kbixGeUB0J8sGgjz32+jYK8IBGjpOUWTTSdeYuGcNTtV4Ob6jLmbvAHHYy84nZyCzoQmcHS04Fr/3Mq1z15mtX1pUpVToaSiiQcJz3OL2v6tBiZAUzTZOXGUp6YvYy5y/MIhOtc+6Qoh5qMJhNaSrk3UCfQ1xfW+W5tLi9/vZ3cLTphvaTlYzMcYez2iHVT3ViavQp2l47TGQZ7B03dErByS5fan1sBOzphQ22VH7GuG/yxtpj/fLGcRRuL4vmoZ0S51CRocGmjT5by6WSsbJOUmtt1U7KuqIzX5+axam2EcLi4daLmFJMbjvwKBGzfnsS7n0xh+tnvo7kNjhixjCe/mAS2DkhqW4Rbj/0UgKBPYe68URw2+Q+SOgfZv99Gflo1oFU+dySiM3dpPm/nrKVbRhI9sjw1I/RSgP9IKQ9tSiCTaMqChpQ8Alxbc1uVEfifOat5dnYRZUXl6GYJrZFdrQiJ8fF0flvSl/G3XA/lybgzy9jwwgw6p3sRx7ZsBaiWghAS8+PpbM7NoN9ld6H7XIwbtJ5fZ97Dti2p5Fxyf6suu/fsmszNfxvNqQf0J91Tx0h8VAhRJ1a+oRBW2OruQ8qZ+2J1YKrl0PSFIrzx25/c984mcjdXGYGtEJxvKpx30C8cstcazn38LOv3pEjL/6oZ/Hbfw5z31Bks35LTLjNI6oWhMOOELzj2gCXsc9P1lr9dNSFsJyXJx+Znb6XflbdTXJHUap9bURRGD8zi7jPGc8iIbjhstShkAAcKIeY3ZuxGeTmklB7gF6yefTsQ0g1+2bid295eyW8LQwTD2+pXNVQFjOaVEppiou/ELWVTTSJGiyftNDvS3QFK/XFaKUZ/zB5HGF/I3nwTEArInd9bu03jxAl9uPXkMQzqlha7krgc2EeI83d75U2Dwt09B+BGYshsSkleuY83ftrE4pU64fAusk2amczATskMdEgyA/HJDDuW+5uVzLBLMgOEIzpfLdzKyF4ZXJg2lKxkV03VYxhwI2h37O6ld1uHlpKBwEKs9sA74AtF+O+vf3L3W5vI3+onYhTQYUJB96BZIIRgZP9M7jlrPJNH9ohVPXzA3mKYWLM7Y2oxgrYBWP4YMWTWTcnivEJen5tHUb6Jbpayh8x7sCtIKVm5qYw3565lQHY6/bNTano9PMBjHDHryN0ZU7Na9zV0AjedABwRu73A6+N/v21l2eoI4Uh5/aqGkFYcwx7sQRThsM63i/MYP3AjZ6cMJj3JUXP3EfLhE08QgvcbOp4GHzboQCl/0rB051oI6QY/byjgk9+K8XsjmGZ94aBN8hDuQQdGQWmAD37ZyLj+XRjXv3OsgXijlHwsxmxrkKtMg/819LpnAvvW3GBKycbiCt6dn8vWLYqVqV1v4cS/IJuNqE6omA1zkZkKmMJyL3ag9KxdwTBMFqwu4sNfN9C7cwpd09w1DcR9gTPpRIOyMzS0T3Z5kIxc4Abuit0eiBjMXZvPvMU+QgH/TuKb/yJlaw0FEKAaODSDMw/4naG9tzKiSz6ZGeV0TfaS7vajKZa3wTBVKkJOtlckU+b1sGJbV9Zt68JLP46luNJtjalrVoxGR/KVx0GlP8xnv2/m4GHdyBzZPTYf8S75efa7Qozz72ocwdh9d3UM8refr8BqC7EDppQszi3ihjcX8fOvBsHgNkwZ2N3P0TEQ0cCm0y3Ny/kTf+WcCfPp3zMvxnTGCl/Vqf5tC6xlqTiJK8FCJwvW92HO0kE88OnBVqHH6HXaLXYh15wOjXOmDOSWk8bQMys5dgVxuhCZu1zaFUwbu9MD5Iu/ObD619WKgPeGQjw3bw0PvJVLSYEX3Sza+Ww7GqQAQ0W1RThz/0XcfcpH9OgXLTshsTK9IzTuKxFYsctV7mQDFi/vzT2zjuJ/C4diVGWbqB1PLenfI40HztmHo8b0xmmv5cbLB3oJMWCnkVUa+XWypWJxOTFkNqVkfZGXTxZuo7IMTOklEWRWhOTAvpuYu653k8dqVkQ0VHuE5y9+l/MO+w5c0vJSFmN9DU115EjAH31JQINRIzfyzsiZ4BP874fxXPj8qZT6nFbmiNo6dUwUIfHYI3gTuFCzpaCSLxZtYWz/zvSoLaW7AlfiLHh4Z+cLDlhf7075Qx87sBgYXHO7LxTh2R9W8sBbeRQXeNGNBEhnKeiWUcb6p+7Acca/mzZWcyH6yL900q88Pf0VsGO5//20TNlLEyscNKrKfD9/BCc/eh5FFW7rR9SShqSpMLrPFl6/7FWG3XTLbur49eseQggG9EjlgXP25cgxvWIXW/4Ehol9qFdKaxyzZScX7jMNGFRziykl64sr+Pj3AipKidbTSICqoatMP3Qe9pQIae4gZZXutmXp6xr9uhYz/6H7yepUYZG4nB0FZHYJFSvHXom+V7D0aTP6f0NaiytYVZgC1hgH7buUwrev59O5Yzjx4fMIBe0tp2MbKpcf8gNDh29thMFa//FSSjbme/lk4SbG9O9M98xaeYh9gWn8ftkz9Z2vUbr/zq58OjEP0JBhMH99AavW60TCvsYH7UtheQUElntLSI4buxiAW4/9kr8/MQ0yyqqPb6XHalW02n1nfcTNZ0U9QoXsmsguLGlaBRPwghnUKA86CesqKc4QLmfQOs4ec2wFFtHjqS8iekwhYIOjJv5OcOLvXPyvaTw3d+yOIKRmg2mpOZOGrQKipSHW94DUaBhzE5OAw2GTn1duZ/mWErqmuWuWFVOA0zE274TQX8cPH5ULnpsATIzdnl/u46tl26koFU0qEHPM3suYMno1W3PTCQfcZGUXMqR3HgTgrAPns62wEygRHPYINleE1+eOs+pbtCQiNlR7mBVP/pOBvbZDGZahF4/IEouU0VQHs0Tj96V9ePOXMfiDDtYVZPHdmt4YUiBrrJaqisSu6Zw0aiV9uhQyqFs+E4espUfvwuqqKRVU50TWhIIl2QsBDzx740vcdPonjL/2ZkoqkhKakdOnUwmdkv0EAjZCZemMGLmSvn0LwID/THuLa/99MVrnIlQpKQi48AYdVAYdux44LiQbt1cyd3keY/t1JiulVrnhiVJ+MkFMid/MRTDq8PhDLvr8bmJSqyKGySdLN3HtyyvI22gQ1vNobMxGj9RK1r1wCzZnDUlSikUYD7VcXouW92KfFshQroWwjU5pXvL+ez2ajWqpHAsTSMKSyDp8OW80N759DEu2drF+6oYajcU26y/yIoW1oFJVvFGRuG061x82j2PHLWHsqDWWa0/H+lFBXXLL6KuT9ed5917IKz/sbR2XAB/2pRN/5ekbo2sb5YATS/UxqPbGKNb2TVuyGHHVbXgjja9jJITgwBHZPDxtf/bu1ym2TO89YsSMW+Oex7RVdTbKFwd1A34HutTcXuD1c8+ni3jlYy+V5UUYMl6xlAYuokQf5Xf+7QvuOPcj60ZVPWKrSGKHA669mZ/W9AGtBf2vYTsDc/JZ/fJt1t8FxKQxROfoBJIhUqnx3EdTuObto4hENOuR3NT5SmGpDjYdu2py1r5/cN85/6NztzJrf1H0uFhi1zAcf/hjIBNuuzoxKoiu0j29nDUv3ILLbVjCp+aQaYAGL388gWnPnFa94tkEdMpwccfpYznn4MEku2o567cDY4R4PTf2HAEb6gwkZe9TsErg7oApJb9szOeylxaweplKKJybmP6AQQcXHzGXZ6551aqlHL0hlT4XB95yI4s3dAdHCyZ1hm0M65nLsuf/z3palFKbzFX3KMv677lZU7jkpeORYZtlkDXHil4Ncu/TO5c3rn2Rvv3yLUKVUPfJUWOOxWVuepx7P4GAs+kqSNTL8/5NL3D8PgstX7sEHFASSOLgW69j6aZuCRM+qqpw4oF9uPesfenXNTV2oeVU8S/5Tuw5Gvv1rrVB/oRKnBC8QMTg903F5G3T0PXK+GRujDGgmKzJ62yRxoUl9UKgKQar8ju3OJl7dym0yBzGemrUJHMNqbxkdU/2v/l6fD6XReTmzCAXcof34pc1vel39W3s1zeXD297gk6dvBaxAlQTu+rGF0BmZz/+D68i57R/sa08pWnGtU0HbxLPfzyB4ycstNQgDbCDd4uLpZtzEvokNQyTJRtLWLm1lJ6d6nTdOkfeKGaJIXfX+kAKWR9Q6wXZwNGxgxd4fXy2NBd/uVJ/zEZjpJNmMOOYz0CAHoCvvhgLDnBmhLnm8DlWbbqWQETD4/Gz4bUZ1bpqTTIbQDqQDBc+eB6jrrsZX9BhEbkl4yyiP5yfV/Wm87T7ue6JMy31rBPWD67mVFQsdUmHvLduZFj3bZYq0BR4fFxz1FfWexeEgwqo0GtIIWnJldF4lsRhc4GXH1duo8JfR7AdDWTj20rNl0JhErVecSrt66bkz8JyVm/SiYTDmDKYuBkrJpMPWc7Wbel4zn2Cwx64nJEX3gnAP07+tGV0Z1MBm07R63+3/i6hLpk7AwL6nH0fL3x1gEXi1nIlwg5iP/rxQaSe+G9Wbcy2SG2jtp2uYqlNQGZaedOLUyqSyQevAOCMO66k39+e55ufhwLw1DnvVUcYJgjBoMGCdYXklfgw67bbOpH+91HzpdA7lVovOCv2rJBu8OuGAspKNEzTT8KyUQyF6ZN/4PeVfehx0d2EIzZwB1i6OQf3cY9jCpUemaUtkBQgWfzgPTg9el0DMEpmr8+O88Qn2Jif1baqLdkjVPidDJl+KzOeP8mqlpRKbYMtE/LWpzN35YCmGYcRjTtO/JyA34Hz2Cd4c8FwttpDTP7nlVz/+GmcccR8RIJVLyklf+aVsyavHN2oQ+izWPAUNV+CIdUVvOSK93oDS6hVzhDyyiu55PV5zJmrEvDnYcpdRvE1GGmuIGXx2ppFvSDJzhDeRvszG4CwnelTv+SJq961PAc1fztRMpeUu8k680FkW492C9sY0SuPJc/daf1dgPV5OsGRt1zJ5wtGNFnXnzhwPfPW9EVG6+jtQERjaK88tpUnUepz1z9AI+B2adxw4l5cc+wo0jy1uOAFRoqhCzZWbdCoeLDmAVOJIbMpJWsKyli72UAPGQmvgBSXzLAjkqxZyayrJCd7LTLH1uqJkjno1+jUHsgMYI+wdFM3ko5/jG2v30By5zAEQFYofL5kcEIM17lr+lpvYtUtm86K3C7NYk8EQpbasa3UT4rbXjPvMBmYSnjkjtBmBXcfdrzgkDqDRQwWbCykuFjDMP3tv0NVTWgGqx+N5i0EqJbOJtZjG8g442HMKpdce4A9jC/gIvW0R9mSnw4uePB/h1kut+ZGMxnH0pSs2lrK6tyyeGrHIVRuouqlsKUEtpQg1+ACBsQe7Q2FWLi5hGClklhjsLURtnPqPgvJ7llmqRpVenPVErYDhl10OwGfp/0VdbRFkLpKzwvvZsPGzjz29UHtPna6oCzIqtxSgnUrlw6Q+QNcnBiGE8NonFG1Urj/gcRE1gEUeoOsyfWjh23trk3xTmEP89bfX7Ci12pCAqlwz6uHs2JjD3C0088cNf76To/WamlLkYuNgD+gs2prKd5AhBR3rfjrQcCBzJ/9FYBCZl/I6gOwP7VjvtBNyZYSLwXFYBih9ttAPhYRG69c+Lq1kOOltqrRCfI2p3PrO8e27KLOHuwUhmmyPr+C7WWBWPedHdgP+wqwr0DhrSfgjacAescOEtINVmwrxe/VotK5A6RYSYGwRzjnyJ+t4Pyabtmo/Tnwmtut5NQ9aAXU46KVsLXYx6ZCbzw9ug+8AvIVFPp5kFvuTgNGxh7lC4X5bdN2wn614yTA6hozjvnCIm9N72O0ZcO7n43BV5nUssFQe1AD9QvNwrIAa7eVEtbrOCZGyvmkUfEqCmmngdXopw6hywIh/swPEgnLjqM/2yLccconlu5cUzong/QpnPLU+R2zCHoHQCCo8+e2cnzBOvdnJDCC7DQUem8HKwGx1jPWKlzup7hUYBqRjuGui9g4cOB6tDSjtt/ZBFzwxIcHQ8je4WtgtFcYpmRjQSUllSFiVsE1oCv9B6EgU8DK1apz8uaSSgKVNsyOoj/bIjx77pvW+5rSOQlMr8LVb5y0xxBsy5CwrcTP9tI6hiFAXzwBFEIGwJDYvWHDZEVBAZGQiqw/ybb9QArsms6QEXlWSlMVotL5jW/3abnIvj1oNIorA2wr88frTDuEsIIinx6ZCYyO3RvWdf4sKEcPKUjZAXRKXWXGMVZXVWqaA1FH5SWvntK60XN70CB4/WG2l/rQ6xbMHy2fGpmpYOVeDI3d6wtHyC8NYkRITGZKa8Omc+xe0Q68NdWNVFi5pBv+kL3dLz78FRAKm2wr9RGK1BE+Q4EsBcuBVcfpWhGIUO5VMA0DZDuXXNEY4L0Gb6rtqos+tZ6fewDUFyS1B20Khi7JLw1QWdfToQEOBSu9sRZMKSnxh/D7VUxptH8Ph6lwzgG/W5nkNQntAMOvMHvhiD3GYJtD/AUWKaGwIoAvGIn1dACkaUCf2K2mhCJfJeGQipQ1y2W2MZgCpyPCsifvAX/9vWrCERs9M4ut9PuaMAA/zLr2aWx2vU5id7uBCSJNMmf+YC564bTWnk2CEJ9zUkrKfCH8IR2JRNQmfh8N6BfvpFxvOXpEgZZuZ7w7MFReu+gl+nUr2HkSjYIlmWOXunVQNZPhQ/OsaLv2rEIrcPbcc60l+w6+ylkeCOENRCV0bUHeVwN6xJ5gSkmhN4AREW07IMmmc/JBC6xY5oYk0cSm01V9GWUJnVXLIw0KN6Xy89oWrl/SrKi/vos/FKEyGEHWZXRfhbpludFNSbHfj64r9ejPbaDxT9jGcaOXgUdaafx/VZiAChe/cDqEO5IfvX41NxDW8QYi1HVF41Kw8pljxpKUB0JIQ9SjmDZOp9YS6RazR3jmgjcsPbhj9s9sGDxAJXywsOn5gu0FYd0kEA5j1mV0pgZkxG7VTZNgxIxyOQEGoRQMztnOs9PeZuLdVzfd36trDO6ZS5fe5Za6UU/jVKB6ISVC7QeLxNKbnbRZm3eXkIAbZn06ts3qzjlpFRw6+E9e+2lswvz8hi4JhAxk3RuXqRHHB23K6AkSZCIsJV3jskN/YML+qxLzoTSdFy58E4lAVoqdS2hpuXrU2OtGyW0GooUS24AWtdvQJDIkOPv5s9rmolDExg1Hf8OVU+fw2vy9EzasYUqCESOehNY0dlRpi87BkJhIIoaMln1NgPhSDQ4ZZHW4zUr2UVSYAc6mhaMedM9VGKayy/lJKXjmvHe4+MTvdxRcASAdvpg7nCMeuszicnuNsJPC+gbaIqFVnf37bEBNNlAViWEoCfmeTQmhiBHvzmcp1C7LjU0V+AJBDIOo43r3J5Ack4fndoYYMXgLSPj3mf+rtYhhU01sjYih0A3FeoJE6y3X98LvIhCy1ZXiCmwo6Ax+l1WJdhfjtNnXbn9zzYyqXot+FyiSccOsYqDTD/0BStKsfabSpOJBUlpSOs7CiluDum4Mp92BueOCu/+V3XTk98y48EPMMpXy4iRS0nxWvbhSOHPCfI4atQxNSpJ7+ijelsygq/6P4kAzLWs4Q6R64vv0pJBNflLsQTXuP/1jJgxfx+YtGQS8SQzqtwlhM6ESbjr2M0Znb8fmCJCe6SfF5ee4By6j2N+IkIP6KWnETZyLw/zdwq3vH86vG3rx4T2Pk55WbgXT1+BNeg/Lz7ZyXTajrp1BJEGPoviQKPU8AUJVxcj3ICH41/uHcf7hd7D/qLXVG6Od7rKzyzn3lDk7Np9x56WNIzPs1N6p15yqLpjeiEeDkHz06whcxz3O78t7W94EwY5Gk95yJ+fcfz5Dr7qNiK42s/4qMOupiOlQjfZpDLZRlPiddD7nX0x76DwIi+oKrirWWkEYFq3qhXbMk7z5U9OMxPpuW9w7raqgqDJaYLqRd9wRJliczp2vHmNp6VVdoJKhIC+V174fbxG5uY2ZoJ3Fm+oshgKwqSgTwonrsfeXR7RH+csfH8L1z5xSO+wtHZat7cHov99oNQ5tgs9cCFBVQdio++TVqNtsAYlAVU2EaGIXyZRKjol2tqr54fr1347TGSIYsjW5bcEu4Q5itxt1VQsJBwz8E5ztrxqUqkiMptZ5bk44g+SXJ1nvq/o8GZCdUma9b2IVJ0WAQ1NxanWoq2pYURC1lr9tQsGmWb8E0RRCKyYXHzsXgFuf+htz54/h5X8+TN/ehZw/4Rdmfn5w01a3DJUu6eVsfPk2K60qzvcUlgKHMGq77ABK4ZjRi6h48Vo02pHmkQqb12cy6Ia4PXPaBmw6/7nwDQC++XEIS5YP4tqLPyCzh5fBXQtYtSWnSbUCFVVgt6koSp275tewKrt1qrlVVcGhVTV9aeS6csTGuZN+BAf0PvM+NhVlgKnQb/qdfH7zMzx15RvM/PaAxo1dBSH535XP47SH40SkWHBWrRTGtkUzAQ2SU8PtJ8ou2s/ktnenWvmPbbHcQkRjVP9NJHcNcOezJ/B/H04Bn5uHv9uPrf+9ia9nPEH3y+9p0iVUBVx2LZ6wLdKgbjidIlSS3RqKKkE00hNgiyANldSTH6EipmfeEfdexhkHLrAWWbz1MHFXkALNprP/uLWWF2VXUa6xn11gOSzbU2CTB4wylXd+3attkhnApnPDkXM44c7L+OCn0dZ9T/KRW5yO/bgn+P3uh3C7QvibEEhlsym4HbZ4ElrXsBow1IKmCJIcGopmIpoQ9v7qT2MsB3qsWqGYvPHjmKZ5NyIaN0z9yrICYovGdEREGxY99NaUHd2o2irO+s9Z1uJJzftuixDRVUbeciNqE+0mh02Q7LJRl88UK+zwFFZDEYJMjwu1iYQG6idtU1119mgFpBAdn8xgZX5KuP2DI9tHadx43qsokZtq0LrsGh6HDSHqjFOsYeVx1IIQgq7JKag2v6VytDVENE4+YAGOzLAVRZe6i+NVLNPXT23yVzlxUqLb2/IiiwYLF/QmHN6TnZ7sspPsssX2LQQIaMCW2K2KgK7JHuwOHSEcNLg7bEvBpnPVYd+zrSQVWbrrSDmpq2SlleOw6XU6RJlhhY3rM3DYIyjN3pyokRBAquSYf1/SAg2U2gric04ISE9ykOSyxTMK12vAunjD9UjJxOGKoAg3ArXNpWJNvPvqhh9sKozqlcuip++G4hrbVZBJcOyt01me17l9RNz9xYvhCCHITk2qT0KvV4jTG1kRgk4eD0lJERRVbZtqx+5AwuIt2ZbKUTN8IAyqx2TSsNV76nK0OcQXLooK3dKTcTvihiFtUKgnRdRts5GSbKLaJIJ2nqsWNaKWrO5Z218d/YVffPA8cHeQ+tcdHDa7oEuaC3vdVUKAsqplhzr6hMOm0TlNRbWbCBGP0O1Ml4vYeOCTKdb7mnp0OQwfuRWnPdL0Lqt70OxwOQVdM5LQ1Dr3SgdCCtZK4YrYvXZVoV+nNGxOHUG8AJ52oG/WhKYz6/eR1V2uqhAGFHj6rHdBb+eq1V8AqR47XdNcqHWd0CuAIgXLTPojdq+qCEZ1ycHhDqMoVZ6OdgwhCUU0Vi/Lttx8Vb9HBQjAWZPn/2WyptstBHTPSKJLmrtm880q/EF0YQVgZexeRQjGduuNKyWAotiavsDSFhCxcdmr0VJZNR8wlaClGDx06gdWBf89aJNQFMGg7Cwykx3xPBwroXqZYX28AVKcDlIz/Gh2EfVHt3PYIsxZ2R+zQqndAFoBgnD1cd/uKdrYhmGzw4CcVDzOuE6K9VBN6HziGIYeu53B2W4cbgNF7Kz4RTtCROPe94603Hc1jcMKS0r/95JX9wT9t1GkpqgMyE6P5+HQsTi8g9BLgSWxRzk0lXE9u2HzBBHCSbvXowE0g9vfP9JaMq+Z764CXjjjmF9wefx7DMQ2iG6ZbrpnJsUzCJdgcXgHocuIQ2hNERzQfSDOlEpU1Y6oW5Om/UFIZNjOG1/sY/mkay68RZNXVj16147WwnvQNqCogmHdOpOTEdcgXEx0PaWmM29jvIFyUlLJygqh2ZWOoUcDaDpn/uccywOfTG2PRxH07F3MP479AkId5PN2ANgdMKxHFiluezyDcHPVm5qE/ok4YfIpTgejeqZg9+goooMsDwsJYRvnPnpO/Lp4FXDvxR/Rr3te+/V6GCqKkHw/48mOoCiSnqIxpHsGTlsdVTAM/FL1R01C/wCsjj3aoakc0r8vjtQKVMXdMdx3APYIr/6wD4Vbk61iaFUGomBHccc/X7wduytoBdS3J0Q0UA2KX7mBiaNXtrclsDoQQjCwWxoDc1JR1To/z9XAd1V/1CR0AFgbe7SmCMZ3601WFy82h73Z1Y6sJB/d0itaJkxS1xh43Z3W+5pVSBWspFoBRW9cj7Dp7cdIDNtxOMIUvX4DaRkBVq3t1iyXEULiaCE7w+aA/Qbm0DX+gspaLO4CdXM95hAHOSlpDO/lxubWm9d9F7bx3EVvMuvq51uGQJpOWVkytzx7bG2/NFhejwJITgqz7eWbLSNRb+OSOmynf3YBwQ+vJDPNDyZozUE6U2Gfvpv5/pbHWyT+JTVFYUyf7Pr8z7U4Gzub2dTugg1Yasd+vbOxp1SiKO44pyUI9ggH91/DPoPXtVzOnD3CfR8ewaq1XSzVo+b9j5K6S5aXwv/egM0WaZs6taGCFFw59TvWvjLD+gx1EusSeT2Fyw+dxz7j/2z27BkhoHeXJAbmpKLVVTe8WJzdgVhmbgRWxZ6lKYKDew+mU1cvNrs9ccahoVqS2FCh1MqjSuvpgyTQVBOKMqqPaU6JLQVDrr0dPaBY/QzikDor1U9g1nRyskrbFqnDdtyOMIsfvY/Hr/qvVZ+klDjlgxIEU4BmcFC0PHJGkh987l2c1HjYHIIDB3UnJ8MTT91YRYx3Lt4z9HVgXOzGnqmdGNXfRt66CKFgEqZsSJee+qEIydmH/kyW209luRNZmsGUST9aZDLhw6uf48PPD0FklOD0hKmIOPjv92MJG81wpxQTIhpZZz1E2azrrCpPZVSTIkpqNQty37yRU++6lHfmj7T0/NZKWA3bwB7hwsk/8dyNL1nbCrGM2gQ/QHPSyin0JmOYAlOBY8cspWefYjDg6XPf4bSnzkMIaWWKuQP4wzaCCeqbnpGucMiwXiS74o73euwGIeuWGu1OnDzDsG7y1or5zPjveorWZhAK5za5ZfLtU+fwf5e+V71BYt0UsB7/NW7Mi+9P4oIXTmrS9XaJsI1B3bex6sU7rEUWb+05VDW6Jwl+XdqPibdeQ8jvbNn4j2iBmVHdC/jk9sfpllNqpTnHJgADpEPB5jS6TG9aYZcHTvqMG6fNjt4fBdymJXh0rGz0SPTaqbA9N5UB0+/CmwDPkKIKDh2fyRPnHcbAnLR4/ucewNZa58QZZxvwSexGu6ZwRJ+96NGjHIdbRRFNf8zc9b/D8Rz/GBvz060NRVRXqyyyNpVVOOhyykNc8PTpTb7eLmGPsHprNuMuu8XyeqRQW/2IBjFRBONHrCP4wZU8Nu0Da1/Y1nyeGSl2dLga1Xsryx+6n0X/+T+6ZZdaAiC2LomMvhQoS0Bcyk3vHs34y2+1pH9n0yJwJHqNEFY4bip8/tNIul5yL95QYqSzyyM5dq8h5GR44pH5Eyyu1kI8QhvAq/EukO52MnFwJ5zplahqUj2n7wbsYfwhB30uvJsP542uXa0yDX5cNID0Mx6hoDwZ7C0kBe0RFqzrw8iL7rCkT6yhWFUWuADww1VnfIL3tet56IzZVh8XQ0mMvi+F5U+WAk01OXX/P9j85J0seuYuhg7eYv3gi6l7C0x2zPvXJX0YfN0dTZ+LYvLbnz3RjnmS7xcOsn7sVd+DHYpLk9j/6ps58q7Lo8c33fMthKBntoPxA7rUlz/4KnGK9dfHyB+B7bEbbarCWSMnkp5diM3hrMc43E0ppRrgd/L1L4PBBqRjVay0Q9l2jyWZWrpKkD3M0k3d6X76A1GpRN36dyqWZCyEJE+Q68/7CP3d6Xx07YsM7hZ1MUhRbfTuSnqb0R9C1A3msOncNPU7vp8xk8hb03nrzqfp0avQInIR1YSqNQZWlcJkuOyRc9jnHzcgE1Wl1B7BKE3l8VmHWmqXhvXDSYKCLan8vKpvQuNfNIfkqFED6ds1JV4w0nYsjtY9r57xcoHngRmxO/plZDFmoJ2ijWFCgWRMGaB2tHwjfp3uIA9Ps3TpjRszCIQcDBmyjaMPWYTyZBjTbM4K//XAHia3OB3HcU+w5pk76JVdYnkPdGqLAQVL147q18cc9hvHHPYbvnwXC9f2Yf6G3iza3J3ZiwbjDTrqfDsCsGkGRw9bSf8uheRklHHM3kvo12+bRRii49dHYqil2+duS2fstbeQX5JqZeAkUgtK8XLZlO+t98nsaJU3ZHguTncgoeWRO2UqTB7Ri1R3XJXpeSyO1sHONPcvgFuI+UocqsplYw/hh8Xf4CvpjhGyI2UT+pREbIwduB57ZoQ584cw6e7pIAUvXfIm5x07j3MP/JWXvp7QOoH39gjhiI3eF/2Tt65+nVOn/GyRp0rXr4mqdOPoV+FJDTBhwgomTIima0bAqFDQwzbKQ07CukaKM4jHGUJ16bWz0SWW+81L9bcfj5hVxWGjtWOvefxMHvtyf0tVaY50MkUy+eDlAJz3z0tYsGg4b9z3ACMHb+WCiT/z1GeTEnJd1SaYPLwPQ7unx0uGlVjcjIt4Xo6a+A44KHZjuT/MhR+8xrffdqGiMIBuFNGUpNnl99/PB3+MZMYbU6vdYLrKBZPmc/epH5F9xT8bPXbCoKsM7V7ATw/eT2q636paGqDhZoSGpVIpVLfoMGq8qoyshqCKyNGWqd/8OJJjH7oAv9/ZfOpZROP2U2dz5zmfkHzyo/gCLuupqem8cNHbnH/YXMTfnmr6dYSgcxfBMxcexpFjeuGoG4z0PXBwvafvgtCXADOJuW2mlHyxdjmXvPYjxSt7EQzlYcrGV8JPdwcorfTUXXXSVVKS/PjCtmhPwlZG1Pf7zLQPuPiEr6rVjZaqfhqtQFq1TD//98Gc9PD55JVFNzTzqt3UUSv5bMlgjIhWW182FA4etpaFm7pR0cSCPaoNTj2kJ/edfhA9spJjvRsmcDnwTH3n74rQdmA50D92R1kwwHmzXmfet9mWlDabJqXbFSIadmeYVy99m1On/GBJXwNrMaaJXTzqQGKNnxb9Owyf/jCGac+eRoHXY3lV2kIyghQJsXM6dRHMvHAyR+/dF5ejjnT+ExjGTqqB74rQANcDD8VuNKVk9upFXP3GbxSs7EkwuC1qIP5FIAUYCjZ7hMsmzecfJ35G157RUtsmluRu7NNfYBmEVXq1hDWru3HPrCN585dRRHQt2qu8Y1Uh1eyCkw/uxv2nH0LPutIZ4O/AwzsboyGEtgObgK6xOypDES748AW++SabinyDiLGdv4yUrkKVa86m0zerjMsO/ZFT911Aj54F1V4KsL6WCHVJrlKtW9dApERj4frePPHFwXy0cBjekN0y9jSjfRSV3F0IQU43eOHio5k0sjt2rY4Olw/0Yhe9GhpCaIArgCdjN5pSMmfDSi59Yw75y/rh9xVgyvbU4yHBMFRAgmritumce8ACRvXZwoBOhWRnlZDh9pPh8aNFdV1TCsoDLop9bkrKk/lubX+8Xg8v/jCObeVRvVjXQDGav1tYK8PmgguO6MdtJ04gO80dTzpPB3ZpdTaU0G6sQh49Y3cEIwZXf/kKH37hpmyLm3Bke5NjPDoEZLSndZTgVRAQbZfHjh7jO1DVA7sl+je2ISiKYMAAhRcvOo7xAzrHc9VtBoZgRazsfKwGXtMP3B5vh9OmcuN+x5PTPx9nkg1FSWrgkB0cQlqroDF6rsSSzGa0+XwtKKZ1zl+IzACuZJOLDh7HkPh+Z7C416Dwzt1xNv0XmB9vR4YrlYsOGE5yt3zsttSOkx2+B80OVRPsNzyDqXv3JdUTd1VwPhb3GoTdIbQO/CvejnSXypnDJzB2VCWeTiE0JX03h96DvyKEEHTONrn2sP3Jjh/ADxbnGuwv2l3WvQ98Hm+HQ7UxY+IxpPdZj8vjRhX1qB5KG8r22INWhd1tMO2gEYwb0Jnk+PmCn2NxrsFojBi9mjidsxyawoguPTh/Qj+SeuRht2fEVz3Mlo/JEIBTa1s9YloSbbEuh6oJ9hmRwhn7DyfdE7eaqA+La7uFxhB6DfU4tx2ayhVjjmTcaC+ezn5sakbrlw8zBcO6b+On2x/7C3WQqgFd5cHTPuaIkausVcU2AKEIuuSY3HTURPpnp9ZnCD6MxbXdQmM/4b+wlsTrwGO3c++hp5Az5E88aXZUJZVWlRGGxuWTfmT03us75oLErqAZXH/8F1x3xLcgW4jQYufXcafqXH/0ePYd2LW+XinLqcde2xUa+wl9wIXEyxgQgoGZXbn2kHGk9N6Aw5naeqV4pQDVYNIQqyBUt/QKaGxv8faIaDkwkiRjem2CllK7ZP1uR80Bh4/rwknjh5AWX9UwsLhVR61tCJryk50PPB5vh6YI/jboII7eJ5mkbgXY7Zkt5srLTq3ApprWjRSSw0evYNCAPDDh6fPehmQfipCoQpLqCuLqQG0oqjKvd8AU3HbcVwBk9Kgk3ROoVRhGbWF/t6IJBg/UuP7wiWQkO+vzajxOPe7hhqChK4X1IRn4hjhlDyKGpDIU4oxZT7Pg5xy82+yE9WKkjGcUJq5T7dWH/sS/r7XclnqBiuYxLKdPNEPZ0KOx+WmwNTedYVfeQUW4nbeti8KlGvz+wP0M6bONLRsziRg2enXZbhFXA5/PSX5BBq6kADk9S/nyp5Ec/q9LWmRuiqrQtYfOU2cfxWF79awvT/A34FDiFDtqKJpKaIC9scoxpcTuiBgmG8qKOfu9Z1m3cDDe7YKIXoisq6kkDobC0OxClr9wu/U7KaO2FzMDUOCb+cOZfPdlVuGUjhInIQU21WTxo/cypF+eta2CHVk0uNkRwTfrq/Gc/Ni5CbrwzgWSEIL0riEeOGMSJ44bSLrHGU/VqAAOARY2aSYJIDTAjcAD8XaYUvLVhoVc/cFnbF+yF5WlPgyjrHlJHQ3E/+L2pzls1DJLG4u2cysNeTj+3suZu7Jfx2wzLAVIwSn7Lea56a+QIgLVlo4dtvnTmXzr1azYlJPANK36CS2EwJUW4cpjB3HDEQfhdqq4bHGfiDfRSEOwJhJl9v4LmBX3AkJwcM/R/H3yeFIHLMed4kFVUhJ46TiwR6AimcfePdQK4dSwMj3cUFbg6bhkhh2BTe98uw9fzh9uJQYo0Vcy/OeDg1ixfEDiyCx2TmZ7ss6JE7O57OADSE9y1EfmWSSAzJBYVp1JtM9FLByawrnDJnPFlAGkDFiJOzkFVUmmWd15ST4urcpQTopeSoU+QwpITvJ3/K6xHj+TR0UTdNOjL+CovZZCRlnirlPvE15gcxscsX86d049jO6Z9S5tL8XiTkKQyLsawppYcbydNlXhir2O47yDs0npuw6XOx1FNKMLTTE55rBFAFxy74UMPeVp5i/qCwJuOPLb9lPvuTHQNfp1KiYtx4cMwmk3XM91j5wFwD77rLM8QM28yGRzmhw41sV9xx9Nj05xG/2AxZUzqdbym4xE6dA1cTb1VF4CCOg6N815iXfnBPGu70vAV4Ip6zFqG5unFtG47tivePjSWaSe/CgVfjcIE1STf532MTec8RniuARkKDcEUtC/ayGpzhC/b+jRMqGhpkLuzFtQPCbdz78HI+ACxaRrehnb3r6Blz48gPOfOrfZOudqTsm4vTWeOuMkhnfPwFY3+6QK5wCvJfLazfHcfQ34R307XZrGfQdN44SD7CT3W4fLk4EiqnSCGDR2Zc+ms3JLDrYTHqci4AJbZEfq0o3/PY4Drr+RzKRG+e13H7rG09Pe4c3pL1oelRaAzRbh1e/2J3vavRi6Cs4g2MPkl6XiOv4xthRmNROZBZpTMma0yszTT6Jfl7SdkfkfJJjM0DwSugr/ZifBJb6wzt+/fYEP5gWo3NCPQGU5hllJ3ZpbzYAEZSg3FJXPXI+nUxBxYgs9FXaGZvrsQgg0l8mYvVSeOvUkhvXMiFdTowqPAdckfBI0b9DyNcCL9e302DUemXwBZ09JIW3QajwpKWhKass0JWpOMkth5RYaCpSkoQiJp1sQ7JDsDEFhprXPUFtMYtdCs5BZwZ6kM3E/O8+ddQojemXujMwv0kxkhuaV0FV4A6i3Fm5IN7j3lzd4eU4u5WuG4CsPYBjlzeunbiaoQvL0xW/icegU5idjelPZd8xi9tvrTzDhj9W9+O7nsYikCtKzfLjtIc5+8lxC7dhAFULBkRLm0P08PHrSSfTpnFJf9BzAm8AZzTqfFiC0BrwDnFDfAWHd5JGF7/LMnJWUrx2Orxgielk9y+RtG4cO3MjXjzxYvUFS3e8ki1o18c67+yJemb9Xy00uwVBUFVeGn2MmZHLf1OPpnpm0MzJ/APyNxlcraRBagtBgFaR9E5hU3wG6KXlzzZfc/833FCwfiTffQThS0rRCkK2BiAY2nc/veIrDx62o7cSUQAYs+rM7Y6+/GSNkbzZPQ3NDURVSulZy4ZH9uPnQI8lIirucXYXvgdOINphvTrQUocEKZHoDmFrfAYYpmbNlMbd99RHrlvSiMi+NUKg0TsneNo5oxf0XrnqV86fOg2hBJdLgy1+Gc/jdV1h/t8vsboFmF2T18HH91LGcv98+9cVmVOETLDWjoiVm15LpJF7gJCxSx22WoiqCST1H0eekbG5Ie4WfF5bh29yTgNeJYVQgm/dplTgICYqksMhjeSMzq3clVT1x2hyZdx3xKBQFu8dgwKAI9x0/lYMH98XjsO2MzLOwyNxiumNLSugq2LDcNpft7KDKUJi7f32VWb9upXT1MLyFSlSvbj8qiHz7CvDANz8MxetzcfzhvwPgOP4xwuHEFQdvCSiqgifLz+T9k7j/qL/Ru1NqvHJdNfE0ltu2RXUq9c4772zJ64HlaP4EK9x0v/oOsmsqB/cYTU4nyYrIL4QidmQwC9MAKXV2Lk1aOXcwbOfkfRdyypTfuXnmKVzywqm8/eM4lq3twSmH/MbA1GLe+2l8uwiQEkLB5oQufb1MnzqYfx55At0ydmr8ATyCReYWfwy1hoSuiRnA3Ts7wJSSTRUF3Pbjy3z/u0nlhv74SyGiV7Rpaf3b/z3Cbe9O5fOFw6oNv5CdYb1yef/6Zxl4Q51uHy2D3VhYUVQVZ1qQkSMN7jvmOMb36oXLru1MxQC4FWhaH7kmoLUJDZYr5z/sqEcfHyHd4LVVn/PvH+ZSuHoAvvwMQv4AhuFvk7X0FCExdbVuyVtdBc2w9rfRLHShqNicJqndypg2aSDXHHgEnVJcu5LKJcClwLstM8v4aAuEBhgKvEycVK6aMKVkRclGHvx1FnMXBfFu6oOvyEk4VBntbNsmPks7hkCzK7gzfew1UuEfU6Ywsc9AXPadGn5gpU6dB6xoiVnuDG2F0GDp1M8Bp+zsICnBkJL31n7HU798w8Y1WVRszSFYDrpeiSlD7CH27uZoClRNxZEcJruvlwsm7sVF4w8hI8lRXwxzTbwDXEQLueV2hbZE6CrciBWJlbarA32RMPf89jKzl2xg+5/dCeR3IVgZwTACe4jdIAgUVcXm0cnoVsahYzO5a9Jp9MhI3pV6AVa25n0kKNMkUWiLhAbL+/EwO/GCVEE3TdaX5/L4wll8vayQks3dCG7vRLDSQDd80eXz3ZNWHf+HYBHZ7tFJ6VrOfiM8XDfxCMb16IvTtkujD+BnrFYlPzf7VHcTbZXQYOUn3xJ97RKGKfklfxlvr5rLp0u2ULG5D4HCNEKVEl0PRrt0tbXFjJaFEMoOIrs7lXLQ6FTOHb8vk/uOaoj3ogr3Rl8tFFC+e2jLhK7CRKx2GCMacnDEMFlSvIbXln/Nl8u3UpGbQ+X2TEJejUgogmkG2qRXZOdQaMqPUQgVza7gSA6R3LWUA4dncP74Azmo97CGSmSwcv+mA3MbPZEWQHsgNFjS+gasXi9ZDTnBlJLN3nxeWP4xX69ez7YtKXi3dSVU5iYckBh6MKpnJ0JqK1aKV52vcveNs8SpOwJV07A5TZzplXTpXsnEYV25bPwRDO3UHbumNpTIRVi9TR6kjUrlmmgvhK7CSCynfb0BTvEQiIR5YeX/+Gb9SlZv0inMzUQv7kSoUkMPmRhGGCnD7TIGuyaqVArNLlA9flI7e+nfVzJ5SF8u3es4MpPc9SWr1ofZWItfS5pnxolHeyN0Fc7GchVN2J2TTCn5YdsffLXpDxZs3sKyjX7CxdlESjMJeW1EQiaGEUGaVeRu69+NQAgFVVXRHBItKYAjvZT+vTTG9u3CEQNHcmivvXdHrajCPCwXasJz/pob7ZXQYKkhx2IZKL139+SykJcFhcv5ffsavv5zFWu2RDBKuhIsSyNc6SASVDAiso0RvJrAql2gOQ3sngBaagm9ugkmDenNfr0GsH/OcLLcKQ3xIcdiI5YR/hHtQL2Ih/ZM6Cp0BQ7Hignp3pgBioPlbPDm8fv2VXyzcRFr8ssJlKXgK0kjUJ5ExOdAD2mYEYlpSEzDwJQ6SLOZiG4RVwgFoSioqkCxSVSHjsMTxJ1WiTujgn45Hib1Hca+3QfTLy2HLp60xpAYIA+4E/iYFgjCb050BEJXwYFlhZ8F7NXYQSKGQUgP80fJKubnL+G3vLUUlIcpL3NQUerA57UT9jsxgy7MkA1DV5A6mKZAmiClRJoS63u1XtVfsdUIvIpzQkTfK6AoVp1woZooNh1hD2NzhnElRUhOCZOSFqJzhsb4Hn3Yv9twxnQagtvuQBUqmtromJBFwOtYXqS2G+m1G+hIhK6Jq4Ap7KbxGA9SgkSyvnwzq8o3sKxkHXnlxZT6dYq8Ycq8OpV+8PsFwZAgElIxIgoYNqSpIU1Ru0qRMBGKgVR0VM1Es5k4HCYuJyS5IDVZJSPZRnqSje7p6YzI7MuQtN70S+uFpii7qwvXh9nAV9RT37s9o6MSugqTgQOw0ubTEjmwKSUhI0JRoJTiUCkVYS+FoTIqw378kRARQyesG0iUGI3ExKap2FUNl+Ygye4my5FGmj2ZDGcandzpOBWrsn2CyFuFMqxaKT8CXyd05DaEjk7oKgzCWpg5Bziav04TRRP4FEutWASsbtXZtAD+KoSugh3oglVS4WwsI7Jrq84o8cgHtmK53N7HKqLQIfTjhuCvRuhYdANOxlJNcrCkeHvrTxHBWpbOw1Il3gNyW3VGrYi/OqFrQsMi9oFUk3s0tERtst2CAfxBNYl/wCJyO0mJb17sIXT9yACGAD2wCD4aGAiMwnIRtgRCwGKsBpR/YBF4C7CS6mofe1ADewi9e0jHCo5yR//vDvQBegGpWBU40rGkfSZWI4x4CGLVVNKB0uj7cmATsAFLBy4C/NH/S5vl03RA/D9g9yDc8Z1yLwAAAABJRU5ErkJggg=='

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	ESX.PlayerData = ESX.GetPlayerData()

	RefreshBussHUD()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
	RefreshBussHUD()
end)

function RefreshBussHUD()
	DisableSocietyMoneyHUDElement()

	if ESX.PlayerData.job.grade_name == 'boss' or ESX.PlayerData.job.grade_name == 'viceboss' or ESX.PlayerData.job.grade_name == 'chief' then
		EnableSocietyMoneyHUDElement()

		ESX.TriggerServerCallback('esx_society:getSocietyMoney', function(money)
			UpdateSocietyMoneyHUDElement(money)
		end, ESX.PlayerData.job.name)
	end
end

RegisterNetEvent('esx_addonaccount:setMoney')
AddEventHandler('esx_addonaccount:setMoney', function(society, money)
	if ESX.PlayerData.job and (ESX.PlayerData.job.grade_name == 'boss' or ESX.PlayerData.job.grade_name == 'viceboss'or ESX.PlayerData.job.grade_name == 'chief') and 'society_' .. ESX.PlayerData.job.name == society then
		UpdateSocietyMoneyHUDElement(money)
	end
end)

function EnableSocietyMoneyHUDElement()
	local societyMoneyHUDElementTpl = '<div><img src="' .. base64MoneyIcon .. '" style="width:20px; height:20px; vertical-align:middle;">&nbsp;{{money}}</div>'

	if ESX.GetConfig().EnableHud then
		ESX.UI.HUD.RegisterElement('society_money', 3, 0, societyMoneyHUDElementTpl, {
			money = 0
		})
	end

	TriggerEvent('esx_society:toggleSocietyHud', true)
end

function DisableSocietyMoneyHUDElement()
	if ESX.GetConfig().EnableHud then
		ESX.UI.HUD.RemoveElement('society_money')
	end

	TriggerEvent('esx_society:toggleSocietyHud', false)
end

function UpdateSocietyMoneyHUDElement(money)
	if ESX.GetConfig().EnableHud then
		ESX.UI.HUD.UpdateElement('society_money', {
			money = ESX.Math.GroupDigits(money)
		})
	end

	TriggerEvent('esx_society:setSocietyMoney', money)
end

function OpenBossMenu(society, close, options)
	local isBoss = nil
	local options  = options or {}
	local elements = {}

	ESX.TriggerServerCallback('esx_society:isBoss', function(result)
		isBoss = result
	end, society)

	while isBoss == nil do
		Citizen.Wait(100)
	end

	if not isBoss then
		return
	end

	local defaultOptions = {
		withdraw  = true,
		deposit   = true,
		wash      = false,
		employees = true,
		grades    = true
	}

	for k,v in pairs(defaultOptions) do
		if options[k] == nil then
			options[k] = v
		end
	end

	if options.withdraw then
		table.insert(elements, {label = _U('withdraw_society_money'), value = 'withdraw_society_money'})
	end

	if options.deposit then
		table.insert(elements, {label = _U('deposit_society_money'), value = 'deposit_money'})
	end

	if options.wash then
		table.insert(elements, {label = _U('wash_money'), value = 'wash_money'})
	end

	if options.employees then
		table.insert(elements, {label = _U('employee_management'), value = 'manage_employees'})
	end

	if options.grades then
		table.insert(elements, {label = _U('salary_management'), value = 'manage_grades'})
	end

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'boss_actions_' .. society,
	{
		title    = _U('boss_menu'),
		align    = 'top-left',
		elements = elements
	}, function(data, menu)

		if data.current.value == 'withdraw_society_money' then

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'withdraw_society_money_amount_' .. society, {
				title = _U('withdraw_amount')
			}, function(data, menu)

				local amount = tonumber(data.value)

				if amount == nil then
					ESX.ShowNotification(_U('invalid_amount'))
				else
					menu.close()
					TriggerServerEvent('esx_society:withdrawMoney', society, amount)
				end

			end, function(data, menu)
				menu.close()
			end)

		elseif data.current.value == 'deposit_money' then

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'deposit_money_amount_' .. society,
			{
				title = _U('deposit_amount')
			}, function(data, menu)

				local amount = tonumber(data.value)

				if amount == nil then
					ESX.ShowNotification(_U('invalid_amount'))
				else
					menu.close()
					TriggerServerEvent('esx_society:depositMoney', society, amount)
				end

			end, function(data, menu)
				menu.close()
			end)

		elseif data.current.value == 'wash_money' then

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'wash_money_amount_' .. society,
			{
				title = _U('wash_money_amount')
			}, function(data, menu)

				local amount = tonumber(data.value)

				if amount == nil then
					ESX.ShowNotification(_U('invalid_amount'))
				else
					menu.close()
					TriggerServerEvent('esx_society:washMoney', society, amount)
				end

			end, function(data, menu)
				menu.close()
			end)

		elseif data.current.value == 'manage_employees' then
			OpenManageEmployeesMenu(society)
		elseif data.current.value == 'manage_grades' then
			OpenManageGradesMenu(society)
		end

	end, function(data, menu)
		if close then
			close(data, menu)
		end
	end)

end

function OpenManageEmployeesMenu(society)

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'manage_employees_' .. society,
	{
		title    = _U('employee_management'),
		align    = 'top-left',
		elements = {
			{label = _U('employee_list'), value = 'employee_list'},
			{label = _U('recruit'),       value = 'recruit'}
		}
	}, function(data, menu)

		if data.current.value == 'employee_list' then
			OpenEmployeeList(society)
		end

		if data.current.value == 'recruit' then
			OpenRecruitMenu(society)
		end

	end, function(data, menu)
		menu.close()
	end)
end

function OpenEmployeeList(society)

	ESX.TriggerServerCallback('esx_society:getEmployees', function(employees)

		local elements = {
			head = {_U('employee'), _U('grade'), 'Dnes', 'Vcera', '7 dni (bez dneska)', _U('actions')},
			rows = {}
		}

		for i=1, #employees, 1 do

			local gradeLabel = (employees[i].job.grade_label == '' and employees[i].job.label or employees[i].job.grade_label)
			
			if employees[i].today == nil then employees[i].today = 0 end
			if employees[i].yeasterday == nil then employees[i].yeasterday = 0 end
			if employees[i].sevenday == nil then employees[i].sevenday = 0 end
			
			table.insert(elements.rows, {
				data = employees[i],
				cols = {
					employees[i].name,
					gradeLabel,
					employees[i].today,
					employees[i].yeasterday,
					employees[i].sevenday,
					'{{' .. _U('promote') .. '|promote}} {{' .. _U('fire') .. '|fire}}'
				}
			})
		end

		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open('list', GetCurrentResourceName(), 'employee_list_' .. society, elements, function(data, menu)
			local employee = data.data

			if data.value == 'promote' then
				menu.close()
				OpenPromoteMenu(society, employee)
			end

			if data.value == 'fire' then
				menu.close()
				OpenFirePrompt(society, employee)
			end

		end, function(data, menu)
			menu.close()
			OpenManageEmployeesMenu(society)
		end)

	end, society)

end

function OpenRecruitMenu(society)

	ESX.TriggerServerCallback('esx_society:getOnlinePlayers', function(players)

		local elements = {}

		for i=1, #players, 1 do
			if players[i].job.name ~= society then
				table.insert(elements, {
					label = players[i].name,
					value = players[i].source,
					name = players[i].name,
					identifier = players[i].identifier
				})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'recruit_' .. society,
		{
			title    = _U('recruiting'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'recruit_confirm_' .. society,
			{
				title    = _U('do_you_want_to_recruit', data.current.name),
				align    = 'top-left',
				elements = {
					{label = _U('no'),  value = 'no'},
					{label = _U('yes'), value = 'yes'}
				}
			}, function(data2, menu2)

				menu2.close()

				if data2.current.value == 'yes' then

					TriggerEvent('esx:showNotification', _U('you_have_hired', data.current.name))

					ESX.TriggerServerCallback('esx_society:setJob', function()
						OpenRecruitMenu(society)
					end, data.current.identifier, society, 0, 'hire')

				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)

	end)

end

function OpenPromoteMenu(society, employee)

	ESX.TriggerServerCallback('esx_society:getJob', function(job)

		local elements = {}

		for i=1, #job.grades, 1 do
			local gradeLabel = (job.grades[i].label == '' and job.label or job.grades[i].label)

			table.insert(elements, {
				label = gradeLabel,
				value = job.grades[i].grade,
				selected = (employee.job.grade == job.grades[i].grade)
			})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'promote_employee_' .. society,
		{
			title    = _U('promote_employee', employee.name),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			menu.close()

			TriggerEvent('esx:showNotification', _U('you_have_promoted', employee.name, data.current.label))

			ESX.TriggerServerCallback('esx_society:setJob', function()
				OpenEmployeeList(society)
			end, employee.identifier, society, data.current.value, 'promote')

		end, function(data, menu)
			menu.close()
			OpenEmployeeList(society)
		end)

	end, society)

end

function OpenFirePrompt(society, employee)
	local elements = {}

	table.insert(elements, {
		label = 'Ano',
		value = 'yes',
		selected = false
	})
	
	table.insert(elements, {
		label = 'Ne',
		value = 'no',
		selected = true
	})

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'fire_employee_' .. society,
	{
		title    = ('Opravdu vyhodit zamestnance: ' .. employee.name),
		align    = 'top-left',
		elements = elements
	}, function(data9, menu9)
		if data9.current.value == 'no' then
			menu9.close()
			OpenEmployeeList(society)
		elseif data9.current.value == 'yes' then
			TriggerEvent('esx:showNotification', _U('you_have_fired', employee.name))

			menu9.close()
			ESX.TriggerServerCallback('esx_society:setJob', function()
				OpenEmployeeList(society)
			end, employee.identifier, 'unemployed', 0, 'fire')
		end
	end, function(data9, menu9)
		menu9.close()
		OpenEmployeeList(society)
	end)
end

function OpenManageGradesMenu(society)

	ESX.TriggerServerCallback('esx_society:getJob', function(job)

		local elements = {}

		for i=1, #job.grades, 1 do
			local gradeLabel = (job.grades[i].label == '' and job.label or job.grades[i].label)

			table.insert(elements, {
				label = ('%s - <span style="color:green;">%s</span>'):format(gradeLabel, _U('money_generic', ESX.Math.GroupDigits(job.grades[i].salary))),
				value = job.grades[i].grade
			})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'manage_grades_' .. society,
		{
			title    = _U('salary_management'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)

			ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'manage_grades_amount_' .. society, {
				title = _U('salary_amount')
			}, function(data2, menu2)

				local amount = tonumber(data2.value)

				if amount == nil then
					ESX.ShowNotification(_U('invalid_amount'))
				elseif amount > Config.MaxSalary then
					ESX.ShowNotification(_U('invalid_amount_max'))
				else
					menu2.close()

					ESX.TriggerServerCallback('esx_society:setJobSalary', function()
						OpenManageGradesMenu(society)
					end, society, data.current.value, amount)
				end

			end, function(data2, menu2)
				menu2.close()
			end)

		end, function(data, menu)
			menu.close()
		end)

	end, society)

end

AddEventHandler('esx_society:openBossMenu', function(society, close, options)
	OpenBossMenu(society, close, options)
end)
