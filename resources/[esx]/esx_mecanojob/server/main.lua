ESX                = nil
PlayersHarvesting  = {}
PlayersHarvesting2 = {}
PlayersHarvesting3 = {}
PlayersHarvesting4 = {}
PlayersHarvesting5 = {}
PlayersCrafting    = {}
PlayersCrafting2   = {}
PlayersCrafting3   = {}
PlayersCrafting4   = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'mechanic', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'mechanic', _U('mechanic_customer'), true, true)
TriggerEvent('esx_society:registerSociety', 'mechanic', 'mechanic', 'society_mechanic', 'society_mechanic', 'society_mechanic', {type = 'private'})

local function Harvest(source)
	SetTimeout(4000, function()

		if PlayersHarvesting[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local item = xPlayer.getInventoryItem('gazbottle')

			if item.count >= item.limit then
				TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
			else
				xPlayer.addInventoryItem('gazbottle', 1)
				Harvest(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startHarvest')
AddEventHandler('esx_mechanicjob:startHarvest', function()
	local _source = source
	PlayersHarvesting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('recovery_gas_can'))
	Harvest(source)
end)

RegisterServerEvent('esx_mechanicjob:stopHarvest')
AddEventHandler('esx_mechanicjob:stopHarvest', function()
	local _source = source
	PlayersHarvesting[_source] = false
end)

local function Harvest2(source)
	SetTimeout(4000, function()

		if PlayersHarvesting2[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local item = xPlayer.getInventoryItem('fixtool')

			if item.count >= item.limit then
				TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
			else
				xPlayer.addInventoryItem('fixtool', 1)
				Harvest2(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startHarvest2')
AddEventHandler('esx_mechanicjob:startHarvest2', function()
	local _source = source
	PlayersHarvesting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('recovery_repair_tools'))
	Harvest2(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopHarvest2')
AddEventHandler('esx_mechanicjob:stopHarvest2', function()
	local _source = source
	PlayersHarvesting2[_source] = false
end)

local function Harvest3(source)
	SetTimeout(4000, function()

		if PlayersHarvesting3[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local item = xPlayer.getInventoryItem('carotool')
			if item.count >= item.limit then
				TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
			else
				xPlayer.addInventoryItem('carotool', 1)
				Harvest3(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startHarvest3')
AddEventHandler('esx_mechanicjob:startHarvest3', function()
	local _source = source
	PlayersHarvesting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('recovery_body_tools'))
	Harvest3(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopHarvest3')
AddEventHandler('esx_mechanicjob:stopHarvest3', function()
	local _source = source
	PlayersHarvesting3[_source] = false
end)

local function Harvest4(source)
	SetTimeout(4000, function()

		if PlayersHarvesting4[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local item = xPlayer.getInventoryItem('engtool')
			if item.count >= item.limit then
				TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
			else
				xPlayer.addInventoryItem('engtool', 1)
				Harvest4(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startHarvest4')
AddEventHandler('esx_mechanicjob:startHarvest4', function()
	local _source = source
	PlayersHarvesting4[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('recovery_eng_tools'))
	Harvest4(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopHarvest4')
AddEventHandler('esx_mechanicjob:stopHarvest4', function()
	local _source = source
	PlayersHarvesting4[_source] = false
end)

local function Harvest5(source)
	SetTimeout(4000, function()

		if PlayersHarvesting5[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local item = xPlayer.getInventoryItem('fuelcanister')
			if item.count >= item.limit then
				TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
			else
				xPlayer.addInventoryItem('fuelcanister', 1)
				Harvest5(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startHarvest5')
AddEventHandler('esx_mechanicjob:startHarvest5', function()
	local _source = source
	PlayersHarvesting5[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('recovery_fuelcanister'))
	Harvest5(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopHarvest5')
AddEventHandler('esx_mechanicjob:stopHarvest5', function()
	local _source = source
	PlayersHarvesting5[_source] = false
end)

local function Craft(source)
	SetTimeout(4000, function()

		if PlayersCrafting[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local items = xPlayer.getInventoryItem('gazbottle')
			local itemt = xPlayer.getInventoryItem('blowpipe')

			if items.count <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_gas_can'))
			elseif itemt.count >= itemt.limit then
				TriggerClientEvent('esx:showNotification', source, _U('max_crafted'))
			else
				xPlayer.removeInventoryItem('gazbottle', 1)
				xPlayer.addInventoryItem('blowpipe', 1)
				Craft(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startCraft')
AddEventHandler('esx_mechanicjob:startCraft', function()
	local _source = source
	PlayersCrafting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('assembling_blowtorch'))
	Craft(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopCraft')
AddEventHandler('esx_mechanicjob:stopCraft', function()
	local _source = source
	PlayersCrafting[_source] = false
end)

local function Craft2(source)
	SetTimeout(4000, function()

		if PlayersCrafting2[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local items = xPlayer.getInventoryItem('fixtool')
			local itemt = xPlayer.getInventoryItem('fixkit')

			if items.count <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_repair_tools'))
			elseif itemt.count >= itemt.limit then
				TriggerClientEvent('esx:showNotification', source, _U('max_crafted'))
			else
				xPlayer.removeInventoryItem('fixtool', 1)
				xPlayer.addInventoryItem('fixkit', 1)
				Craft2(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startCraft2')
AddEventHandler('esx_mechanicjob:startCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('assembling_repair_kit'))
	Craft2(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopCraft2')
AddEventHandler('esx_mechanicjob:stopCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = false
end)

local function Craft3(source)
	SetTimeout(4000, function()

		if PlayersCrafting3[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local items = xPlayer.getInventoryItem('carotool')
			local itemt = xPlayer.getInventoryItem('carokit')

			if items.count <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_body_tools'))
			elseif itemt.count >= itemt.limit then
				TriggerClientEvent('esx:showNotification', source, _U('max_crafted'))
			else
				xPlayer.removeInventoryItem('carotool', 1)
				xPlayer.addInventoryItem('carokit', 1)
				Craft3(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startCraft3')
AddEventHandler('esx_mechanicjob:startCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('assembling_body_kit'))
	Craft3(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopCraft3')
AddEventHandler('esx_mechanicjob:stopCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = false
end)

local function Craft4(source)
	SetTimeout(4000, function()

		if PlayersCrafting4[source] == true then
			local xPlayer = ESX.GetPlayerFromId(source)
			local items = xPlayer.getInventoryItem('engtool')
			local itemt = xPlayer.getInventoryItem('engkit')
			
			if items.count <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_eng_tools'))
			elseif itemt.count >= itemt.limit then
				TriggerClientEvent('esx:showNotification', source, _U('max_crafted'))
			else
				xPlayer.removeInventoryItem('engtool', 1)
				xPlayer.addInventoryItem('engkit', 1)
				Craft4(source)
			end
		end

	end)
end

RegisterServerEvent('esx_mechanicjob:startCraft4')
AddEventHandler('esx_mechanicjob:startCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = true
	TriggerClientEvent('esx:showNotification', _source, _U('assembling_eng_kit'))
	Craft4(_source)
end)

RegisterServerEvent('esx_mechanicjob:stopCraft4')
AddEventHandler('esx_mechanicjob:stopCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = false
end)

RegisterServerEvent('esx_mechanicjob:onNPCJobMissionCompleted')
AddEventHandler('esx_mechanicjob:onNPCJobMissionCompleted', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local total   = math.random(Config.NPCJobEarnings.min, Config.NPCJobEarnings.max);

	--TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mechanic', function(account)
	--	account.addMoney(total)
	--end)
	xPlayer.addMoney(total)

	--TriggerClientEvent("esx:showNotification", _source, _U('your_comp_earned').. total)
	TriggerClientEvent("esx:showNotification", _source, 'Vydelal sis €'.. total)
end)

ESX.RegisterUsableItem('blowpipe', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('blowpipe', 1)

	TriggerClientEvent('esx_mechanicjob:onHijack', _source)
	TriggerClientEvent('esx:showNotification', _source, _U('you_used_blowtorch'))
end)

ESX.RegisterUsableItem('fixkit', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('fixkit', 1)

	TriggerClientEvent('esx_mechanicjob:onFixkit', _source)
	TriggerClientEvent('esx:showNotification', _source, _U('you_used_repair_kit'))
end)

ESX.RegisterUsableItem('carokit', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('carokit', 1)

	TriggerClientEvent('esx_mechanicjob:onCarokit', _source)
	TriggerClientEvent('esx:showNotification', _source, _U('you_used_body_kit'))
end)

ESX.RegisterUsableItem('engkit', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('engkit', 1)

	TriggerClientEvent('esx_mechanicjob:onEngkit', _source)
	TriggerClientEvent('esx:showNotification', _source, _U('you_used_eng_kit'))
end)

ESX.RegisterUsableItem('fuelcanister', function(source)
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('fuelcanister', 1)

	TriggerClientEvent('esx_mechanicjob:onFuelCanister', _source)
	TriggerClientEvent('esx:showNotification', _source, _U('you_used_fuelcanister'))
end)

RegisterServerEvent('esx_mechanicjob:getStockItem')
AddEventHandler('esx_mechanicjob:getStockItem', function(itemName, count)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mechanic', function(inventory)
		local item = inventory.getItem(itemName)
		local sourceItem = xPlayer.getInventoryItem(itemName)

		-- is there enough in the society?
		if count > 0 and item.count >= count then

			-- can the player carry the said amount of x item?
			if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('player_cannot_hold'))
			else
				inventory.removeItem(itemName, count)
				xPlayer.addInventoryItem(itemName, count)
				TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn', count, item.label))
			end
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
		end
	end)
end)

ESX.RegisterServerCallback('esx_mechanicjob:getStockItems', function(source, cb)
	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mechanic', function(inventory)
		cb(inventory.items)
	end)
end)

RegisterServerEvent('esx_mechanicjob:putStockItems')
AddEventHandler('esx_mechanicjob:putStockItems', function(itemName, count)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mechanic', function(inventory)
		local item = inventory.getItem(itemName)
		local playerItemCount = xPlayer.getInventoryItem(itemName).count

		if item.count >= 0 and count <= playerItemCount then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_deposited', count, item.label))
	end)
end)

ESX.RegisterServerCallback('esx_mechanicjob:getPlayerInventory', function(source, cb)
	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({items = items})
end)
