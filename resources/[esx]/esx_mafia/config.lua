Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale                     = 'en'

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_COMBATPISTOL',     price = 4000 },
      { name = 'WEAPON_ASSAULTSMG',       price = 15000 },
      { name = 'WEAPON_ASSAULTRIFLE',     price = 25000 },
      { name = 'WEAPON_PUMPSHOTGUN',      price = 10000 },
--      { name = 'WEAPON_STUNGUN',          price = 250 },
--      { name = 'WEAPON_FLASHLIGHT',       price = 50 },
--      { name = 'WEAPON_FIREEXTINGUISHER', price = 50 },
--      { name = 'WEAPON_FLAREGUN',         price = 3000 },
--      { name = 'GADGET_PARACHUTE',        price = 2000 },
      { name = 'WEAPON_SNIPERRIFLE',      price = 50000 },
--      { name = 'WEAPON_FIREWORK',         price = 5000 },
--      { name = 'WEAPON_BZGAS',            price = 8000 },
--      { name = 'WEAPON_SMOKEGRENADE',     price = 8000 },
      { name = 'WEAPON_APPISTOL',         price = 19000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 25000 },
      { name = 'WEAPON_HEAVYSNIPER',      price = 100000 },
--      { name = 'WEAPON_FLARE',            price = 8000 },
--      { name = 'WEAPON_SWITCHBLADE',      price = 500 },
	  { name = 'WEAPON_REVOLVER',         price = 6000 },
--	  { name = 'WEAPON_POOLCUE',          price = 100 },
	  { name = 'WEAPON_GUSENBERG',        price = 18000 },
	  { name = 'WEAPON_SAWNOFFSHOTGUN',   price = 45000 },
	  
    },

	  AuthorizedVehicles = {
		  { name = 'schafter3',  label = 'Civilni auto' },
		  { name = 'btype',      label = 'Roosevelt' },
		  { name = 'sandking',   label = '4X4' },
		  { name = 'mule3',      label = 'Dodavka' },
		  { name = 'guardian',   label = 'Grand 4x4' },
		  { name = 'burrito3',   label = 'Burrito' },
		  { name = 'mesa',       label = 'Mesa' },
	  },

    Cloakrooms = {
      { x = -2674.95, y = 1307.47, z = 151.11 },
    },

    Armories = {
      { x = -2679.39, y = 1332.89, z = 140.01 },
    },

    Vehicles = {
      {
        Spawner    = { x = -2677.32, y = 1307.51, z = 146.26 },
        SpawnPoint = { x = -2669.36, y = 1309.57, z = 145.92 },
        Heading    = 269.7,
      }
    },
	
	Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -2668.57, y = 1305.07, z = 146.22 }
    },

    BossActions = {
      { x = -2679.27, y = 1337.92, z = 150.92 }
    },

  },

}
