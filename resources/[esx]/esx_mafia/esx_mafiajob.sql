INSERT INTO `addon_account` (name, label, shared) VALUES 
	('society_mafia','mafia',1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
	('society_mafia','mafia',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES 
	('society_mafia', 'mafia', 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('mafia', 'Mafia', 1);


INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
('mafia', 0, 'soldier', 'Soldier', 200, '{}', '{}'),
('mafia', 1, 'capo', 'Caporegime', 200, '{}', '{}'),
('mafia', 2, 'underboss', 'Underboss', 200, '{}', '{}'),
('mafia', 3, 'viceboss', 'Consigliere', 200, '{}', '{}'),
('mafia', 4, 'boss', 'Boss', 200, '{}', '{}');

CREATE TABLE `fine_types_mafia` (
  
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  
  PRIMARY KEY (`id`)
);

INSERT INTO `fine_types_mafia` (label, amount, category) VALUES 
	('Dluh organizaci',3000,0),
	('Dluh organizaci',5000,0),
	('Dluh organizaci',10000,1),
	('Dluh organizaci',20000,1),
	('Dluh organizaci',50000,2),
	('Dluh organizaci',150000,3),
	('Dluh organizaci',350000,3)
;