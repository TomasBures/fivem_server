Locales['cs'] = {
  ['tattoo_shop_prompt'] = 'stiskni ~INPUT_PICKUP~ pro otevreni ~y~tetovaciho salonu~s~.',
  ['money_amount']       = '<span style="color:green;">€%s</span>',
  ['part']               = 'cast %s',
  ['go_back_to_menu']    = '< Zpet',
  ['tattoo_item']        = 'tetovani %s - %s',
  ['tattoos']            = 'tetovani',
  ['tattoo_shop']        = 'tetovaci salon',
  ['bought_tattoo']      = 'koupil jsi ~y~tetovani~s~ za ~r~€%s~s~',
  ['not_enough_money']   = '~r~Nemas dostatek penez~s~ na toto tetovani! Chybi ti ~r~€%s~s~'
}