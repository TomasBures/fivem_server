--===============================================--===============================================
--= stationary radars based on  https://github.com/DreanorGTA5Mods/StationaryRadar           =
--===============================================--===============================================



ESX              = nil
local PlayerData = {}

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)

local radares = {
    {x = 190.57, y = -824.62, z = 30.88},
    {x = -253.10794067383, y = -630.20385742188, z = 33.002685546875},
	{x = -37.6, y = -1584.29, z = 29.3},
	{x = -52.98, y = -159.91, z = 29.34},
	{x = -171.16, y = -873.71, z = 29.35},
	{x = -181.49, y = -870.77, z = 29.35},
	{x = -234.1, y = -661.61, z = 33.37},
	{x = -236.4, y = -678.55, z = 33.34},
	{x = 221.67, y = -1065.32, z = 29.18},
	{x = 206.93, y = -1058.41, z = 339.12},
	{x = 370.16, y = -649.96, z = 29.31},
	{x = 360.26, y = -645.86, z = 29.31},
	{x = -632.29, y = -678.92, z = 31.52},
	{x = -876.99, y = -591.68, z = 29.94},
	{x = 1097.61, y = -779.55, z = 19.35},
	{x = -1061.45, y = -771.96, z = 19.34},
	{x = 90.91, y = -1018.05, z = 29.41},
	{x = 206.66, y = -1058.42, z = 29.18},
	{x = 54.37, y = -1215.17, z = 357.99},
	{x = 70.4, y = -1217.5, z = 29.34},
	{x = -36.61, y = -1611.31, z = 29.35},
	{x = -269.52, y = -1410.22, z = 31.31},
	{x = -283.31, y = -1435.77, z = 31.36},
	{x = -95.58, y = -1104.01, z = 55.88},
	{x = 108.29, y = -1366.29, z = 29.34},
	{x = 163.91, y = -1394.48, z = 29.3},
	
}

Citizen.CreateThread(function()
    while true do
        Wait(0)
        for k,v in pairs(radares) do
            local player = GetPlayerPed(-1)
            local coords = GetEntityCoords(player, true)
            if Vdist2(radares[k].x, radares[k].y, radares[k].z, coords["x"], coords["y"], coords["z"]) < 20 then
                if PlayerData.job ~= nil and not (PlayerData.job.name == 'police' or PlayerData.job.name == 'ambulance') then
                    checkSpeed()
                end
            end
        end
    end
end)

function checkSpeed()
    local pP = GetPlayerPed(-1)
    local speed = GetEntitySpeed(pP)
    local vehicle = GetVehiclePedIsIn(pP, false)
    local driver = GetPedInVehicleSeat(vehicle, -1)
    local plate = GetVehicleNumberPlateText(vehicle)
    local maxspeed = 120
    --local mphspeed = math.ceil(speed*2.236936)
	local kphspeed = math.ceil(speed * 3.6)
	local fineamount = nil
	local finelevel = nil
	local truespeed = kphspeed
    if kphspeed > maxspeed and driver == pP then
        Citizen.Wait(250)
        TriggerServerEvent('fineAmount', kphspeed)
	if truespeed > 120 and truespeed <= 200 then
	fineamount = Config.Fine
	finelevel = 'Prekroceni rychlosti #1 '
	end
	if truespeed > 200 and truespeed <= 250 then
	fineamount = Config.Fine2
	finelevel = 'Prekroceni rychlosti #2'
	end
	if truespeed > 250 and truespeed <= 300 then
	fineamount = Config.Fine3
	finelevel = 'Prekroceni rychlosti #3'
	end
	if truespeed > 300 and truespeed <= 800 then
	fineamount = Config.Fine4
	finelevel = 'Jedes jak blazen uklidni se'
	end
        exports.pNotify:SetQueueMax("left", 1)
        exports.pNotify:SendNotification({
            text = "<h2><center>Speed Camera</center></h2>" .. "</br>Byla Vam udelena pokuta za rychlou jizdu!</br>SPZ vozidla: " .. plate .. "</br>Castka: $" .. fineamount .. "</br>Prestupek: " .. finelevel .. "</br>Rychlostni limit: " .. maxspeed .. "</br>Vase rychlost: " ..kphspeed,
            type = "error",
            timeout = 9500,
            layout = "centerLeft",
            queue = "left"
        })
    end
end

