Locales['en'] = {
  ['shop'] = 'Obchod/Restaurace',
  ['shops'] = 'Obchody/Restaurace',
  ['press_menu'] = 'Stisknete ~INPUT_CONTEXT~ pro prístup k ~y~obchodu~s~.',
  ['shop_item'] = '€%s',
  ['bought'] = 'Práve jste si koupili ~y~%sx~s~ ~b~%s~s~ za ~r~€%s~s~',
  ['not_enough'] = 'Nemáte ~r~dostatek~s~ penez, Chybí ~y~vám~s~ ~r~€%s~s~!',
  ['player_cannot_hold'] = 'Nemáte ~r~dost~s~ volného ~y~místa~s~ v invetári!',
  ['shop_confirm'] = 'Koupit %sx %s za €%s?',
  ['no'] = 'Ne',
  ['yes'] = 'Ano',
}

