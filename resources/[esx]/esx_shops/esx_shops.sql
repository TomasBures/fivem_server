USE `essentialmode`;

CREATE TABLE `shops` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`store` varchar(100) NOT NULL,
	`item` varchar(100) NOT NULL,
	`price` int(11) NOT NULL,

	PRIMARY KEY (`id`)
);

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'CoopJednota', 'bread', 3),
(2, 'CoopJednota', 'cokolada', 4),
(3, 'CoopJednota', 'sandwich', 5),
(4, 'CoopJednota', 'water', 1),
(5, 'CoopJednota', 'mineral', 2),
(6, 'Mcdonald', 'hamburger', 5),
(7, 'Mcdonald', 'cheesburger', 7),
(8, 'Mcdonald', 'bigmac', 10),
(9, 'Mcdonald', 'mcchicken', 10),
(10, 'Mcdonald', 'cocacola', 3),
(11, 'Mcdonald', 'coffe', 5),
(12, 'Mcdonald', 'fanta', 3),
(13, 'Mcdonald', 'milkshake', 5)
;
