Locales['en'] = {
	['invalid_amount'] = '~r~To je neplatná suma peňazí~s~',
	['deposit_money']  = 'vložil si ~g~€%s~s~',
	['withdraw_money'] = 'vybral si ~g~€%s~s~',
	['press_e_atm']    = 'stlac ~INPUT_PICKUP~ na vklad alebo výber z ~g~Prima Banky~s~',
	['atm_blip']       = 'Prima Banka',
}
