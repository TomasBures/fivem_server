Locales ['en'] = {
	['judge']              	= 'SOUDCE',
	['escape_attempt']     	= 'Nemuzete utect. Vase komunitni sluzba byla rozsirena.',
	['remaining_msg']      	= 'Nez dokoncite prospesne prace, musite dokoncit ~b~%s~s~ sluzeb.',
	['comserv_msg']       	= '%s byl odeslan k %s sluzeb prospesnich praci.',
	['comserv_finished']  	= '%s dokoncil prospesnich praci! Uzivej svobody.',
	['press_to_start']      = 'Zmackni ~INPUT_CONTEXT~ pro start.',
	['system_msn']          = '^1SYSTEM',
	['invalid_player_id_or_actions']      = 'Invalid player ID or actions count.',
	['insufficient_permissions']      = 'Insufficient Permissions.',
	['give_player_community']      = 'Dat hrace k prospesnim pracim',
	['target_id']      = 'target id',
	['action_count_suggested']      = 'actions count [suggested: 10-40]',
	['invalid_player_id']      = 'Invalid player ID!',
	['unjail_people']      = 'Unjail people from jail'
}
