Config                            = {}
Config.DrawDistance               = 100.0
--language currently available EN and SV
Config.Locale                     = 'cs'

Config.Zones = {

  PoliceDuty = {
    Pos   = { x = 439.825, y = -975.693, z = 29.691 },
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

  AmbulanceDuty = {
    Pos = { x = 308.83, y = -597.5, z = 42.32 },
    Size = { x = 2.0, y = 2.0, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },
}