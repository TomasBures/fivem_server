INSERT INTO `jobs` (name, label) VALUES
  ('offpolice','Mimo sluzbu'),
  ('offambulance','Mimo sluzbu')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('offpolice',0,'recruit','Rekrut',12,'{}','{}'),
  ('offpolice',1,'officer','Dustojnik',24,'{}','{}'),
  ('offpolice',2,'sergeant','Serzant',36,'{}','{}'),
  ('offpolice',3,'lieutenant','Porucik',48,'{}','{}'),
  ('offpolice',4,'boss','Kapitan',0,'{}','{}'),
  ('offambulance',0,'ambulance','Ambulance',12,'{}','{}'),
  ('offambulance',1,'doctor','Doctor',24,'{}','{}'),
  ('offambulance',2,'chief_doctor','Chef',36,'{}','{}'),
  ('offambulance',3,'boss','Boss',48,'{}','{}')
;