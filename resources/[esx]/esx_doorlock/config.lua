Config = {}
Config.Locale = 'cs'

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- Entrance Doors
	
	{
		objName = 'v_ilev_ph_door01',
		objCoords  = {x = 434.747, y = -980.618, z = 30.839},
		textCoords = {x = 434.747, y = -981.50, z = 31.50},
		authorizedJobs = { 'police', 'offpolice' },
		locked = false,
		distance = 2.5,
	},
	{
		objName = 'prop_fnclink_03gate2',
		objCoords  = vector3(-54.24, -1071.98, 27.24),
		textCoords = vector3(-54.85, -1074.53, 27.13),
		authorizedJobs = { 'cardealer' },
		locked = true,
		distance = 35,
		size = 2
	},

	{
		objName = 'v_ilev_ph_door002',
		objCoords  = {x = 434.747, y = -983.215, z = 30.839},
		textCoords = {x = 434.747, y = -982.50, z = 31.50},
		authorizedJobs = { 'police', 'offpolice' },
		locked = false,
		distance = 2.5
	},

	-- To locker room & roof
	{
		objName = 'v_ilev_ph_gendoor004',
		objCoords  = {x = 449.698, y = -986.469, z = 30.689},
		textCoords = {x = 450.104, y = -986.388, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Rooftop
	{
		objName = 'v_ilev_gtdoor02',
		objCoords  = {x = 464.361, y = -984.678, z = 43.834},
		textCoords = {x = 464.361, y = -984.050, z = 44.834},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Hallway to roof
	{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 461.286, y = -985.320, z = 30.839},
		textCoords = {x = 461.50, y = -986.00, z = 31.50},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Armory
	{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 452.618, y = -982.702, z = 30.689},
		textCoords = {x = 453.079, y = -982.600, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Captain Office
	{
		objHash = -1320876379,
		objCoords  = {x = 447.238, y = -980.630, z = 30.689},
		textCoords = {x = 447.200, y = -980.010, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- To downstairs (double doors)
	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 443.97, y = -989.033, z = 30.6896},
		textCoords = {x = 444.020, y = -989.445, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 445.37, y = -988.705, z = 30.6896},
		textCoords = {x = 445.350, y = -989.445, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	-- 
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 463.815, y = -992.686, z = 24.9149},
		textCoords = {x = 463.30, y = -992.686, z = 25.10},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.381, y = -993.651, z = 24.914},
		textCoords = {x = 461.806, y = -993.308, z = 25.064},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.331, y = -998.152, z = 24.914},
		textCoords = {x = 461.806, y = -998.800, z = 25.064},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 3
-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objYaw = 90.0,
		objCoords  = vector3(462.7, -1001.9, 24.9),
		textCoords = vector3(461.8, -1002.4, 25.0),
		authorizedJobs = { 'police' },
		locked = true
	},
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(467.92, -996.65, 24.91),
		textCoords = vector3(467.91, -996.64, 24.90),
		authorizedJobs = { 'police' },
		locked = true
	},
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(472.3, -996.99, 24.91),
		textCoords = vector3(472.3, -996.99, 24.91),
		authorizedJobs = { 'police' },
		locked = true
	},
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(476.31, -996.98, 24.91),
		textCoords = vector3(476.31, -996.98, 24.91),
		authorizedJobs = { 'police' },
		locked = true
	},
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = vector3(480.82, -996.51, 24.91),
		textCoords = vector3(480.82, -996.51, 24.91),
		authorizedJobs = { 'police' },
		locked = true
	},
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = vector3(442.06, -985.85, 26.67),
		textCoords = vector3(442.06, -985.85, 26.99),
		authorizedJobs = { 'police' },
		distance = 2,
		locked = true
	},
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = vector3(443.33, -988.24, 26.67),
		textCoords = vector3(443.33, -988.24, 26.99),
		authorizedJobs = { 'police' },
		distance = 2,
		locked = true
	},
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = vector3(471.24, -985.32, 24.91),
		textCoords = vector3(471.21, -985.36, 25.20),
		authorizedJobs = { 'police' },
		distance = 2.5,
		locked = true
	},
	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = vector3(468.49, -978.05, 24.91),
		textCoords = vector3(468.39, -978.05, 25.20),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},
	{
		objHash = -1033001619,
		objCoords  = vector3(467.98, -1003.51, 24.91),
		textCoords = vector3(467.98, -1003.0, 24.91),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},
	{
		objHash = -1033001619,
		objCoords  = vector3(472.04, -1003.0, 24.91),
		textCoords = vector3(472.04, -1003.0, 24.91),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},
	{
		objHash = -1033001619,
		objCoords  = vector3(476.74, -1003.0, 24.91),
		textCoords = vector3(476.74, -1003.0, 24.91),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},
	{
		objHash = -1033001619,
		objCoords  = vector3(480.96, -1003.01, 24.91),
		textCoords = vector3(480.96, -1003.01, 24.91),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},

	{
		objName = 'v_ilev_ph_gendoor006',
		objCoords  = vector3(463.53, -981.46, 24.91),
		textCoords = vector3(463.53, -981.46, 25.20),
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},
			-- To downstairs (double doors) 
			{
		objHash = -1033001619,
		objCoords  = {x = 446.52, y = -999.06, z = 30.72},
		textCoords = {x = 446.52, y = -999.06, z = 30.72},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},
	{
		objHash = -1033001619,
		objCoords  = {x = 445.33, y = -999.31, z = 30.72},
		textCoords = {x = 445.33, y = -999.31, z = 30.72},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},
	




	-- To Back
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 463.478, y = -1003.538, z = 25.005},
		textCoords = {x = 464.00, y = -1003.50, z = 25.50},
		authorizedJobs = { 'police' },
		locked = true
	},

	--
	-- Mission Row Back
	--

	-- Back (double doors)
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 467.371, y = -1014.452, z = 26.536},
		textCoords = {x = 468.09, y = -1014.452, z = 27.1362},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 469.967, y = -1014.452, z = 26.536},
		textCoords = {x = 469.35, y = -1014.452, z = 27.136},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	-- Back Gate
	{
		objName = 'hei_prop_station_gate',
		objCoords  = {x = 488.894, y = -1017.210, z = 27.146},
		textCoords = {x = 488.894, y = -1020.210, z = 30.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 14,
		size = 2
	},

	--
	-- Sandy Shores
	--

	-- Entrance
	{
		objName = 'v_ilev_shrfdoor',
		objCoords  = {x = 1855.105, y = 3683.516, z = 34.266},
		textCoords = {x = 1855.105, y = 3683.516, z = 35.00},
		authorizedJobs = { 'police' },
		locked = false
	},

	--
	-- Paleto Bay
	--

	-- Entrance (double doors)
	{
		objName = 'v_ilev_shrf2door',
		objCoords  = {x = -443.14, y = 6015.685, z = 31.716},
		textCoords = {x = -443.14, y = 6015.685, z = 32.00},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 2.5
	},

	{
		objName = 'v_ilev_shrf2door',
		objCoords  = {x = -443.951, y = 6016.622, z = 31.716},
		textCoords = {x = -443.951, y = 6016.622, z = 32.00},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 2.5
	},

	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1844.998, y = 2604.810, z = 44.638},
		textCoords = {x = 1844.998, y = 2608.50, z = 48.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},

	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1818.542, y = 2604.812, z = 44.611},
		textCoords = {x = 1818.542, y = 2608.40, z = 48.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},

	--
	-- Addons
	--

	
	--
	-- Unicorn
	--
	
	-- Main unicorn doors
	{
		objName = 'prop_strip_door_01',
		objCoords  = {x = 127.95, y = -1298.50, z = 29.41},
		textCoords = {x = 128.61, y = -1298.19, z = 29.53},
		authorizedJobs = { 'unicorn' },
		locked = false,
		distance = 3
	},
	
	-- Unicorn back doors
	{
		objName = 'prop_magenta_door',
		objCoords  = {x = 96.091, y = -1284.854, z = 29.438},
		textCoords = {x = 95.53, y = -1285.24, z = 29.73},
		authorizedJobs = { 'unicorn' },
		locked = true,
		distance = 3
	},
	
	-- Unicorn office doors
	{
		objName = 'v_ilev_roc_door2',
		objCoords  = {x = 99.08, y = -1293.70, z = 29.41},
		textCoords = {x = 99.72, y = -1293.21, z = 29.71},
		authorizedJobs = { 'unicorn' },
		locked = true,
		distance = 3
	},
	
	-- Unicorn dressingroom doors
	{
		objName = 'v_ilev_door_orangesolid',
		objCoords  = {x = 113.98, y = -1297.43, z = 29.41},
		textCoords = {x = 113.7, y = -1296.7, z = 29.71},
		authorizedJobs = { 'unicorn' },
		locked = true,
		distance = 3
	},
	
	--
	-- MAPADDONS
	--
	
	-- Zancudo Military Base Front Entrance
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -1587.23, y = 2805.08, z = 15.82},
		textCoords = {x = -1587.23, y = 2805.08, z = 19.82},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -1600.29, y = 2793.74, z = 15.74},
		textCoords = {x = -1600.29, y = 2793.74, z = 19.74},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	-- Zancudo Military Base Back Entrance
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -2296.17, y = 3393.1, z = 30.07},
		textCoords = {x = -2296.17, y = 3393.1, z = 34.07},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -2306.13, y = 3379.3, z = 30.2},
		textCoords = {x = -2306.13, y = 3379.3, z = 34.2},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	-- Paleto Bay Parking Lot Gate
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = -451.04, y = 6025.31, z = 30.12},
		textCoords = {x = -453.6, y = 6027.87, z = 32.12},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- Mission Row PD Parking Lot Gate
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = 415.85, y = -1025.08, z = 28.15},
		textCoords = {x = 415.85, y = -1021.49, z = 30.15},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- Sandy Shores Parking Lot Gates
	-- PD Front Gate
	{
		objName = 'prop_gate_military_01',
		objCoords  = {x = 1871.62, y = 3681.23, z = 32.35},
		textCoords = {x = 1871.62, y = 3681.23, z = 34.35},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- PD Back Gate
	{
		objName = 'prop_gate_military_01',
		objCoords  = {x = 1858.11, y = 3719.22, z = 32.03},
		textCoords = {x = 1858.11, y = 3719.22, z = 34.03},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- FR Back Gate (Exit)
	{
		objName = 'prop_gate_military_01',
		objCoords  = {x = 1845.07, y = 3712.2, z = 32.17},
		textCoords = {x = 1845.07, y = 3712.2, z = 34.17},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- FR Front Gate (Entrance)
	{
		objName = 'prop_gate_military_01',
		objCoords  = {x = 1804.49, y = 3675.7, z = 33.21},
		textCoords = {x = 1804.49, y = 3675.7, z = 35.21},
		authorizedJobs = { 'ambulance', 'police' },
		locked = true,
		distance = 14,
		size = 2
	},
	-- Los Santos | FBI Building
	-- Entrance Double Doors
	{
		objName = 'v_ilev_fibl_door02',
		objCoords  = {x = 106.37, y = -742.69, z = 46.18},
		textCoords = {x = 106.07, y = -743.76, z = 46.18},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 6
	},
	{
		objName = 'v_ilev_fibl_door01',
		objCoords  = {x = 105.76, y = -746.64, z = 46.18},
		textCoords = {x = 105.71, y = -745.28, z = 46.18},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 6
	},
	-- Front Gates | Left - Right
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = 185.59, y = -732.54, z = 32.77},
		textCoords = {x = 186.82, y = -729.06, z = 34.77},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 25,
		size = 2
	},
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = 190.6, y = -718.76, z = 32.77},
		textCoords = {x = 189.27, y = -722.17, z = 34.77},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 25,
		size = 2
	},
	-- Back Gates | Left - Right
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = 82.51, y = -684.89, z = 30.68},
		textCoords = {x = 81.26, y = -688.39, z = 32.68},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 25,
		size = 2
	},
	{
		objName = 'prop_gate_airport_01',
		objCoords  = {x = 77.53, y = -698.55, z = 30.68},
		textCoords = {x = 78.80, y = -695.21, z = 32.68},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 25,
		size = 2
	}, 

	--
	-- PDM
	--
	
	-- vchodove dvere leve a prave
	{
		objHash = 2059227086,
		objCoords  = {x = -38.57, y = -1108.73, z = 26.04},
		textCoords = {x = -38.77, y = -1108.32, z = 26.74},
		authorizedJobs = { 'cardealer' },
		locked = false,
		distance = 2.5,
		size = 1
	},
	{
		objHash = 1417577297,
		objCoords  = {x = -38.57, y = -1108.73, z = 26.04},
		textCoords = {x = -37.73, y = -1108.73, z = 26.74},
		authorizedJobs = { 'cardealer' },
		locked = false,
		distance = 2.5,
		size = 1
	}, 
	
	-- boss kancl
	{
		objHash = -2051651622,
		objCoords  = {x = -34.28, y = -1108.3, z = 26.74},
		textCoords = {x = -34.28, y = -1108.3, z = 26.74},
		authorizedJobs = { 'cardealer' },
		locked = true,
		distance = 2.5,
		size = 1
	}, 
	
	-- prodejni kancl
	{
		objHash = -2051651622,
		objCoords  = {x = -32.15, y = -1102.53, z = 26.74},
		textCoords = {x = -32.15, y = -1102.53, z = 26.74},
		authorizedJobs = { 'cardealer' },
		locked = true,
		distance = 2.5,
		size = 1
	}, 
	
	-- addon back gate
	{
		objName = 'prop_ld_garaged_01',
		objCoords  = {x = -28.61, y = -1085.34, z = 27.58},
		textCoords = {x = -28.61, y = -1085.34, z = 27.58},
		authorizedJobs = { 'cardealer' },
		locked = true,
		distance = 12,
		size = 2
	}, 
	
	-- male vchodove dvere leve a prave
	{
		objHash = 2059227086,
		objCoords  = {x = -60.3, y = -1093.79, z = 26.63},
		textCoords = {x = -60.3, y = -1093.79, z = 26.63},
		authorizedJobs = { 'cardealer' },
		locked = false,
		distance = 2.5,
		size = 1
	},
	{
		objHash = 1417577297,
		objCoords  = {x = -60.3, y = -1093.79, z = 26.63},
		textCoords = {x = -60.3, y = -1093.79, z = 26.63},
		authorizedJobs = { 'cardealer' },
		locked = false,
		distance = 2.5,
		size = 1
	}, 
	
	--
	-- BENNY
	--
	
	-- vrata
	{
		objHash = -427498890,
		objCoords  = {x = -205.61, y = -1310.48, z = 32.3},
		textCoords = {x = -205.61, y = -1310.48, z = 32.3},
		authorizedJobs = { 'mechanic' },
		locked = true,
		distance = 12,
		size = 2
	}, 
	
	--
	-- Taxi
	--
	
	-- vrata predni
	{
		objHash = -573536640,
		objCoords  = {x = -107.73, y = -66.84, z = 55.42},
		textCoords = {x = -110.57, y = -67.37, z = 56.42},
		authorizedJobs = { 'taxi' },
		locked = true,
		distance = 9,
		size = 2
	}, 
	{
		objHash = -1751877127,
		objCoords  = {x = -115.84, y = -65.35, z = 55.42},
		textCoords = {x = -112.73, y = -66.58, z = 56.42},
		authorizedJobs = { 'taxi' },
		locked = true,
		distance = 10,
		size = 2
	}, 
	
	-- vrata zadni
	{
		objHash = -822900180,
		objCoords  = {x = -87.22, y = -61.27, z = 58.71},
		textCoords = {x = -87.22, y = -61.27, z = 58.71},
		authorizedJobs = { 'taxi' },
		locked = true,
		distance = 11,
		size = 2
	}, 
	
	--
	-- Gank
	--
	
	-- franklin dvere
	{
		objHash = 520341586,
		objCoords  = {x = -14.17, y = -1441.08, z = 31.1},
		textCoords = {x = -14.17, y = -1441.08, z = 31.1},
		authorizedJobs = { 'gang' },
		locked = true,
		distance = 9,
		size = 1
	}, 
	-- Mafia
	--
	
	-- hlavni dvere
    {
        objName = 'apa_p_mp_Yacht_Door_01',
        objCoords  = vector3(-2667.28800000, 1325.69700000, 146.88230000),
        textCoords = vector3(-2667.18, 1325.96, 147.44),
        authorizedJobs = { 'mafia' },
        locked = true,
        distance = 3
    },
-- Mafia #2
    {
        objName = 'xm_Prop_IAA_BASE_Door_01',
        objCoords  = vector3(-2667.28800000, 1330.51, 146.44),
        textCoords = vector3(-2667.28800000, 1330.51, 147.44),
        authorizedJobs = { 'mafia' },
        locked = true,
        distance = 3
    },
-- Mafia Gate
    {
        objName = 'Prop_LrgGate_01c_R',
        objCoords  = vector3(-2653.16, 1327.4, 146.44),
        textCoords = vector3(-2653.04, 1326.3, 147.44),
        authorizedJobs = { 'mafia' },
        locked = true,
        distance = 3
    },
-- Mafia Gate #2
    {
        objName = 'apa_Prop_SS1_MPint_Garage2',
        objCoords  = vector3(-2652.44000000, 1307.36600000, 147.67900000),
        textCoords = vector3(-2652.44000000, 1307.36600000, 147.67900000),
        authorizedJobs = { 'mafia' },
        locked = true,
        distance = 20,
        size = 2
    },
	-- principal bank
	{
		objName = 'hei_v_ilev_bk_gate2_pris',
		objCoords  = {x = 261.99899291992, y = 221.50576782227, z = 106.68346405029},
		textCoords = {x = 261.99899291992, y = 221.50576782227, z = 107.68346405029},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	-- principal bank \2
	{
		objName = 'hei_v_ilev_bk_gate2_pris',
		objCoords  = {x = 257.23, y = 220.5, z = 106.28},
		textCoords = {x = 257.23, y = 220.5, z = 106.28},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},
	
		-- LCS gate
	{
		objHash = -550347177,
		objCoords  = {x = -355.75, y = -133.95, z = 39.01},
		textCoords = {x = -355.75, y = -133.95, z = 39.01},
		authorizedJobs = { 'tokio' },
		locked = true,
		distance = 24,
		size = 2
	},
	-- Nightclub Office
	{
		objName = 'ba_prop_door_club_glass',
		objCoords  = {x = -1621.93, y = -3015.99, z = -75.21},
		textCoords = {x = -1621.93, y = -3015.99, z = -75.21},
		authorizedJobs = { 'nightclub' },
		locked = true,
		distance = 5
	},
	{
		objName = 'ba_prop_door_club_glass',
		objCoords  = {x = -1614.40, y = -3008.41, z = -75.21},
		textCoords = {x = -1614.40, y = -3008.41, z = -75.21},
		authorizedJobs = { 'nightclub' },
		locked = true,
		distance = 5
	},
	  -- ba_prop_door_club_generic_vip
    {
		objName = 'ba_prop_door_club_generic_vip',
		objYaw = 180.0,
		objCoords  = {x = -1607.52, y = -3006.1, z = -75.21},
		textCoords = {x = -1607.52, y = -3006.1, z = -75.21},
		authorizedJobs = { 'nightclub' },
		locked = true,
		distance = 2,
		size = 1
	},
	  -- ba_prop_door_club_generic_vip
    {
		objHash = 1695461688,
		objYaw = 180.0,
		objCoords  = {x = -1611.35, y = -3005.46, z = -79.01},
		textCoords = {x = -1610.95, y = -3005.31, z = -79.01},
		authorizedJobs = { 'nightclub' },
		locked = true,
		distance = 2,
		size = 1
	},
	 -- ba_prop_door_club_generic_vip
    {
		objHash = 1695461688,
		objYaw = 180.0,
		objCoords  = {x = -1583.84, y = -3005.27, z = -76.01},
		textCoords = {x = -1583.84, y = -3005.85, z = -76.01},
		authorizedJobs = { 'nightclub' },
		locked = true,
		distance = 2,
		size = 1
	}
 
}