description 'A simple loading screen!'

files {
    'index.html',
	'cursor.png',

    'css/index.css',
    'css/colors.css',
    'css/icomoon.css',
    
    'fonts/gravity.otf',
    'fonts/icomoon.ttf',
    'fonts/Demonized.ttf',
    
    'img/cursor.png',    
    'img/logo.png',
    'img/Wallpaper_1.jpg',
	'img/Wallpaper_2.jpg',
	'img/Wallpaper_3.jpg',
	'img/Wallpaper_4.jpg',
    
    'js/synn.js',
    'js/config.js',
    'js/logger.js',
    'js/progressbar-handler.js',
    'js/progressbar-main.js',
    'js/progressbar-renderer.js',
    'js/music-handler.js',
    'js/music-controls.js',
    'js/background-handler.js',
}

loadscreen 'index.html'
