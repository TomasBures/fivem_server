-- MADE BY KALU 
-- [ALL CITY]
-- version : 1.00
-- update : 
-- info: 



resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'
-- server_script "main_sv.lua"
client_script "main.lua"

ui_page 'html/ui.html'

files {
	'html/ui.html',
	'html/app.js',
	'html/style.css',
	'font/edosz.ttf',
	'img/header.png',
	'img/blank.png',
	'img/touches/pol.png',
	'img/touches/=.png',
	'img/touches/b.png',
	'img/touches/b2.png',
	'img/touches/BACKSPACE.png',
	'img/touches/f1.png',
	'img/touches/f2.png',
	'img/touches/f3.png',
	'img/touches/f6.png',
	'img/touches/f7.png',
	'img/touches/f9.png',
	'img/touches/g.png',
	'img/touches/h.png',
	'img/touches/home.png',
	'img/touches/k.png',
	'img/touches/l.png',
	'img/touches/lalt.png',
	'img/touches/q.png',
	'img/touches/r.png',
	'img/touches/t.png',
	'img/touches/u.png',
	'img/touches/u2.png',
	'img/touches/y.png',
	'img/touches/z.png'
}