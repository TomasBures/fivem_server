--[[-------------------------------------------------------------------------
  Author: Lucifer
  Credits: @Deziel0495 for Check Weapon function, I'm a lazy shit and cba writing it.
  Last Edit: 08/05/2019 - 22:27 GMT
  Description: The resource for a gangster style unholster, do not claim it as your own.
]]---------------------------------------------------------------------------

local holstered = true
local weapons = { -- Add more to this if you wants more weapons.
	"WEAPON_PISTOL",
	"WEAPON_COMBATPISTOL",
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local ply = PlayerPedId()
		if DoesEntityExist(ply) and not IsEntityDead(ply) and not IsPedInAnyVehicle(ply, true) then
			loadAnimDict( "reaction@intimidation@1h" )
			if CheckWeapon(ply) then
				if holstered then
					loadAnimDict("reaction@intimidation@1h")
					TaskPlayAnim(ply, "reaction@intimidation@1h", "intro", 1.0, 1.0, -1, 50, 0, 0, 0, 0)
					Citizen.Wait(900)
					ClearPedTasks(ply)
					holstered = false
				end
			elseif not CheckWeapon(ped) then
				if not holstered then
					loadAnimDict("reaction@intimidation@1h")
					TaskPlayAnim(ply, "reaction@intimidation@1h", "outro", 1.0, 1.0, -1, 50, 0, 0, 0, 0)
					Citizen.Wait(900)
					ClearPedTasks(ply)
					holstered = true
				end
			end
		end
	end
end)

function CheckWeapon(ped)
	for i = 1, #weapons do
		if GetHashKey(weapons[i]) == GetSelectedPedWeapon(ped) then
			return true
		end
	end
	return false
end

function loadAnimDict(dict)
	while (not HasAnimDictLoaded(dict)) do
		RequestAnimDict(dict)
		Citizen.Wait(0)
	end
end