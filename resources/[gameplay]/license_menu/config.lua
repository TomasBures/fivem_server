Config = {}

Config.Usermenu = {

	{
		name  = 'license',
		label = 'Vaše licence',
		items = {
			{label = 'Zkontrolujte své ID', type = 'checkID'},
			{label = 'Ukažte své ID', type = 'showID'},
			{label = 'Zkontrolujte řidičský průkaz', type = 'checkDriver'},
			{label = 'Ukažte svůj řidičský průkaz', type = 'showDriver'},
			{label = 'Zkontrolujte svou zbrojní licenci', type = 'checkFirearms'},
			{label = 'Ukažte svou zbrojní licenci', type = 'showFirearms'},
		}
	}

}
