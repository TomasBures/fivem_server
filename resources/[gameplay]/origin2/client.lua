local appid = '620636614485475328'
local asset = 'logo'

function SetRP()
    local name = GetPlayerName(PlayerId())
    local id = GetPlayerServerId(PlayerId())
    local info =  " Na nohou"
    local playerspeed = math.floor((GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false))*3.6)) .. " KM/h"

    if(IsPedInAnyVehicle(GetPlayerPed(-1), false)) then
        info = "Jede " .. playerspeed
        if playerspeed == "0 KM/h" then info = "v aute" end
    end

    SetRichPresence(name .. ' [' .. id .. '] ' .. info)
    SetDiscordAppId(appid)
    SetDiscordRichPresenceAsset(asset)
end

Citizen.CreateThread(function()
    
    SetRP()
    
    while true do
        Citizen.Wait(2500)
        SetRP()
    end

end)
