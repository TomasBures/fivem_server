--  V A R I A B L E S --
engineoff = false

-- E N G I N E --
IsEngineOn = true
RegisterNetEvent('engine')
AddEventHandler('engine',function() 
	local player = GetPlayerPed(-1)
	
	if (IsPedSittingInAnyVehicle(player)) then 
		local vehicle = GetVehiclePedIsIn(player,false)
		
		if IsEngineOn == true then
			IsEngineOn = false
			SetVehicleEngineOn(vehicle,false,false,false)
		else
			IsEngineOn = true
			SetVehicleUndriveable(vehicle,false)
			SetVehicleEngineOn(vehicle,true,false,false)
		end
		
		while (IsEngineOn == false) do
			SetVehicleUndriveable(vehicle,true)
			Citizen.Wait(100)
		end
	end
end)

RegisterNetEvent('engineoff')
AddEventHandler('engineoff',function() 
	local player = GetPlayerPed(-1)

    if (IsPedSittingInAnyVehicle(player)) then 
        local vehicle = GetVehiclePedIsIn(player,false)
		engineoff = true
		ShowNotification("Motor ~r~chcipnut~s~.")
		
		while (engineoff) do
			SetVehicleEngineOn(vehicle,false,false,false)
			SetVehicleUndriveable(vehicle,true)
			Citizen.Wait(100)
		end
	end
end)
RegisterNetEvent('engineon')
AddEventHandler('engineon',function() 
    local player = GetPlayerPed(-1)

    if (IsPedSittingInAnyVehicle(player)) then 
        local vehicle = GetVehiclePedIsIn(player,false)
		engineoff = false
		SetVehicleUndriveable(vehicle,false)
		SetVehicleEngineOn(vehicle,true,false,false)
		ShowNotification("Motor ~g~nastartovan~s~.")
	end
end)
-- T R U N K --
RegisterNetEvent('trunk')
AddEventHandler('trunk',function() 
	local player = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(player,true)
			
	if vehicle then
		local isopen = GetVehicleDoorAngleRatio(vehicle,5)
	
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,5,0,0)
		else
			SetVehicleDoorShut(vehicle,5,0)
		end
	else
		ShowNotification("~r~Musis byt ve vozidle")
	end
end)
-- R E A R  D O O R S --
RegisterNetEvent('rdoors')
AddEventHandler('rdoors',function() 
	local player = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(player,true)
			
	if vehicle then
		local isopen = GetVehicleDoorAngleRatio(vehicle,2) and GetVehicleDoorAngleRatio(vehicle,3)
	
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,2,0,0)
			SetVehicleDoorOpen(vehicle,3,0,0)
		else
			SetVehicleDoorShut(vehicle,2,0)
			SetVehicleDoorShut(vehicle,3,0)
		end
	else
		ShowNotification("~r~Musis byt ve vozidle")
	end
end)

-- F R O N T  D O O R S --
RegisterNetEvent('fdoors')
AddEventHandler('fdoors',function() 
	local player = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(player,true)
			
	if vehicle then
		local isopen = GetVehicleDoorAngleRatio(vehicle,0) and GetVehicleDoorAngleRatio(vehicle,1)
	
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,0,0,0)
			SetVehicleDoorOpen(vehicle,1,0,0)
		else
			SetVehicleDoorShut(vehicle,0,0)
			SetVehicleDoorShut(vehicle,1,0)
		end
	else
		ShowNotification("~r~Musis byt ve vozidle")
	end
end)			

-- H O O D --
RegisterNetEvent('hood')
AddEventHandler('hood',function() 
	local player = GetPlayerPed(-1)
	local vehicle = GetVehiclePedIsIn(player,true)
			
	if vehicle then
		local isopen = GetVehicleDoorAngleRatio(vehicle,4)
	
		if (isopen == 0) then
			SetVehicleDoorOpen(vehicle,4,0,0)
		else
			SetVehicleDoorShut(vehicle,4,0)
		end
	else
		ShowNotification("~r~Musis byt ve vozidle")
	end
end)

function ShowNotification( text )
    SetNotificationTextEntry( "STRING" )
    AddTextComponentString( text )
    DrawNotification( false, false )
end
