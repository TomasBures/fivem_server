-- E N G I N E --
RegisterCommand('engine', function(s, n, m)
		TriggerClientEvent('engine', s)
end)
-- T R U N K --
RegisterCommand('trunk', function(s, n, m)
		TriggerClientEvent('trunk', s)
end)
-- R E A R  D O O R S --
RegisterCommand('rdoors', function(s, n, m)
		TriggerClientEvent('rdoors', s)
end)

-- F R O N T  D O O R S --
RegisterCommand('fdoors', function(s, n, m)
		TriggerClientEvent('fdoors', s)
end)

-- H O O D --
RegisterCommand('hood', function(s, n, m)
		TriggerClientEvent('hood', s)
end)