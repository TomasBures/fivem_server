ESX = nil
local isOn	=	false

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function hasCarNeons(veh)

	local currentMods = ESX.Game.GetVehicleProperties(veh)
	
--	print('xenonmod: ' .. tostring(currentMods.modXenon))
	
	if currentMods.modXenon then
		return true
	else
		return false
	end
end

RegisterCommand("neon", function()
    local ped = PlayerPedId()
    local veh = GetVehiclePedIsIn(ped, false)
	
--	print('hasneon: ' .. tostring(hasCarNeons(veh)))
	
	isOn = IsVehicleNeonLightEnabled(veh, 0)
	
	if hasCarNeons(veh) == false then
		ESX.ShowNotification("Na vozidle nejsou neony")
    elseif veh ~= nil and veh ~= 0 and veh ~= 1 and hasCarNeons(veh) then
		--left
        if isOn then
            SetVehicleNeonLightEnabled(veh, 0, false)
            SetVehicleNeonLightEnabled(veh, 1, false)
            SetVehicleNeonLightEnabled(veh, 2, false)
            SetVehicleNeonLightEnabled(veh, 3, false)
			
			isOn = false
        else
            SetVehicleNeonLightEnabled(veh, 0, true)
            SetVehicleNeonLightEnabled(veh, 1, true)
            SetVehicleNeonLightEnabled(veh, 2, true)
            SetVehicleNeonLightEnabled(veh, 3, true)
			
			isOn = true
        end
    end
end, false)