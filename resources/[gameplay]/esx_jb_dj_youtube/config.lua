Config = {}

Config.nightclubs = {
	 nightclubUnicorn = {
		 dancefloor = {
			 Pos = {x = 110.13, y=  -1288.70, z = 28.85},
			 Marker = { w= 25.0, h= 1.0,r = 204, g = 204, b = 0},
			 HelpPrompt = "Pressione ~INPUT_PICKUP~ para exibir o menu de musicas",
		 }, 
		 djbooth = {
			 Pos = {x = 120.67, y= -1280.99, z = 29.48}, 
			 Marker = { w= 1.0, h= 0.5,r = 204, g = 204, b = 0},
			 HelpPrompt = "Pressione ~INPUT_PICKUP~ para tomar o seu lugar de dj",
		 },
	},
}

Config.Songs = {
	-- SONGS = Youtube Video ID Ex.: www.youtube.com/watch?v=((jfreFPe99GU)) catch only jfreFPe99GU
	-- SONGS = ID do video  do Youtube Exemplo: www.youtube.com/watch?v=((jfreFPe99GU)) pegue apenas jfreFPe99GU
	{song = "zTgfGtutqhY", label ="Vanic - Save Yourself ft. Gloria Kim"},
	{song = "HrQ_oNHGV70", label ="Remady feat. Manu.L - Save your Heart"},
	{song = "gYodYfNAwc0", label ="KVSH Feat. Breno Miranda - PIECES"},
	{song = "uiyUOrr5Ieo", label ="Martin Garrix - Bouncybob "},
}