Locales['en'] = {
	-- afk script
	['afk'] = '^2Anti-AFK',
	['afk_kicked_message'] = 'byl jsi kicknut za afkovani',
	['afk_warning'] = 'budes kicknut za ^3%s sekund^0 za neaktivitu!',

	-- speed limiter
	['speedlimiter_set'] = 'omezovac nastaven na ~b~%s~s~ km/h',
	['speedlimiter_disabled'] = 'omezovac ~y~vypnut~s~',
}