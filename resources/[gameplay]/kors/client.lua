Citizen.CreateThread(function()
    local dict = "amb@world_human_hang_out_street@female_arms_crossed@base"
    
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(100)
	end
	
    local handsup = false
	local playerPed = nil
	local IsInVeh = nil
	
	while true do
		Citizen.Wait(10)
		
		playerPed = GetPlayerPed(-1)
		IsInVeh = IsPedInAnyVehicle(playerPed, false)

		if IsInVeh then
			handsup = false
			ClearPedSecondaryTask(playerPed)
		end
		
		if IsControlJustPressed(1, 47) and  IsInVeh == false then --Start holding g
            if not handsup and IsInVeh == false then
                TaskPlayAnim(GetPlayerPed(-1), dict, "base", 8.0, 8.0, -1, 50, 0, false, false, false)
                handsup = true
            else
                handsup = false
                ClearPedSecondaryTask(playerPed)
            end
        end
    end
end)
	