local function runHUD(vehicle, stop)
    Citizen.Wait(1)
    if not IsPedInAnyEVVehicle() or not IsPedDriving() then
        return stop()
    end

    local rpm = GetVehicleCurrentRpm(vehicle)
    local whr = math.ceil(math.max(0, (965 * rpm) - 195))
	
    DrawRct(UI.x + 0.165, UI.y + 0.862, 0.046,0.03,0,0,0,150)
    DrawTxt(UI.x + 0.665, UI.y + 1.35, 1.0,1.0,0.64, "~w~" .. whr, 255, 255, 255, 255)
    DrawTxt(UI.x + 0.69, UI.y + 1.3615, 1.0,1.0,0.4, "~w~ Whr", 255, 255, 255, 255)
end

-- Vehicle detection thread
SIThreads:LoopUntilStopped('HUD_MAIN', function(_) -- we can't stop and we won't stop
    Citizen.Wait(1000)
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), false)
    if IsPedInAnyEVVehicle() and IsPedDriving() then
        SIThreads:LoopUntilStopped('HUD_DRAW', function(stop) runHUD(vehicle, stop) end)
    end
end)