ESX                = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterCommand('carstats', function(source, args, user)
		  TriggerClientEvent('carstats', source, {})
end)

RegisterServerEvent('carstats:getMileage')
AddEventHandler('carstats:getMileage', function(plate)	
	local _source = source
	
	MySQL.Async.fetchAll(
	"SELECT mileage FROM owned_vehicles WHERE `plate`=@plate",
				{
					['@plate'] = plate
				},
	function(data) 
		if (#data == 0) then TriggerClientEvent('carmileageresp', _source, -1) end
		
		for _,v in pairs(data) do
			TriggerClientEvent('carmileageresp', _source, v.mileage)
			break
		end
	end) 
end)