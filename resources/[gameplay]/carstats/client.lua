ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end) 

RegisterNetEvent('carstats');
AddEventHandler('carstats', function()
local ped = PlayerPedId()
	if IsPedInAnyVehicle(ped, false) then
		local veh = GetVehiclePedIsIn(ped, false)
		local model = GetEntityModel(veh, false)
		local hash = GetHashKey(model)
		TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Max speed (v zakladu):^7 ".. round(GetVehicleMaxSpeed(model) * 3.6,1))
		TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Zrychleni:^7 ".. round(GetVehicleModelAcceleration(model),1))
		TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Prevodovych stupnu:^7 ".. GetVehicleHighGear(veh))
		TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Kapacita:^7 ".. GetVehicleMaxNumberOfPassengers(veh) + 1)
	end
end)

--snippet borrowed from http://lua-users.org/wiki/SimpleRound
function round(num, numDecimalPlaces)
  local mult = 100^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

RegisterCommand("carmileage", function()
    local ped = PlayerPedId()
    local veh = GetVehiclePedIsIn(ped, false)
	
	TriggerServerEvent('carstats:getMileage', GetVehicleNumberPlateText(veh))
end, false)

RegisterNetEvent('carmileageresp');
AddEventHandler('carmileageresp', function(mileage)
		if mileage < 0 then
			TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Najezd:^7 data pro toto vozidlo nejsou dostupna")
		else
			TriggerEvent('chatMessage', "", {255, 0, 0}, "^3Najezd:^7 ".. tostring(round(mileage, 1)))
		end
end)